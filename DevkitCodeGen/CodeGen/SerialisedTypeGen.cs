﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace HBPartCodeGen
{
    static class SerialisedTypeGen
    {
        private static List<string> blacklistNamespaces = new List<string>()
        {
            "System",
            "UnityEngine",
        };

        private static List<string> blacklistAttributes = new List<string>()
        {
            "UnityEngine.AddComponentMenu", // AddComponentMenu is always broken, since the code to do anything is not generated
            "SubjectNerd.Utilities.ReorderableAttribute",
            "UnityEngine.ContextMenu",
            "System.Runtime.CompilerServices.CompilerGeneratedAttribute",
            "UnityEngine.Serialization.FormerlySerializedAsAttribute",
        };

        private static List<string> blacklistMethods = new List<string>()
        {

        };

        public class PartStub
        {

            public PartStub(Type inType)
            {
                partType = inType;
            }

            public List<Type> dependencies = new List<Type>();

            private Type partType;
            private StringBuilder sb;
            private StringBuilder indentSb;
            private List<string> namespaces = new List<string>();
            private List<PropertyInfo> autoProperties = new List<PropertyInfo>();

            public string WriteFile(string outputDir)
            {
                string outPath = outputDir + "/" + partType.Name + ".cs";
                using (StreamWriter swriter = new StreamWriter(outPath))
                {
                    Console.WriteLine("writing " + partType.Name + " to " + outPath);
                    swriter.Write(sb.ToString());
                }
                return outPath;
            }

            public void GenerateNow()
            {

                Console.WriteLine("generating " + partType.Name);
                sb = new StringBuilder();
                indentSb = new StringBuilder();


                // start of file
                AppendReferences();


                // own namespace?
                if (partType.Namespace != null)
                {
                    AppendLine("namespace ", false); AppendLine(partType.Namespace);
                    OpenBracket();
                }



                // 'root' class
                GenerateClass(partType);

                CloseAllBrackets(); // make sure file is all closed

            }


            private void GenerateClass(Type targetClass)
            {
                // calculate dependencies
                GetDependencies();

                // attributes
                foreach (Attribute attrib in targetClass.GetCustomAttributes(false))
                {
                    WriteAttribute(attrib);
                }

                string friendlyBaseName = GetFriendlyClass(targetClass.BaseType);

                // switch for handling special types (enum, struct)
                switch (friendlyBaseName)
                {
                    case "enum":
                        AppendLine("public enum ", false); AppendLine(targetClass.Name);
                        OpenBracket();

                        int index = 0;
                        foreach (int enumValue in Enum.GetValues(targetClass))
                        {
                            index++; // count here so we can just check for less than

                            AppendLine(Enum.GetName(targetClass, enumValue), false);
                            AppendLine(" = ", false);
                            AppendLine(enumValue, false);

                            // comma on all but last entry
                            if (index < Enum.GetValues(targetClass).Length)
                            {
                                AppendLine(",", false);
                            }

                            AppendLine(""); // end line
                        }

                        CloseBracket();
                        return;

                    case "struct":
                        AppendLine("public struct ", false); AppendLine(targetClass.Name);
                        break;

                    default:
                        // universal class declaration
                        AppendLine("public class ", false); AppendLine(targetClass.Name, false);
                        if (!targetClass.IsNested || targetClass.BaseType == typeof(object)) { AppendLine(" : ", false); AppendLine(friendlyBaseName, false); }
                        break;
                }

                AppendLine(""); // add newline
                OpenBracket();

                List<string> serPrivateFields = new List<string>();

                // add private fields (for getters/setters)
                foreach (FieldInfo field in targetClass.GetFields(BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if (HasAttribute(field, "System.NonSerializedAttribute")) { continue; } // never add nonserialised fields
                    if (!HasAttribute(field, "UnityEngine.SerializeField")) { continue; } // only interested in serialisables

                    // field attributes
                    foreach (Attribute attrib in field.GetCustomAttributes(false))
                    {
                        WriteAttribute(attrib);
                    }

                    // field declaration
                    AppendLine("private ", false); AppendLine(GetFriendlyType(field.FieldType), false); AppendLine(" ", false); AppendLine(field.Name, false); AppendLine(";");

                    serPrivateFields.Add(field.Name);
                }



                // add properties
                foreach (PropertyInfo property in targetClass.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {
                    //if (!serialiseAll) { if (!HasAttribute(field, "HBS.SerializePartVarAttribute")) { continue; } } // not serialise all and not serialise var
                    if (HasAttribute(property, "System.NonSerializedAttribute")) { continue; } // never add nonserialised fields


                    // property attributes
                    foreach (Attribute attrib in property.GetCustomAttributes(false))
                    {
                        WriteAttribute(attrib);
                    }

                    // property declaration
                    AppendLine("public ", false); AppendLine(GetFriendlyType(property.PropertyType), false); AppendLine(" ", false); AppendLine(property.Name, false);

                    if (property.GetAccessors().Length == 0)
                    {
                        // property has no accessors???
                        AppendLine(";"); // end declaration and open new line
                        continue;
                    }

                    // property has accessors
                    autoProperties.Add(property);

                    AppendLine("");
                    OpenBracket(); // open getterss/setters

                    MethodInfo method = property.GetGetMethod();

                    while (method != null) // only runs once, just using this to break easier
                    {
                        AppendLine("get");

                        OpenBracket(); // open method body


                        // return private field if setter defined
                        if (property.GetSetMethod() != null)
                        {
                            if (serPrivateFields.Contains("_" + property.Name))
                            {
                                AppendLine("return ", false); AppendLine("_", false); AppendLine(property.Name, false); AppendLine(";");
                                CloseBracket(); // close method body
                                break;
                            }
                        }

                        // return null if possible
                        if (IsNullable(method.ReturnType))
                        {
                            AppendLine("return null;", true); // nullable type, safest way to return
                            CloseBracket(); // close method body
                            break;
                        }

                        // return isn't nullable


                        // handle IEnumerator/IEnumerable
                        if ((method.ReturnType.IsGenericType && (
                            method.ReturnType.GetGenericTypeDefinition() == typeof(IEnumerator<>) ||
                            method.ReturnType.GetGenericTypeDefinition() == typeof(IEnumerable<>))) ||
                            method.ReturnType == typeof(System.Collections.IEnumerator) ||
                            method.ReturnType == typeof(System.Collections.IEnumerable))
                        {

                            {
                                // special handling for IEnumerator
                                AppendLine("yield return null;", true); // special handling for IEnumerator
                                CloseBracket(); // close method body
                                break;
                            }
                        }

                        AppendLine("return ", false); // not nullable, must return a default value

                        bool isStruct = method.ReturnType.IsValueType && !method.ReturnType.IsEnum;
                        bool isClass = method.ReturnType.IsClass;

                        if (!IsBuiltIn(method.ReturnType))
                        {
                            if (method.ReturnType.IsArray || method.ReturnType.IsGenericType || isStruct || isClass)
                            {
                                AppendLine("new ", false); // needs new operator if not built in
                            }
                        }

                        string constructorOverride = ConstructorOverride(method.ReturnType);
                        if (constructorOverride != "")
                        {
                            AppendLine(constructorOverride, false); AppendLine(";", true);
                        }
                        else
                        {
                            AppendLine(GetDefaultReturnValue(method.ReturnType), false); AppendLine(";", true);
                        }

                        CloseBracket(); // close method body
                        break;
                    } // end getter

                    method = property.GetSetMethod();

                    if (method != null)
                    {
                        AppendLine("set");
                        OpenBracket(); // open method body

                        if (serPrivateFields.Contains("_" + property.Name))
                        {
                            AppendLine("_", false); AppendLine(property.Name, false); AppendLine(" = value;");
                        }

                        CloseBracket(); // close method body
                    }

                    CloseBracket(); // close getters/setters
                }


                // add fields
                foreach (FieldInfo field in targetClass.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {
                    if (HasAttribute(field, "System.NonSerializedAttribute")) { continue; } // never add nonserialised fields


                    // field attributes
                    foreach (Attribute attrib in field.GetCustomAttributes(false))
                    {
                        WriteAttribute(attrib);
                    }

                    // field declaration
                    AppendLine("public ", false); AppendLine(GetFriendlyType(field.FieldType), false); AppendLine(" ", false); AppendLine(field.Name, false); AppendLine(";");
                }

                // add function signatures
                foreach (MethodInfo method in targetClass.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {

                    // method attributes
                    foreach (Attribute attrib in method.GetCustomAttributes(false))
                    {
                        WriteAttribute(attrib);
                    }

                    WriteMethod(method);
                }


                // add nested types
                foreach (Type nestedType in targetClass.GetNestedTypes())
                {
                    GenerateClass(nestedType);
                }

                // end file
                CloseBracket();
                return;
            }

            private void WriteMethod(MethodInfo method)
            {
                if (blacklistMethods.Contains(method.Name)) { return; } // blacklist some methods
                
                foreach (PropertyInfo autoProperty in autoProperties)
                {
                    MethodInfo[] accessors = autoProperty.GetAccessors();
                    if (accessors[0] == method || (accessors.Length > 1 && accessors[1] == method))
                    {
                        return; // skip autoproperty getter/setter
                    }
                }

                AppendLine("public ", false); // should always be public, otherwise we shouldn't have been able to find it

                // method override/virtual?
                bool methodOverride = method.GetBaseDefinition() != method;
                if (method.IsVirtual && !methodOverride)
                {
                    AppendLine("virtual ", false);
                } else if (method.IsVirtual && methodOverride)
                {
                    AppendLine("override ", false);
                }

                Type returnType = method.ReturnType;
                if (returnType.GetElementType()!= null)
                {
                    returnType = returnType.GetElementType();
                }
                AppendLine(GetFriendlyType(method.ReturnType), false); AppendLine(" ", false); // return type



                AppendLine(method.Name, false);

                AppendLine("(", false); // open argument parentheses

                // get arguments
                ParameterInfo[] methodParams = method.GetParameters();

                List<ParameterInfo> outParams = new List<ParameterInfo>();

                foreach (ParameterInfo paramInfo in methodParams)
                {
                    if (methodParams.Length > 1)
                    {
                        // only add comma if not last parameter
                        if (methodParams.First() != paramInfo)
                        {
                            AppendLine(", ", false);
                        }
                    }

                    if (paramInfo.IsOut)
                    {
                        AppendLine("out ", false);
                        AppendLine(GetFriendlyType(paramInfo.ParameterType.GetElementType()), false); AppendLine(" ", false);

                        outParams.Add(paramInfo);
                    }
                    else
                    {
                        AppendLine(GetFriendlyType(paramInfo.ParameterType), false); AppendLine(" ", false);
                    }

                    AppendLine(paramInfo.Name, false);
                }

                AppendLine(")", false); // close argument parentheses

                if (method.ReturnType == typeof(void))
                {
                    AppendLine(" {}", true);
                    return;
                }

                AppendLine("", true); // newline

                OpenBracket(); // open method body

                // assign to any out parameters

                foreach (ParameterInfo outParamInfo in outParams)
                {
                    AppendLine(outParamInfo.Name, false); AppendLine(" = ", false); // out param name to assign to

                    AppendLine(GetDefaultReturnValue(outParamInfo.ParameterType.GetElementType()), false); AppendLine(";", true); 
                }

                // return null if possible
                if (IsNullable(method.ReturnType))
                {
                    AppendLine("return null;", true); // nullable type, safest way to return
                    CloseBracket(); // close method body
                    return;
                }

                // return isn't nullable


                // handle IEnumerator/IEnumerable
                if ((method.ReturnType.IsGenericType && (
                    method.ReturnType.GetGenericTypeDefinition() == typeof(IEnumerator<>) ||
                    method.ReturnType.GetGenericTypeDefinition() == typeof(IEnumerable<>))) ||
                    method.ReturnType == typeof(System.Collections.IEnumerator) ||
                    method.ReturnType == typeof(System.Collections.IEnumerable))
                {

                    {
                        // special handling for IEnumerator
                        AppendLine("yield return null;", true); // special handling for IEnumerator
                        CloseBracket(); // close method body
                        return;
                    }
                }


                AppendLine("return ", false); // not nullable, must return a default value


                //bool usedNew = false;

                bool isStruct = method.ReturnType.IsValueType && !method.ReturnType.IsEnum;
                bool isClass = method.ReturnType.IsClass;

                if (!IsBuiltIn(method.ReturnType))
                {
                    if (method.ReturnType.IsArray || method.ReturnType.IsGenericType || isStruct || isClass)
                    {
                        AppendLine("new ", false); // needs new operator if not built in
                                                   //usedNew = true;
                    }
                }

                string constructorOverride = ConstructorOverride(method.ReturnType);
                if (constructorOverride != "")
                {
                    AppendLine(constructorOverride, false); AppendLine(";", true);
                }
                else
                {
                    AppendLine(GetDefaultReturnValue(method.ReturnType), false); AppendLine(";", true);
                }


                CloseBracket(); // close method body
            }

            private static string ConstructorOverride(Type type)
            {
                string ret = "";

                if (type.Name == "XElement")
                    ret = "XElement(\"autoGenerated\")";
                if (type.Name == "Material")
                    ret = "Material(\"autoGenerated\")";


                return ret;
            }



            private static string GetDefaultReturnValue(Type type)
            {
                Type decType = type.DeclaringType;
                string ret = "";
                if (type == typeof(int))
                    ret = "0";
                else if (type == typeof(short))
                    ret = "0";
                else if (type == typeof(byte))
                    ret = "0";
                else if (type == typeof(bool))
                    ret = "false";
                else if (type == typeof(long))
                    ret = "0";
                else if (type == typeof(float))
                    ret = "0";
                else if (type == typeof(double))
                    ret = "0";
                else if (type == typeof(decimal))
                    ret = "0";
                else if (type == typeof(string))
                    ret = "\"\"";
                else if (type == typeof(object))
                    ret = "object()";
                else if (type.Name == "Vector3")
                    ret = "Vector3.zero";
                else if (type.Name == "Vector2")
                    ret = "Vector2.zero";
                else if (type.Name == "Quaternion")
                    ret = "Quaternion.identity";
                else if (type.IsGenericType)
                    ret = type.Name.Split('`')[0] + "<" + string.Join(", ", type.GetGenericArguments().Select(x => GetFriendlyType(x)).ToArray()) + ">()";
                else if (type.IsArray)
                    ret = GetFriendlyType(type.GetElementType()) + "[0]";
                else if (type.IsEnum)
                    ret = GetFriendlyType(type) + "." + Enum.ToObject(type, 0).ToString();
                else
                    ret = type.Name + "()";


                if (decType != null && !decType.IsGenericType)
                {
                    if (decType.Name != "")
                        ret = decType.Name + "." + ret;
                }

                return ret;

            }

            private bool IsBuiltIn(Type type)
            {
                //string scopeName = type.Module.ScopeName;
                //return scopeName == "CommonLanguageRuntimeLibrary";// || scopeName == "UnityEngine.dll";


                bool ret = false;
                if (type == typeof(int))
                    ret = true;
                else if (type == typeof(short))
                    ret = true;
                else if (type == typeof(byte))
                    ret = true;
                else if (type == typeof(bool))
                    ret = true;
                else if (type == typeof(long))
                    ret = true;
                else if (type == typeof(float))
                    ret = true;
                else if (type == typeof(double))
                    ret = true;
                else if (type == typeof(decimal))
                    ret = true;
                else if (type == typeof(string))
                    ret = true;
                else if (type.Name == "Vector3")
                    ret = true;
                else if (type.Name == "Vector2")
                    ret = true;
                else if (type.Name == "Quaternion")
                    ret = true;

                return ret;
            }

            private bool IsNullable(Type type)
            {
                if (type.Name == "Transform")
                    return true;

                return Nullable.GetUnderlyingType(type) != null;
            }

            private bool HasAttribute(object reference, string attrib)
            {
                foreach (Attribute attribInfo in ((MemberInfo)reference).GetCustomAttributes(false))
                {
                    if (attribInfo.ToString() == attrib) { return true; }
                }
                return false;
            }


            private void WriteAttribute(Attribute attrib)
            {
                if (blacklistAttributes.Contains(attrib.GetType().FullName)) { return; } // blacklist some attributes

                AppendLine("[", false);

                AppendLine(attrib.GetType().FullName, false);

                if (attrib.GetType().Name.Contains("Attribute"))
                {
                    sb.Remove(sb.Length - 9, 9); // remove 'Attribute'
                }

                FieldInfo[] fields = attrib.GetType().GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

                if (fields.Length > 0) { AppendLine("(", false); }

                foreach (FieldInfo field in fields)
                {
                    if (field.GetValue(attrib) == null) { continue; }

                    // add comma to separate multiple values if needed
                    if (fields.Length > 1)
                    {
                        // only add comma if not last field
                        if (fields.First() != field)
                        {
                            AppendLine(", ", false);
                        }
                    }

                    // format value nicely
                    switch (field.GetValue(attrib).GetType().ToString())
                    {
                        case "System.String":
                            AppendLine("\"", false);
                            AppendLine(field.GetValue(attrib), false);
                            AppendLine("\"", false);
                            break;
                        case "System.Single":
                            AppendLine(field.GetValue(attrib), false);
                            AppendLine("f", false);
                            break;

                        case "System.RuntimeType":
                            AppendLine("typeof(", false);
                            AppendLine(field.GetValue(attrib), false);
                            AppendLine(")", false);
                            break;
                    }

                }

                if (fields.Length > 0) { AppendLine(")", false); }



                AppendLine("]", true);
            }

            private void GetDependencies()
            {
                List<Type> usedTypes = new List<Type>();
                Type fieldType;
                Type propType;
                Type dependencyType;
                Type decType;

                // base class
                usedTypes.Add(partType.BaseType);

                // private fields
                foreach (FieldInfo field in partType.GetFields(BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if (HasAttribute(field, "System.NonSerializedAttribute")) { continue; } // never add nonserialised fields

                    if (!HasAttribute(field, "UnityEngine.SerializeField")) { continue; } // only interested in serialisables

                    fieldType = field.FieldType;

                    // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                    if (fieldType.IsGenericType)
                    {
                        foreach (Type genericType in fieldType.GetGenericArguments())
                        {
                            usedTypes.Add(genericType);
                        }
                        continue;
                    }
                    string baseNamespace = field.FieldType.Namespace;
                    if (baseNamespace != "" && baseNamespace != null)
                        if (baseNamespace.Contains(".")) { baseNamespace = baseNamespace.Split('.')[0]; }

                    if (blacklistNamespaces.Contains(baseNamespace)) { continue; } // blacklist namespace like System or UnityEngine


                    // check if type has parent type and use that as dependency, otherwise set type as dependency

                    dependencyType = field.FieldType;

                    decType = field.FieldType.DeclaringType;
                    if (decType != null && !decType.IsGenericType && decType.Name != "")
                    {
                        dependencyType = decType;
                    }

                    if (dependencyType.IsArray) { dependencyType = dependencyType.GetElementType(); }

                    if (!usedTypes.Contains(dependencyType)) { usedTypes.Add(dependencyType); }
                }


                // fields
                foreach (FieldInfo field in partType.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {
                    if (!field.IsPublic) { continue; } // only interested in public fields

                    fieldType = field.FieldType;

                    // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                    if (fieldType.IsGenericType) {
                        foreach (Type genericType in fieldType.GetGenericArguments())
                        {
                            usedTypes.Add(genericType);
                        }
                        continue;
                    }
                    string baseNamespace = field.FieldType.Namespace;
                    if (baseNamespace != "" && baseNamespace != null)
                        if (baseNamespace.Contains(".")) { baseNamespace = baseNamespace.Split('.')[0]; }

                    if (blacklistNamespaces.Contains(baseNamespace)) { continue; } // blacklist namespace like System or UnityEngine

                    if (HasAttribute(field.FieldType, "HBS.SerializeAttribute") || HasAttribute(field.FieldType, "HBS.SerializeComponentOnlyAttribute")) { continue; }

                    // type does not contain serialisable attrib


                    // check if type has parent type and use that as dependency, otherwise set type as dependency

                    dependencyType = field.FieldType;

                    decType = field.FieldType.DeclaringType;
                    if (decType != null && !decType.IsGenericType && decType.Name != "")
                    {
                        dependencyType = decType;
                    }

                    if (dependencyType.IsArray) { dependencyType = dependencyType.GetElementType(); }

                    if (!usedTypes.Contains(dependencyType)) { usedTypes.Add(dependencyType); }
                }

                // properties
                foreach (PropertyInfo prop in partType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {

                    propType = prop.PropertyType;

                    // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                    if (propType.IsGenericType)
                    {
                        foreach (Type genericType in propType.GetGenericArguments())
                        {
                            usedTypes.Add(genericType);
                        }
                        continue;
                    }
                    string baseNamespace = prop.PropertyType.Namespace;
                    if (baseNamespace != "" && baseNamespace != null)
                        if (baseNamespace.Contains(".")) { baseNamespace = baseNamespace.Split('.')[0]; }

                    if (blacklistNamespaces.Contains(baseNamespace)) { continue; } // blacklist namespace like System or UnityEngine

                    if (HasAttribute(prop.PropertyType, "HBS.SerializeAttribute") || HasAttribute(prop.PropertyType, "HBS.SerializeComponentOnlyAttribute")) { continue; }

                    // type does not contain serialisable attrib


                    // check if type has parent type and use that as dependency, otherwise set type as dependency

                    dependencyType = prop.PropertyType;

                    decType = prop.PropertyType.DeclaringType;
                    if (decType != null && !decType.IsGenericType && decType.Name != "")
                    {
                        dependencyType = decType;
                    }

                    if (dependencyType.IsArray) { dependencyType = dependencyType.GetElementType(); }

                    if (!usedTypes.Contains(dependencyType)) { usedTypes.Add(dependencyType); }
                }

                // methods
                Type retType;
                foreach (MethodInfo method in partType.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                {

                    retType = method.ReturnType;

                    if (retType.IsGenericType)
                    {
                        foreach (Type genericType in retType.GetGenericArguments())
                        {
                            usedTypes.Add(genericType);
                        }
                    }

                    if (retType.IsArray)
                    {
                        retType = retType.GetElementType();
                        if (!usedTypes.Contains(retType)) { usedTypes.Add(retType); }
                    }

                    ParameterInfo[] methodParams = method.GetParameters();

                    List<ParameterInfo> outParams = new List<ParameterInfo>();

                    foreach (ParameterInfo paramInfo in methodParams)
                    {

                        // check if type has parent type and use that as dependency, otherwise set type as dependency


                        dependencyType = paramInfo.ParameterType;

                        if (paramInfo.IsOut)
                        {
                            dependencyType = paramInfo.ParameterType.GetElementType();
                        }

                        decType = paramInfo.ParameterType.DeclaringType;
                        if (decType != null && !decType.IsGenericType && decType.Name != "")
                        {
                            dependencyType = decType;
                        }


                        if (dependencyType.IsArray) { dependencyType = dependencyType.GetElementType(); }



                        if (dependencyType.IsGenericType)
                        {
                            foreach (Type genericType in dependencyType.GetGenericArguments())
                            {
                                usedTypes.Add(genericType);
                            }
                        }


                        if (!usedTypes.Contains(dependencyType)) { usedTypes.Add(dependencyType); }
                    }

                }


                // check used types and add as dependencies
                foreach (Type usedType in usedTypes)
                {
                    string baseNamespace = usedType.Namespace;
                    if (baseNamespace != "" && baseNamespace != null)
                        if (baseNamespace.Contains(".")) { baseNamespace = baseNamespace.Split('.')[0]; }

                    if (blacklistNamespaces.Contains(baseNamespace)) { continue; } // blacklist namespace like System or UnityEngine

                    if (HasAttribute(usedType, "HBS.SerializeAttribute") || HasAttribute(usedType, "HBS.SerializeComponentOnlyAttribute")) { continue; } // serialised types will be generated

                    // check if type has parent type and use that as dependency, otherwise set type as dependency
                    dependencyType = usedType;
                    decType = usedType.DeclaringType;
                    if (decType != null && !decType.IsGenericType && decType.Name != "")
                    {
                        dependencyType = decType;
                    }

                    if (dependencyType.IsArray) { dependencyType = dependencyType.GetElementType(); } // if type is array, discard array part

                    if (IsBuiltIn(usedType)) { continue; }

                    if (!dependencies.Contains(usedType)) { dependencies.Add(usedType); }
                }

            }

            private void AppendReferences()
            {
                // build namespaces list

                // always
                namespaces.Add("System");
                namespaces.Add("UnityEngine");
                namespaces.Add("HBS");
                namespaces.Add("SLua");

                // dependenceis below

                List<Type> usedTypes = new List<Type>();
                Type fieldType;

                if (partType.BaseType.ToString() != "System.Enum")
                {

                    // fields
                    foreach (FieldInfo field in partType.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (!field.IsPublic) { continue; } // only interested in public fields

                        fieldType = field.FieldType;

                        // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                        if (fieldType.IsGenericType)
                        {
                            foreach (Type genericType in fieldType.GetGenericArguments())
                            {
                                usedTypes.Add(genericType);
                            }
                        }

                        if (fieldType.IsArray) { fieldType = fieldType.GetElementType(); } // if type is array, discard array part

                        usedTypes.Add(fieldType);

                    }

                    // methods
                    foreach (MethodInfo method in partType.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (!method.IsPublic) { continue; } // only interested in public fields

                        Type returnType = method.ReturnType;

                        // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                        if (returnType.IsGenericType)
                        {
                            foreach (Type genericType in returnType.GetGenericArguments())
                            {
                                usedTypes.Add(genericType);
                            }
                        }

                        if (returnType.IsArray) { returnType = returnType.GetElementType(); } // if type is array, discard array part

                        usedTypes.Add(returnType);

                        ParameterInfo[] methodParams = method.GetParameters();

                        foreach (ParameterInfo paramInfo in methodParams)
                        {
                            Type paramType = paramInfo.ParameterType;

                            // check attributes of type, if type does not contain serialisable attributes, then add as dependency

                            if (paramType.IsGenericType)
                            {
                                foreach (Type genericType in paramType.GetGenericArguments())
                                {
                                    usedTypes.Add(genericType);
                                }
                            }

                            if (paramType.IsArray) { paramType = paramType.GetElementType(); } // if type is array, discard array part

                            usedTypes.Add(paramType);
                        }
                    }


                    // check used types and add as dependencies
                    foreach (Type usedType in usedTypes)
                    {
                        string baseNamespace = usedType.Namespace;
                        if (baseNamespace == null) { continue; } // no namespace to add

                        if (blacklistNamespaces.Contains(baseNamespace)) { continue; } // blacklist namespace like System or UnityEngine
                        if (baseNamespace == partType.Namespace) { continue; } // don't add reference to namespace we're already in
                        if (!namespaces.Contains(baseNamespace)) { namespaces.Add(baseNamespace); }

                    }
                }

                foreach (string useNamespace in namespaces)
                {
                    AppendLine("using ", false);
                    AppendLine(useNamespace, false);
                    AppendLine(";");
                }
            }


            bool indented = false;
            private void AppendLine(object line, bool newLine = true)
            {
                sb.Append(line);
                if (newLine) 
                { 
                    sb.Append("\n");
                    sb.Append(indentSb);
                    indented = true;
                    return;
                }
                indented = false;

            }

            int curBrackets = 0;
            private void OpenBracket()
            {
                IncIndent();
                AppendLine("{");
                curBrackets++;
            }

            private void CloseBracket()
            {
                DecIndent();
                AppendLine("}");
                curBrackets--;
            }

            private void CloseAllBrackets()
            {
                int bracketNum = curBrackets;
                for (int i = 0; i < bracketNum; i++)
                {
                    CloseBracket();
                }
            }

            private void IncIndent()
            {
                indentSb.Append("    ");
            }

            private void DecIndent()
            {
                if (indentSb.Length < 4) { return; }
                indentSb.Remove(indentSb.Length - 4, 4);
                if (indented)
                {
                    sb.Remove(sb.Length - 4, 4);
                }
            }


            private static string GetFriendlyClass(Type type)
            {
                switch(type.ToString())
                {
                    case "UnityEngine.MonoBehaviour":
                        return "MonoBehaviour";
                    case "System.Enum":
                        return "enum";
                    case "System.Object":
                        return "object";
                    case "System.ValueType":
                        return "struct";
                    default:
                        return type.ToString();
                }
            }

            private static string GetFriendlyType(Type type)
            {
                Type decType = type.DeclaringType;
                string ret = "";
                if (type == typeof(int))
                    ret = "int";
                else if (type == typeof(short))
                    ret = "short";
                else if (type == typeof(byte))
                    ret = "byte";
                else if (type == typeof(bool))
                    ret = "bool";
                else if (type == typeof(long))
                    ret = "long";
                else if (type == typeof(float))
                    ret = "float";
                else if (type == typeof(double))
                    ret = "double";
                else if (type == typeof(decimal))
                    ret = "decimal";
                else if (type == typeof(string))
                    ret = "string";
                else if (type == typeof(void))
                    ret = "void";
                else if (type == typeof(object))
                    ret = "object";
                else if (type.IsGenericType)
                    ret = type.Name.Split('`')[0] + "<" + string.Join(", ", type.GetGenericArguments().Select(x => GetFriendlyType(x)).ToArray()) + ">";
                else if (type.IsArray)
                    ret = GetFriendlyType(type.GetElementType()) + "[]";
                else
                    ret = type.Name;


                if (decType != null && !decType.IsGenericType)
                {
                    if (decType.Name != "")
                        ret = decType.Name + "." + ret;
                }

                return ret;

            }
        }
    }
}
