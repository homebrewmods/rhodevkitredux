﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HBPartCodeGen
{
    static class CodeGenHandler
    {
        public static Assembly loadedAssembly;
        public static Assembly hbBaseAssembly;
        public static Assembly hbBaseFirstPassAssembly;
        public static Assembly modLoaderAssembly;
        public static Type SerializeAttribute;
        public static Type SerializePartAttribute;
        public static Type SerializeComponentOnlyAttribute;
        public static Type SerializePartVarAttribute;
        public static Type ModClassAttribute;
        public static void GenStubsFromAssembly(string[] args)
        {
            bool clearCache = false; // wipe codeGen cache
            bool standalone = false; // no communication with devkit

            // get assembly path
            string assemblyPath = args[1];
            if (assemblyPath == "" || Path.GetExtension(assemblyPath) != ".dll" || !File.Exists(assemblyPath))
            {
                Console.WriteLine("Error: assembly path argument not valid .dll file.");
                Console.ReadKey();
                Environment.Exit(64);
                return;
            }

            // get output path
            string codeGenPath = args[2];
            if (codeGenPath == "" || Path.GetDirectoryName(codeGenPath) == null)
            {
                Console.WriteLine("Error: output path argument not directory");
                Console.ReadKey();
                Environment.Exit(64);
                return;
            }

            // set args
            foreach (string arg in args)
            {
                switch (arg)
                {
                    case ("-clearCache"):
                        clearCache = true;
                        break;
                    case ("-standalone"):
                        standalone = true;
                        break;
                }
            }

            // clear cache
            if (clearCache)
            {
                // clear part stub cache
                foreach (FileInfo file in new DirectoryInfo(codeGenPath).GetFiles())
                {
                    file.Delete();
                }
                // clear schema cache

            }

            // if not HB base assembly, then load that from base assembly arg
            string hbAssemblyPath = "";
            string hbBaseFirstPassAssemblyPath = "";
            string modLoaderPath = "";

            string knownClassesPath = "";
            if (Path.GetFileName(assemblyPath) != "Assembly-CSharp.dll")
            {
                hbAssemblyPath = args[3];
                if (hbAssemblyPath == "" || Path.GetFileName(hbAssemblyPath) != "Assembly-CSharp.dll" || !File.Exists(hbAssemblyPath))
                {
                    Console.WriteLine("Error: assembly path argument was mod path, but HB base assembly path was not provided.");
                    Console.ReadKey();
                    Environment.Exit(64);
                    return;
                }
                hbBaseFirstPassAssemblyPath = Path.GetDirectoryName(hbAssemblyPath);
                hbBaseFirstPassAssemblyPath += "\\Assembly-CSharp-firstpass.dll";
                if (hbBaseFirstPassAssemblyPath == "" || Path.GetFileName(hbBaseFirstPassAssemblyPath) != "Assembly-CSharp-firstpass.dll" || !File.Exists(hbBaseFirstPassAssemblyPath))
                {
                    Console.WriteLine(hbBaseFirstPassAssemblyPath);

                    Console.WriteLine("Error: assembly path argument was mod path, but HB base firstpass assembly could not be found.");
                    Console.ReadKey();
                    Environment.Exit(64);
                    return;
                }

                // get modloader dll
                modLoaderPath = args[4];
                if (hbAssemblyPath == "" || Path.GetFileName(modLoaderPath) != "ModAssemblyLoader.dll" || !File.Exists(modLoaderPath))
                {
                    Console.WriteLine("Error: assembly path argument was mod path, but ModAssemblyLoader assembly path was not provided.");
                    Console.ReadKey();
                    Environment.Exit(64);
                    return;
                }
            }



            if (hbAssemblyPath != "")
            {
                hbBaseAssembly = Assembly.LoadFrom(hbAssemblyPath);
                hbBaseFirstPassAssembly = Assembly.LoadFrom(hbBaseFirstPassAssemblyPath);
                modLoaderAssembly = Assembly.LoadFrom(modLoaderPath);
                Console.WriteLine(modLoaderAssembly);
            }

            // load assembly
            loadedAssembly = Assembly.LoadFrom(assemblyPath);


            if (hbAssemblyPath != "")
            {
                SerializeAttribute = hbBaseAssembly.GetType("HBS.SerializeAttribute");
                SerializePartAttribute = hbBaseAssembly.GetType("HBS.SerializePartAttribute");
                SerializeComponentOnlyAttribute = hbBaseAssembly.GetType("HBS.SerializeComponentOnlyAttribute");
                SerializePartVarAttribute = hbBaseAssembly.GetType("HBS.SerializePartVarAttribute");
                ModClassAttribute = modLoaderAssembly.GetType("ModAssemblyLoader.ModClass");
                Console.WriteLine(ModClassAttribute);

            } else
            {
                SerializeAttribute = loadedAssembly.GetType("HBS.SerializeAttribute");
                SerializePartAttribute = loadedAssembly.GetType("HBS.SerializePartAttribute");
                SerializeComponentOnlyAttribute = loadedAssembly.GetType("HBS.SerializeComponentOnlyAttribute");
                SerializePartVarAttribute = loadedAssembly.GetType("HBS.SerializePartVarAttribute");
            }


            Type[] allTypes = loadedAssembly.GetTypes();

            List<Type> attribs = new List<Type> { SerializeAttribute, SerializePartAttribute, SerializeComponentOnlyAttribute };

            List<Type> genTypeList = new List<Type>();

            foreach (Type foundType in allTypes)
            {
                    foreach (Type attrib in attribs)
                    {
                        try
                        {
                            //Console.WriteLine(attrib);
                            if (foundType.IsDefined(attrib, true))
                            {
                                if (!foundType.IsNested)
                                {
                                    genTypeList.Add(foundType);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            Console.ReadKey();
                        }
                    }

            }




            // get list of types to skip from file
            knownClassesPath = args[5];

            List<string> knownTypeNames = null;

            if (knownClassesPath != "")
            {

                knownTypeNames = File.ReadAllLines(knownClassesPath).ToList();

                foreach (string knownType in knownTypeNames)
                {
                    Console.WriteLine(knownType);
                }


                if (File.Exists(knownClassesPath))
                {
                    File.Delete(knownClassesPath);
                    Console.WriteLine("temp file deleted");
                }

            }


            Console.WriteLine(genTypeList.Count);
            Console.WriteLine(codeGenPath);

            // dependencyTypes are types without serialiser attributes, but still necessary for serialisable classes to function
            List<Type> dependencyTypes = new List<Type>();

            // generatedTypes keeps track of all generated types thus far, for recursive dependency generation
            List<Type> generatedTypes = new List<Type>();

            // generate serialised type code

            // first pass
            foreach (Type genType in genTypeList)
            {
                SerialisedTypeGen.PartStub curStub = new SerialisedTypeGen.PartStub(genType);

                curStub.GenerateNow();

                foreach (Type type in curStub.dependencies)
                {
                    if (!dependencyTypes.Contains(type)) { dependencyTypes.Add(type); }
                }

                // only write file if it isn't already defined

                if (knownTypeNames != null)
                {
                    if (!knownTypeNames.Contains(genType.Name))
                    {
                        curStub.WriteFile(codeGenPath);
                    }
                }
                else
                {
                    curStub.WriteFile(codeGenPath); // standalone, just write the file
                }

                if (!generatedTypes.Contains(genType)) { generatedTypes.Add(genType); }
                //SerialisedTypeGen.GenerateStub(genType, codeGenPath, true);

            }



            // recursively generate dependencies
            while (dependencyTypes.Count > 0)
            {
                Type genType = dependencyTypes[0];
                if (generatedTypes.Contains(genType))
                {
                    dependencyTypes.RemoveAt(0);

                }
                else
                {


                    SerialisedTypeGen.PartStub curStub = new SerialisedTypeGen.PartStub(genType);

                    curStub.GenerateNow();

                    foreach (Type type in curStub.dependencies)
                    {
                        if (!dependencyTypes.Contains(type)) { dependencyTypes.Add(type); }

                    }

                    // only write file if it isn't already defined

                    if (knownTypeNames != null)
                    {

                        if (!knownTypeNames.Contains(genType.Name))
                        {
                            curStub.WriteFile(codeGenPath);
                        }
                    }
                    else
                    {
                        curStub.WriteFile(codeGenPath); // standalone, just write the file
                    }

                    if (!generatedTypes.Contains(genType)) { generatedTypes.Add(genType); }
                }
            }

        }


    }
}
