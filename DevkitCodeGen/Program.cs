﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HBPartCodeGen
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            foreach (string arg in args)
            {
                Console.WriteLine(arg);
            }
            //Console.ReadKey();

            //Console.WriteLine("HBPartCodeGen");



            if (args.Length == 0)
            {
                // manual mode?
                Console.WriteLine("No arguments provided");
                Console.ReadKey();
                Environment.Exit(0);
            }

            if (args[0] == "-generatestubs")
            {
                CodeGenHandler.GenStubsFromAssembly(args);
            }
            //Console.ReadKey();
        }
    }
}
