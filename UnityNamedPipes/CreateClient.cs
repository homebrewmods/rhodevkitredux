﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace UnityNamedPipes
{
    public static class UnityNamedPipes
    {
        public static UnityNamedPipe CreateClient(string pipeName, int connectTimeout)
        {
            UnityNamedPipe returnPipe = new UnityNamedPipe();

            returnPipe.client = new NamedPipeClientStream(pipeName);
            returnPipe.client.Connect(connectTimeout);
            if (returnPipe.client.IsConnected)
            {
                returnPipe.reader = new StreamReader(returnPipe.client);
                returnPipe.writer = new StreamWriter(returnPipe.client);
                return returnPipe;
            }
            return null;

            //while (true)
            //{
            //    string input = Console.ReadLine();
            //    if (String.IsNullOrEmpty(input)) break;
            //    writer.WriteLine(input);
            //    writer.Flush();
            //    Console.WriteLine(reader.ReadLine());
            //}
        }

        //public static void SendMessage(UnityNamedPipe clientPipe, string message)
        //{
        //    clientPipe.writer.WriteLine(message);
        //    clientPipe.writer.Flush();
        //    clientPipe.client.Flush();
        //}
    }

    public class UnityNamedPipe
    {
        public NamedPipeClientStream client;
        public StreamReader reader;
        public StreamWriter writer;

        public void Close()
        {
            client.Close();
        }

        public void SendMessage(string message)
        {
            writer.WriteLine(message);
            writer.Flush();
            client.Flush();
            client.Dispose();
        }

        public void ReadMessage(out string inMessage)
        {
            inMessage = reader.ReadLine();
            client.Flush();
        }

        public bool IsConnected()
        {
            return client.IsConnected;
        }

        public void WaitForPipeDrain()
        {
            client.WaitForPipeDrain();
        }
    }
}
