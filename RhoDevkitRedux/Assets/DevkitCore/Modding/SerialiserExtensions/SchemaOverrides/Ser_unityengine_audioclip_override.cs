using UnityEngine;
using UnityEngine.UI;
using System;
namespace HBS {

    [SerialiserExtensions.SchemaOverride(targetClass = typeof(AudioClip))]
    public static class Ser_unityengine_audioclip_override
    {
        public static void Ser( HBS.Writer writer, object oo ) 
        {
            if( writer.WriteNull(oo)) { return; }
            UnityEngine.AudioClip o = (UnityEngine.AudioClip)oo;
            writer.Write(o.name);
        }

        public static object Res( HBS.Reader reader, object o = null) 
        {
            if(reader.ReadNull()){ return null; }

            string clipName = (string)reader.Read();

            AudioClip ret = Resources.Load<UnityEngine.AudioClip>("Audio/" + clipName);
            if (ret == null)
            {
                ret = AudioClip.Create(clipName,1,1,1,false);
                ret.name = clipName;
            }
            return ret;
        }
    }
}
