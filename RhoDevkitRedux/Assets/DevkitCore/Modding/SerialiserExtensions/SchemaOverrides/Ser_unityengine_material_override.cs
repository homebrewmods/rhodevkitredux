using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;
using HarmonyLib;

namespace HBS {

    [SerialiserExtensions.SchemaOverride(targetClass = typeof(Material))]
    public static class Ser_unityengine_material_override
    {
        public static void Ser(HBS.Writer writer, object oo)
        {
            if (writer.WriteNull(oo)) { return; }
            UnityEngine.Material o = (UnityEngine.Material)oo;
            writer.Write(o.name);
        }

        public static object Res(HBS.Reader reader, object o = null)
        {
            if(reader.ReadNull()){return null; }
            //return (object)Resources.Load<UnityEngine.Material>((string)reader.Read());

            string matName = (string)reader.Read();
            string[] guids = AssetDatabase.FindAssets(matName + " t:material");

            foreach (string guid in guids)
            {
                return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
            }

            // material not found, create new material to save material name

            Material tempMaterial = new Material(MaterialExtension.missingMaterial);
            tempMaterial.name = matName;
            return tempMaterial;
        }
    }
}
