﻿using HarmonyLib;
using HBS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SerialiserExtensions
{

	// devkit stuff
#if UNITY_EDITOR
	[InitializeOnLoad]
	public static class SchemaOverrideEditor
	{
		static SchemaOverrideEditor()
		{
			Debug.Log("DevkitMaterialLoader loaded");


            //Assembly targetAssembly = null;
            //foreach (Assembly loadedAssembly in AppDomain.CurrentDomain.GetAssemblies())
            //{
            //    if (loadedAssembly.GetName().Name != "Assembly-CSharp") { continue; } // skip if not on allowed assemblies
            //    targetAssembly = loadedAssembly;
            //}

            //if (targetAssembly == null) { return; }

            List<Type> overrideSchemas = SchemaOverride.GetSchemaOverrides(Assembly.GetExecutingAssembly());

            foreach (Type overrideSchema in overrideSchemas)
            {
                Debug.Log("overriding schema " + overrideSchema.Name);
                SchemaOverride.ApplySchemaOverride(overrideSchema);
            }


            // for each type with SchemaOverrideAttribute force override schema binding with target

            //private static void GetAssemblies()
            //{
            //	loadedAssemblies = new List<Assembly>();
            //	foreach (Assembly loadedAssembly in AppDomain.CurrentDomain.GetAssemblies())
            //	{
            //		if (!serialisableAssemblies.Contains(loadedAssembly.GetName().Name)) { continue; } // skip if not on allowed assemblies
            //		loadedAssemblies.Add(loadedAssembly);
            //	}
            //}


            //private static void GetClasses(Assembly a)
            //{
            //	Type[] types = a.GetExportedTypes();
            //	foreach (Type exportedType in types)
            //	{
            //		// blacklisted?
            //		if (!IsValidType(exportedType)) { continue; }

            //		// appropriate fields/properties?
            //		if (GetFields(exportedType, false).Length == 0 && GetProperties(exportedType, false).Length == 0) { continue; }

            //		exportTypes.Add(exportedType); // valid type, serialise it
            //	}
            //}

        }
	}
#endif


	public static class SchemaOverride
	{
		public static List<Type> GetSchemaOverrides(Assembly fromAssembly)
        {
			List<Type> overrideSchemas = new List<Type>();
            Type[] types = fromAssembly.GetExportedTypes();

            foreach (Type exportedType in types)
            {
				if (!exportedType.IsDefined(typeof(SchemaOverrideAttribute), false)) { continue; }

				SchemaOverrideAttribute attrib = exportedType.GetCustomAttributes(typeof(SchemaOverrideAttribute), true).FirstOrDefault() as SchemaOverrideAttribute;
                Type targetClass = attrib.targetClass;

                if (targetClass != null) // get schema, add if valid
                {
                    if (exportedType.GetMethod("Ser") != null && exportedType.GetMethod("Res") != null)
                    {
						overrideSchemas.Add(exportedType);
					}
                }
            }

			return overrideSchemas;
        }

		public static void ApplySchemaOverride(Type overrideSchema)
		{
			if (overrideSchema.GetMethod("Ser") != null && overrideSchema.GetMethod("Res") != null)
			{
				SchemaOverrideAttribute attrib = overrideSchema.GetCustomAttributes(typeof(SchemaOverrideAttribute), true).FirstOrDefault() as SchemaOverrideAttribute;
				Type targetClass = attrib.targetClass;

				MethodInfo Ser = overrideSchema.GetMethod("Ser");
				MethodInfo Res = overrideSchema.GetMethod("Res");

				Action<Writer, object> serDelegate = (Action<Writer, object>)Delegate.CreateDelegate(typeof(Action<Writer, object>), null, Ser);
				Func<Reader, object, object> resDelegate = (Func<Reader, object, object>)Delegate.CreateDelegate(typeof(Func<Reader, object, object>), null, Res);

				if (SerializerBinder.bindsSer.ContainsKey(targetClass)) { SerializerBinder.bindsSer.Remove(targetClass); }
				SerializerBinder.bindsSer.Add(targetClass, serDelegate);
				if (SerializerBinder.bindsRes.ContainsKey(targetClass)) { SerializerBinder.bindsRes.Remove(targetClass); }
				SerializerBinder.bindsRes.Add(targetClass, resDelegate);
			}
		}

	}




	public class SchemaOverrideAttribute : Attribute
	{
		public Type targetSchema;
		public Type targetClass;
	}
}