﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using HBS;
using HarmonyLib;

namespace ModAssemblyLoader
{

    // adds mod type lookup handler
    public static class Modding
    {
        public static Dictionary<string, Type> modAssemblies = new Dictionary<string, Type>();
        public static void Inject(Type modType)
        {
            modAssemblies[modType.Name] = modType;
        }
    }

    #region serializer_patches

    [HarmonyPatch(typeof(HBS.Serializer.GameObjects), "ReadGameObjectStructureChunk")]
    public static class ReadGameObjectStructureChunk_Patch
    {
        // patch in new method
        static bool Prefix(int prefix, Reader r, GameObject root, out GameObject outRoot, GameObject[] objs, int fromIndex, int length)
        {
            outRoot = root;

            HBS.Serializer.currentRoot = root;//set current root ( UnserializePath uses this global var ) 

            for (var i = fromIndex; i < fromIndex + length; i++)
            {
                objs[i] = new GameObject("Child" + i.ToString());
                if (i == 0 && root == null)
                {
                    root = objs[i];
                    HBS.Serializer.currentRoot = root; //set current root ( UnserializePath uses this global var )
                    outRoot = root;
                    objs[i].SetActive(false);
                    objs[i].transform.parent = root.transform;
                }
                else
                {
                    var parentFromPath = HBS.Serializer.UnserializePath(r);
                    if (parentFromPath != null)
                    {
                        objs[i].transform.SetParent((Transform)parentFromPath);
                    }
                    else
                    {
                        Debug.LogError("coudnt find path for " + objs[i].ToString());
                        objs[i].transform.parent = HBS.Serializer.currentRoot.transform;
                    }
                }
                try
                {
                    var componentCount = (int)r.Read();
                    for (var ii = 0; ii < componentCount; ii++)
                    {
                        var typeName = (string)r.Read();
                        Type ctype;
                        if (Modding.modAssemblies.ContainsKey(typeName))
                        {
                            ctype = Modding.modAssemblies[typeName];
                        }
                        else
                        {
                            ctype = Type.GetType(typeName + ",Assembly-CSharp");
                        }
                        //var ctype = Modding.modAssemblies.ContainsKey(typeName) ? Modding.modAssemblies[typeName] : Type.GetType(typeName + ",Assembly-CSharp"); // check mod assemblies first, the lookup is extremely fast and allows for replacement
                        //var ctype = Type.GetType(typeName + ",Assembly-CSharp");
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine.UI"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",HBNetworking"); }
                        if (ctype != typeof(Transform))
                        {
                            if (objs[i].AddComponent(ctype) == null)
                            {;
                                Debug.Log("failed to add " + typeName + ", adding MissingClass instead to preserve structure");
                                objs[i].AddComponent<MissingClass>();
                            }
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning(e.ToString());
                }
            }
            return false; // skip old serializer method completely
        }
    }

    [HarmonyPatch(typeof(HBS.Serializer.GameObjects), "ReadComponentChunk")]
    public static class ReadComponentChunk_Patch
    {
        // patch in new method
        static bool Prefix(int prefix, Reader r, GameObject root, int fromIndex, int length, int componentCount, Component[] comps)
        {

            HBS.Serializer.currentRoot = root;//set current root ( UnserializePath uses this global var )       

            if (prefix > 0)
            {

                for (var ii = fromIndex; ii < fromIndex + length; ii++)
                {
                    var typeName = (string)r.Read();
                    var compData = (byte[])r.Read();
                    try
                    {
                        var r2 = new Reader(compData);

                        Type ctype;
                        if (Modding.modAssemblies.ContainsKey(typeName))
                        {
                            ctype = Modding.modAssemblies[typeName];
                        } else
                        {
                            ctype = Type.GetType(typeName + ",Assembly-CSharp");
                        }

                        //var ctype = Modding.modAssemblies.ContainsKey(typeName) ? Modding.modAssemblies[typeName] : Type.GetType(typeName + ",Assembly-CSharp"); // check mod assemblies first, the lookup is extremely fast and allows for replacement
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine.UI"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",HBNetworking"); }
                        var c = comps[ii];//objs[i].GetComponent(ctype);
                                          //Debug.Log(comps[ii].GetType());
                                          //Debug.Log(ctype);
                                          //Debug.Log(typeName);
                                          //Debug.Log(ctype == comps[ii].GetType());

                        HBS.Serializer.Unserialize(r2, ctype, (object)c);
                        r2.Close();
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning(e.ToString());
                    }
                }

            }
            else
            {

                for (var ii = fromIndex; ii < fromIndex + length; ii++)
                {
                    var typeName = (string)r.Read();
                    var ctype = Type.GetType(typeName);
                    var c = comps[ii];
                    HBS.Serializer.Unserialize(r, ctype, (object)c);
                }

            }

            return false; // skip old serializer method completely
        }
    }


    [HarmonyPatch(typeof(HBS.SerializerBinder), "Serialize")]
    public static class SerializerBinder_Serialize_Patch
    {

        static bool Prefix(HBS.Writer writer, object o)
        {

            if (writer.WriteNull(o))
            {
                return false;
            }

            Type objType = o.GetType();

            writer.Write('>');

            if (!HBS.SerializerBinder.bindsSer.ContainsKey(objType))
            {
                // reflected schema
                ReflectedSchema.ReflectedSerialise(writer, o);
                return false;
            }

            HBS.SerializerBinder.bindsSer[o.GetType()].Invoke(writer, o);

            return false; // skip old serializer method completely
        }


    }

    [HarmonyPatch(typeof(HBS.SerializerBinder), "Unserialize")]
    public static class SerializerBinder_Unserialize_Patch
    {

        //public static object Unserialize(HBS.Reader reader, Type t, object o = null)
        static bool Prefix(HBS.Reader reader, Type t, object o = null)
        {
            if (reader.ReadNull()) { return false; }
            if (reader.ReadNull()) { return false; }
            // check if bindings doesn't contain binding
            Type objType = o.GetType();

            if (!HBS.SerializerBinder.bindsRes.ContainsKey(objType))
            {
                // reflected schema
                ReflectedSchema.ReflectedDeserialise(reader, o);
                return false;
            }

            HBS.SerializerBinder.bindsRes[objType].Invoke(reader, o);

            return false; // skip old serializer method completely
        }
    }

    [HarmonyPatch(typeof(HBS.Serializer.GameObjects), "GetComponents")]
    public static class Serializer_GetComponents_Patch
    {
        static bool Prefix(GameObject o, ref List<Component> __result)
        {
            var ret = new List<Component>();
            //only return components that have binders or are serialisable types


            var comps = o.GetComponents<Component>();
            foreach (var c in comps)
            {
                if (c == null) { continue; }
                if (SerializerBinder.bindsSer.ContainsKey(c.GetType()) || c.GetType().IsDefined(typeof(HBS.SerializeAttribute), false) || c.GetType().IsDefined(typeof(HBS.SerializeComponentOnlyAttribute), false) || c.GetType().IsDefined(typeof(HBS.SerializePartAttribute), false))
                {
                    ret.Add(c);
                }
            }
            __result = ret;
            return false;
        }
    }

    #endregion
}
