﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace ModAssemblyLoader
{
    public class BuilderField : Attribute
    {
        public string name;
        public string toolTip;
        public float minValue;
        public float maxValue;
        public string buiOptionsOverride;
    }


    public static class BuilderFieldHelper
    {

        public static List<FieldInfo> GetBuilderFields(object target)
        {
            Debug.Log(target.GetType());

            FieldInfo[] fields = target.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);

            List<FieldInfo> adjustableMembers = new List<FieldInfo>();

            foreach (FieldInfo field in fields)
            {
                if (!field.IsDefined(typeof(BuilderField), false))
                {
                    continue; // this member is not builder adjustable
                }
                adjustableMembers.Add(field);
            }
            return adjustableMembers;
        }


        public static BuilderField GetBuilderFieldAttribute(FieldInfo targetField)
        {
            BuilderField attrib = targetField.GetCustomAttributes(typeof(BuilderField), true).FirstOrDefault() as BuilderField;
            return attrib;
        }

        public static FieldInfo SetFieldValue(object target, FieldInfo targetField, object value)
        {
            targetField.SetValue(target, Convert.ChangeType(value, targetField.FieldType));
            return targetField;
        }


    }
}
