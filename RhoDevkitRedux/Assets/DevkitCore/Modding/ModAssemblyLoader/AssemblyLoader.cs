﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Reflection;
using HBS;
using System.ComponentModel;

namespace ModAssemblyLoader
{
    /// <summary>
    /// Flags that the class should be loaded as a serializable class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ModClass : Attribute
    {
        /// <summary>
        /// Flags that the class should be loaded as a serializable class.
        /// <paramref name="schema"/>
        /// <param name="schema">
        /// TODO: add schema description
        /// </param>
        /// </summary>
        public ModClass(Type schema) { }
        public ModClass() { }
        public Type schema;
    }

    // find all assemblies in folder, load each one, check for custom attributes
    // if attribute == AssemblyModLoader.ModClass then add this to the serializer lookup
    // attribute should have parameter pointing to schema class
    public static class Loader
    {
        public static string assemblyDir = HBU.GetLuaFolder() + "/ModAssemblies";
        public static void LoadAssemblies()
        {
            if (!Directory.Exists(assemblyDir))
            {
                Directory.CreateDirectory(assemblyDir);
            }
            // all mod assemblies found
            string[] assemblyFiles = Directory.GetFiles(assemblyDir, "*.dll", SearchOption.AllDirectories);

            foreach (string assemblyFile in assemblyFiles)
            {
                // load each assembly file
                Debug.Log(assemblyFile);
                var modAssembly = Assembly.LoadFrom(assemblyFile);

                var types = from type in modAssembly.GetTypes()
                            where Attribute.IsDefined(type, typeof(ModClass))
                            select type;

                foreach (var type in types)
                {
                    var attrib = type.GetCustomAttributes(typeof(ModClass), true).FirstOrDefault() as ModClass;
                    Type schemaClass = attrib.schema;
                    bool validSchema = false;


                    if (schemaClass != null) // get schema, add if valid
                    {
                        if (schemaClass.GetMethod("Ser") != null && schemaClass.GetMethod("Res") != null)
                        {
                            var Ser = schemaClass.GetMethod("Ser");
                            var Res = schemaClass.GetMethod("Res");

                            Action<Writer, object> serDelegate = (Action<Writer, object>)Delegate.CreateDelegate(typeof(Action<Writer, object>), null, Ser);
                            Func<Reader, object, object> resDelegate = (Func<Reader, object, object>)Delegate.CreateDelegate(typeof(Func<Reader, object, object>), null, Res);

                            SerializerBinder.bindsSer.Add(type, serDelegate);
                            SerializerBinder.bindsRes.Add(type, resDelegate);

                            validSchema = true;
                        }
                    }

                    if (validSchema)
                    {
                        Modding.Inject(type);
                        Debug.Log("added " + type.Name + " to serializer");
                    }
                    else
                    {

                        // reflected fallback
                        Modding.Inject(type);
                        Debug.Log("found mod class " + type.Name + " but no schema found, using ReflectedSchema");
                    }

                }
            }

        }
    }

}
