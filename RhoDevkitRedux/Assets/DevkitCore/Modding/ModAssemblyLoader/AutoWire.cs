﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace ModAssemblyLoader
{
    //private List<Type> 

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class AutoWireAttribute : Attribute
    {
		public string tunerName;
    }

	public static class AutoWire
	{
		public static void SetupAutoWires(object target)
        {
            // create lookup for properties to link to wires
            Dictionary<string, Property> propLookup = new Dictionary<string, Property>(); 
            foreach (Property prop in ((Part)target).properties) { propLookup.Add(prop.pname, prop); }

            Component[] comps = ((Component)target).gameObject.GetComponents<Component>(); // get all comps to try find wirebase

            foreach (FieldInfo field in target.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)) // find all private fields with AutoWire attribute
            {
                if (!field.IsDefined(typeof(AutoWireAttribute), false)) { continue; }// not an AutoWire

                if (!propLookup.ContainsKey(field.Name)) { continue; } // doesn't have a matching property

                if (field.FieldType.DeclaringType != null)
                {
                    if (!field.FieldType.DeclaringType.IsSubclassOf(typeof(HBLink))) { continue; } // declaring type doesn't inherit from HBLink
                }

                // get components on this object, to hopefully find wirebases
                foreach (Component comp in comps)
                {
                    if (field.FieldType.DeclaringType == comp.GetType()) 
                    {
                        CreateWire(field, target, comp, propLookup[field.Name]); // found wirebase, create wire
                    }
                }
            }
        }

        private static void CreateWire(FieldInfo field, object target, object targetWireBase, Property prop)
        {
            string methodName = prop.type == PropertyType.POutput ? "NewOutput" : "NewInput"; // either input or output, hopefully.
            MethodBase creatorMethod = field.FieldType.DeclaringType.GetMethod(methodName, new Type[1] { typeof(Property) }); // find appropriate wire creator method
            field.SetValue(target, creatorMethod.Invoke(targetWireBase, new object[1] { prop })); // ... and invoke the method on the wirebase
        }
    }
}