﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace RhoDevktRedux
{
	[InitializeOnLoad]
	public static class ModAssemblyManager
	{
		static ModAssemblyManager()
		{
			Debug.Log("ModAssemblyManager loaded");

			//string[] assemblyFiles = GetHBModAssemblies();

			//foreach (string assemblyFile in assemblyFiles)
			//{
			//	Debug.Log(assemblyFile);
			//	GenerateModAssemblyStubs(assemblyFile);
			//}
		}

		public static void GenerateModStubs()
        {
			string[] assemblyFiles = GetHBModAssemblies();

			foreach (string assemblyFile in assemblyFiles)
			{
				Debug.Log(assemblyFile);
				GenerateModAssemblyStubs(assemblyFile);
			}
		}

		static string[] GetHBModAssemblies()
        {
			string HBModLuaPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).Replace("Roaming", "LocalLow");
			HBModLuaPath += "\\CopyBugPaste\\Homebrew14\\Lua\\00001";



			string ModAssemblyPath = HBModLuaPath + "\\ModAssemblies";

			string[] assemblyFiles = Directory.GetFiles(ModAssemblyPath, "*.dll", SearchOption.AllDirectories);

			return assemblyFiles;
		}

		static void GenerateModAssemblyStubs(string assemblyPath)
        {


            // if HB path not found then prompt user

            string modAssemblyStubCodePath = Application.dataPath + "/GeneratedCode/";
			modAssemblyStubCodePath += Path.GetFileNameWithoutExtension(assemblyPath);
			Debug.Log(modAssemblyStubCodePath);

            if (!Directory.Exists(modAssemblyStubCodePath))
            {
                Directory.CreateDirectory(modAssemblyStubCodePath);
            }

            // clear cache

            // clear part stub cache
            foreach (FileInfo file in new DirectoryInfo(modAssemblyStubCodePath).GetFiles())
            {
                file.Delete();
            }

            GeneratePartCode.InvokeGenerator(assemblyPath, modAssemblyStubCodePath);

        }
	}
}