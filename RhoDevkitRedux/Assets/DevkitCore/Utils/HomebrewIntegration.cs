﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace RhoDevktRedux
{
    public static class HomebrewIntegration
    {

        [MenuItem("Devkit/Settings/Set Homebrew directory")]
        public static void SetHomebrewDirectory()
        {
            string dir = UnityEditor.EditorUtility.OpenFolderPanel("Open folder containing HB.exe", Application.streamingAssetsPath + "/Assets/GameObjects/", "");
            string hbDataDir = dir;
            if (dir != "" && File.Exists(dir + "/HB.exe"))
            {
                hbDataDir += "/HB_Data"; // set data dir (depends on beta or not)
                EditorUtility.DisplayDialog("Homebrew install successfully detected.", "This location will be used as the Homebrew working directory.\n\nThis will be saved to the Devkit.\n\nYou can always set a new Homebrew directory by running this utility again.", "Close");
            }
            else if (dir != "" && File.Exists(dir + "/HB-beta.exe"))
            {
                hbDataDir += "/HB-beta_Data"; // set data dir (depends on beta or not)
                EditorUtility.DisplayDialog("Homebrew beta install successfully detected.", "This location will be used as the Homebrew working directory.\n\nThis will be saved to the Devkit.\n\nYou can always set a new Homebrew directory by running this utility again.", "Close");
            }
            else
            {
                EditorUtility.DisplayDialog("HB.exe or HB-beta.exe not found.", "Please Select root Homebrew install folder\n\nThe root folder is the folder which contains HB.exe or HB-beta.exe", "Close");
                return;
            }

            // found correct folder, apply settings
            DevkitSettingsManager.SetSetting("homebrewDirectory", dir, false);
            DevkitSettingsManager.SetSetting("homebrewDataDirectory", hbDataDir);
        }
    }
}