﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;
using System.Reflection;

[CustomEditor(typeof(Part), true)]
public class PartPropertyEditor : Editor
{

    SerializedProperty prop;
    ReorderableList list;

    private void OnEnable()
    {
        prop = serializedObject.FindProperty("properties");
        list = CreateList(serializedObject, prop);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        base.DrawDefaultInspector();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

    public enum PropertyTypeFiltered
    {
        NoneSelected = 0,
        Bool = 1,
        Int = 2,
        Float = 3,
        Text = 4,
        //Password = 5,
        //Email = 6,
        //Dialoge = 7,
        //Slider = 8,
        WireInput = 9,
        WireOutput = 10,
        //Button = 11,
        //Enum = 12,
        KeybindOutput = 13,
        //Asset = 14,
    }

    public enum PropertyLinkTypeFiltered
    {
        Fuel = 0,
        Float = 1,
        Electric = 2,
        //Driveshaft = 3,
        SimpleDriveshaft = 4,
        Air = 5,
        Joint = 6,
        Ammo = 7,
        Vector = 8,
        Seat = 9,
    }

    //public enum PropertyCollapseName
    //{
    //    Properties,
    //    Controls,
    //    Inputs,
    //    Outputs,
    //    None
    //}

    public Hashtable CompatibleTypes = new Hashtable()
    {
        {PropertyType.Bool, PropertyCollapseName.Properties},
        {PropertyType.Int, PropertyCollapseName.Properties},
        {PropertyType.Float, PropertyCollapseName.Properties},
        {PropertyType.Text, PropertyCollapseName.Properties},
        {PropertyType.PInput, PropertyCollapseName.Inputs},
        {PropertyType.POutput, PropertyCollapseName.Outputs},
        //{PropertyType.Button, PropertyCollapseName.Properties},
        {PropertyType.KeybindOutput, PropertyCollapseName.Controls},
    };

    ReorderableList CreateList(SerializedObject obj, SerializedProperty prop)
    {
        ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);
        Texture2D tex = new Texture2D(1, 1);
        tex.SetPixel(0, 0, new Color(0.33f, 0.66f, 1f, 0.66f));
        tex.Apply();

        list.drawHeaderCallback = rect => {
            EditorGUI.LabelField(rect, "Properties");
        };

        List<float> heights = new List<float>(prop.arraySize);


        list.drawElementCallback = (rect, index, active, focused) => {

            if (index == -1) { return; }
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

            float margin = 4f;
            float height = EditorGUIUtility.singleLineHeight * 1.25f;
            bool foldout = active;

            if (foldout)
            {
                var indentation = 200;
                var line = margin;
                // name
                EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Property name");
                EditorGUI.PropertyField(
                    new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("pname"),
                    GUIContent.none
                );
                line += EditorGUIUtility.singleLineHeight;

                // descriptive name
                EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Descriptive name");
                EditorGUI.PropertyField(
                    new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("descriptiveName"),
                    GUIContent.none
                );
                line += EditorGUIUtility.singleLineHeight;

                // collapser name (property class) PropertyCollapseName
                EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Collapser name (property class)");
                EditorGUI.PropertyField(
                    new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("collapserName"),
                    GUIContent.none
                );

                // if collapser name gets set to something other than wire input/output, but type is still wire input/output, change that to none selected
                //Debug.Log((PropertyType)element.FindPropertyRelative("type").intValue);

                if (CompatibleTypes.ContainsKey((PropertyType)element.FindPropertyRelative("type").intValue))
                {
                    var expectedCollapser = (PropertyCollapseName)CompatibleTypes[(PropertyType)element.FindPropertyRelative("type").intValue];
                    var currentCollapser = (PropertyCollapseName)element.FindPropertyRelative("collapserName").intValue;
                    if (currentCollapser != expectedCollapser)
                    {
                        element.FindPropertyRelative("type").intValue = 0; // set as type unselected
                    }

                }
                line += EditorGUIUtility.singleLineHeight;

                // property type
                EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Property type");


                // check if the property is one the reorderable list supports, to clean up the list
                var propType = (PropertyTypeFiltered)0;
                if (System.Enum.IsDefined(typeof(PropertyTypeFiltered), element.FindPropertyRelative("type").intValue))
                {
                    var selected = (PropertyTypeFiltered)element.FindPropertyRelative("type").intValue;
                    propType = (PropertyTypeFiltered)EditorGUI.EnumPopup(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        selected,
                        EditorStyles.popup
                    );
                    element.FindPropertyRelative("type").intValue = (int)propType;
                    //Debug.Log((int)display);
                }
                else
                {
                    // unknown type
                    EditorGUI.LabelField(new Rect(rect.x + indentation, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), Enum.GetName(typeof(PropertyType), element.FindPropertyRelative("type").intValue) + " (unknown type)");
                }
                line += EditorGUIUtility.singleLineHeight;



                // type handling
                if (propType == PropertyTypeFiltered.WireInput || propType == PropertyTypeFiltered.WireOutput)
                {
                    // it's a wire
                    // set property class accordingly
                    if (propType == PropertyTypeFiltered.WireInput)
                    {
                        element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Inputs; // wire input selected, set class as input
                    }
                    else
                    {
                        element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Outputs;// wire output selected, set class as output
                    }

                    // add link type parameter
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Tuner link type");
                    var linkType = (PropertyLinkTypeFiltered)0;
                    if (System.Enum.IsDefined(typeof(PropertyLinkTypeFiltered), element.FindPropertyRelative("linkType").intValue))
                    {
                        var selected = (PropertyLinkTypeFiltered)element.FindPropertyRelative("linkType").intValue;
                        linkType = (PropertyLinkTypeFiltered)EditorGUI.EnumPopup(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                            selected,
                            EditorStyles.popup
                        );
                        element.FindPropertyRelative("linkType").intValue = (int)linkType;
                    }
                    else
                    {
                        // unknown type
                        EditorGUI.LabelField(new Rect(rect.x + indentation, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), Enum.GetName(typeof(PropertyLinkType), element.FindPropertyRelative("linkType").intValue) + " (unknown link type)");
                    }
                    line += EditorGUIUtility.singleLineHeight;
                }
                else if (propType == PropertyTypeFiltered.Bool)
                {

                    element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Properties; // set as property

                    // add bool checkbox
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Bool value");
                    var selected = element.FindPropertyRelative("value").stringValue == "True" ? true : false;
                    var value = EditorGUI.Toggle(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        selected,
                        EditorStyles.toggle
                    );
                    element.FindPropertyRelative("value").stringValue = value == true ? "True" : "False";
                    line += EditorGUIUtility.singleLineHeight;

                }
                else if (propType == PropertyTypeFiltered.Int)
                {
#pragma warning disable CS0168 // Variable is declared but never used, disable because we're just using a generic catch
                    element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Properties; // set as property

                    // add int value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Int value");
                    int value = 0;
                    try { value = int.Parse(element.FindPropertyRelative("value").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.IntField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("value").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;


                    // add min value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Min value");
                    value = 0;
                    try { value = int.Parse(element.FindPropertyRelative("minValue").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.IntField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("minValue").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;

                    // add max value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Max value");
                    value = 0;
                    try { value = int.Parse(element.FindPropertyRelative("maxValue").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.IntField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("maxValue").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;
#pragma warning restore CS0168 // see above
                }
                else if (propType == PropertyTypeFiltered.Float)
                {
#pragma warning disable CS0168 // Variable is declared but never used, disable because we're just using a generic catch
                    element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Properties; // set as property

                    // add float value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Float value");
                    float value = 0;
                    try { value = float.Parse(element.FindPropertyRelative("value").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.FloatField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("value").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;


                    // add min value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Min value");
                    value = 0;
                    try { value = float.Parse(element.FindPropertyRelative("minValue").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.FloatField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("minValue").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;

                    // add max value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Max value");
                    value = 0;
                    try { value = float.Parse(element.FindPropertyRelative("maxValue").stringValue); } catch (FormatException e) { }
                    value = EditorGUI.FloatField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.numberField
                    );
                    element.FindPropertyRelative("maxValue").stringValue = value.ToString();
                    line += EditorGUIUtility.singleLineHeight;
#pragma warning restore CS0168 // see above
                }
                else if (propType == PropertyTypeFiltered.Text)
                {
                    element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Properties; // set as property

                    // add text value
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + line, indentation, EditorGUIUtility.singleLineHeight), "Text value");
                    string value = "";
                    value = element.FindPropertyRelative("value").stringValue;
                    value = EditorGUI.TextField(new Rect(rect.x + indentation, rect.y + line, 300, EditorGUIUtility.singleLineHeight),
                        value,
                        EditorStyles.textField
                    );
                    element.FindPropertyRelative("value").stringValue = value;
                    line += EditorGUIUtility.singleLineHeight;

                }
                else if (propType == PropertyTypeFiltered.KeybindOutput)
                {
                    element.FindPropertyRelative("collapserName").intValue = (int)PropertyCollapseName.Controls; // set as control input
                }

                // copy button
                if(GUI.Button(new Rect(rect.x + indentation, rect.y + line, 150, EditorGUIUtility.singleLineHeight), "Copy"))
                {


                    PartPropertyHelper.copiedProp = new Property(((Part)target).properties[index]);

                    EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Copied '" + PartPropertyHelper.copiedProp.pname + "' property.");
                }

                // paste button, disable if there's nothing to paste
                if (PartPropertyHelper.copiedProp == null)
                    GUI.enabled = false;
                if (GUI.Button(new Rect(rect.x + indentation + 150, rect.y + line, 150, EditorGUIUtility.singleLineHeight), "Paste (overwrite)"))
                {
                    ((Part)target).properties[index] = PartPropertyHelper.copiedProp;
                    EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Pasted '" + PartPropertyHelper.copiedProp.pname + "' property.");
                }
                GUI.enabled = true;

                line += EditorGUIUtility.singleLineHeight;


                height = line + margin;
            }
            else
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 300, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("pname").stringValue!="" ? element.FindPropertyRelative("pname").stringValue : "Unnamed property");
            }


            try
            {
                heights[index] = height;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Debug.LogWarning(e.Message);
            }

            rect.height = (height / 5) * 4;
            rect.width = rect.width / 2 - margin / 2;
            rect.x += rect.width + margin;
        };

        list.elementHeightCallback = (index) => {
            if (index == -1) { return EditorGUIUtility.singleLineHeight; }
            float height = 0;
            try
            {
                height = heights[index];
            }
            catch (ArgumentOutOfRangeException) {}
            finally
            {
                // trim size but keep data
                float[] floats = heights.ToArray();
                Array.Resize(ref floats, prop.arraySize);
                heights = floats.ToList();
            }

            return height;
        };

        list.drawElementBackgroundCallback = (rect, index, active, focused) => {
            if (index == -1) { return; }
            rect.height = heights[index];
            if (active)
                GUI.DrawTexture(rect, tex as Texture);
        };

        list.onAddDropdownCallback = (rect, li) => {
            var menu = new GenericMenu();
            menu.AddItem(new GUIContent("Add property"), false, () => {
                serializedObject.Update();
                li.serializedProperty.arraySize++;
                serializedObject.ApplyModifiedProperties();
            });

            menu.AddItem(new GUIContent("Copy all properties"), false, () => {

                PartPropertyHelper.copiedProps = new List<Property>();

                foreach (Property targetProp in ((Part)target).properties)
                {
                    PartPropertyHelper.copiedProps.Add(new Property(targetProp));
                }

                EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Copied all properties.");
            });

            if (PartPropertyHelper.copiedProps != null)
            {
                menu.AddItem(new GUIContent("Paste all properties (will overwrite)"), false, () =>
                {
                    ((Part)target).properties = PartPropertyHelper.copiedProps;
                    EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Pasted all properties.");
                    serializedObject.Update();
                    serializedObject.ApplyModifiedProperties();
                    EditorNotifications.RepaintWindow(EditorNotifications.EditorWindowTypes.InspectorWindow);
                    return;
                });
            }
            if (PartPropertyHelper.copiedProp != null)
            {
                menu.AddItem(new GUIContent("Paste '" + PartPropertyHelper.copiedProp.pname + "' property as new"), false, () =>
                {
                    ((Part)target).properties.Add(PartPropertyHelper.copiedProp);
                    EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Pasted '" + PartPropertyHelper.copiedProp.pname + "' property.");
                    serializedObject.Update();
                    serializedObject.ApplyModifiedProperties();
                    EditorNotifications.RepaintWindow(EditorNotifications.EditorWindowTypes.InspectorWindow);
                    return;

                });
            }

            menu.AddItem(new GUIContent("Generate properties from part"), false, () => {

                PartPropertyHelper.copiedProps = ((Part)target).properties;

                ((Part)target).properties = PartPropertyHelper.AutoGetProperties(target.GetType());

                Debug.Log(target.GetType());

                EditorNotifications.ShowNotification(EditorNotifications.EditorWindowTypes.InspectorWindow, "Generated properties automatically");
            });

            menu.ShowAsContext();
        };

        return list;
    }
}

public class PartPropertyHelper
{

    #region copyPaste
    private static List<Property> _copiedProps;
    public static List<Property> copiedProps
    {
        get
        {
            return _copiedProps;
        }
        set
        {
            _copiedProps = value;
            _copiedProp = null;
        }

    }

    private static Property _copiedProp;
    public static Property copiedProp
    {
        get
        {
            return _copiedProp;
        }
        set
        {
            _copiedProp = value;
            _copiedProps = null;
        }
    }

    public static Property CloneProperty(Property prop)
    {
        return new Property(prop);
    }

    #endregion

    // default property example for each link type

    private static Dictionary<Type, Property> defaultProps = new Dictionary<Type, Property>
    {
        { typeof(FloatWire.FloatWireInput),
            new Property
            {
                collapserName = PropertyCollapseName.Inputs,
                type = PropertyType.PInput,
                linkType = PropertyLinkType.Float,
            }
        }
    };


    // search all wire fields to auto add properties

    public static List<Property> AutoGetProperties(Type partType)
    {
        // check for floatwire in, out

        //List<FieldInfo> fields = new List<FieldInfo>();

        List<Property> ret = new List<Property>();

        FieldInfo[] fields = partType.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

        foreach (KeyValuePair<Type, Property> entry in defaultProps)
        {
            foreach (FieldInfo field in fields)
            {
                Debug.Log(field);
                if (field.FieldType == entry.Key)
                {
                    Property prop = new Property();

                    prop.pname = field.Name;

                    string dName = field.Name;

                    if (entry.Value.collapserName == PropertyCollapseName.Inputs)
                    {
                        if (dName.EndsWith("Input"))
                            dName = dName.Substring(0, dName.Length - 5);
                    }
                    else if (entry.Value.collapserName == PropertyCollapseName.Outputs)
                    {
                        if (dName.EndsWith("Output"))
                            dName = dName.Substring(0, dName.Length - 6);
                    }

                    prop.descriptiveName = dName;

                    ret.Add(prop);
                }
            }
        }
        return ret;

    }



    public static List<FieldInfo> GetFields(Type targetType, Type linkType)
    {
        FieldInfo[] fields = targetType.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

        List<FieldInfo> ret = new List<FieldInfo>();

        foreach (FieldInfo field in fields)
        {
            if (field.FieldType == linkType)
            {
                ret.Add(field);
            }
        }

        return ret;
    }



}