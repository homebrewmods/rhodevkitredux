﻿using UnityEngine;
using UnityEditor;

public class EditorNotifications : EditorWindow
{

    public static bool ShowNotification(EditorWindowTypes windowType, string message)
    {
        EditorWindow targetWindow = GetWindow(windowType);
        if (targetWindow == null)
            return false;

        targetWindow.ShowNotification(new GUIContent(message));
        targetWindow.Repaint();
        return true;
    }

    public static void RepaintWindow(EditorWindowTypes windowType)
    {
        EditorWindow targetWindow = GetWindow(windowType);
        if (targetWindow == null)
            return;
        targetWindow.Repaint();
    }

    public static EditorWindow GetWindow(EditorWindowTypes windowType)
    {
        EditorWindow[] allWindows = Resources.FindObjectsOfTypeAll<EditorWindow>();

        foreach (EditorWindow window in allWindows)
        {
            if (window.GetType().Name == windowType.ToString())
            {
                return window;
            }
        }
        return null;
    }


    public enum EditorWindowTypes
    {
        ProjectBrowser,
        InspectorWindow,
        SceneHierarchyWindow,
        SceneView,
        GameView,
        ConsoleWindow,
    }
}