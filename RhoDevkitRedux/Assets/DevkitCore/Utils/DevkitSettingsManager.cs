﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace RhoDevktRedux
{
    [InitializeOnLoad]
    public static class DevkitSettingsManager
    {
        // load settings from file on startup
        static DevkitSettingsManager()
        {
            LoadSettings();
        }
        


        public static string globalSettingsPath = Application.dataPath + "/DevkitCore/Settings/globalSettings.json";

        public static Dictionary<string, string> globalSettings = new Dictionary<string, string>();

        public static void SetSetting(string key, string value, bool writeToFile = true)
        {
            if (key == null || key == "") { return; }
            
            if (value != null)
            {
                if (!globalSettings.ContainsKey(key))
                {
                    globalSettings.Add(key, value);
                }
                else
                {
                    globalSettings[key] = value;
                }
            }
            else
            {
                // value is null, remove key
                globalSettings[key] = null;
            }

            if (writeToFile)
            {
                SaveSettings();
            }

        }

        public static string GetSetting(string key)
        {
            if (globalSettings.ContainsKey(key))
            {
                return globalSettings[key];
            }
            return null;
        }



        public static void SaveSettings()
        {
            File.WriteAllText(globalSettingsPath, JsonConvert.SerializeObject(globalSettings, Formatting.Indented));
        }

        public static void LoadSettings()
        {
            globalSettings = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(globalSettingsPath));
        }

    }
}