﻿using System;
using System.Collections;
using System.Collections.Generic;
using SLua;
using UnityEngine;

//[CustomLuaClass]
public static class Picture
{
	public static Texture2D Create(GameObject target)
	{
		return Picture.Create(target, 128, 128, new Color(1f, 1f, 1f, 0f), new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.7f, 0.5f, 0.5f), new Color(1f, 1f, 1f, 1f), 1.5f, new Vector3(-0.5f, 0.5f, -0.5f), new Color(0f, 0f, 0f, 0f), 1f, false);
	}

	public static Texture2D Create(GameObject target, int width, int height)
	{
		return Picture.Create(target, width, width, new Color(1f, 1f, 1f, 0f), new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.7f, 0.5f, 0.5f), new Color(1f, 1f, 1f, 1f), 1.5f, new Vector3(-0.5f, 0.5f, -0.5f), new Color(0f, 0f, 0f, 0f), 1f, false);
	}

	public static Texture2D Create(GameObject target, int width, int height, Color backgroundColor)
	{
		return Picture.Create(target, width, width, backgroundColor, new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.7f, 0.5f, 0.5f), new Color(1f, 1f, 1f, 1f), 1.5f, new Vector3(-0.5f, 0.5f, -0.5f), new Color(0f, 0f, 0f, 0f), 1f, false);
	}

	public static Texture2D Create(GameObject target, int width, int height, Color backgroundColor, Vector3 objectDirection)
	{
		return Picture.Create(target, width, width, backgroundColor, objectDirection, new Vector3(0.7f, 0.5f, 0.5f), new Color(1f, 0.9f, 0.75f, 1f), 1.5f, new Vector3(-0.5f, 0.5f, -0.5f), new Color(0f, 0f, 0f, 0f), 1f, false);
	}

	public static Texture2D Create(GameObject target, int width, int height, Color backgroundColor, Vector3 objectDirection, Vector3 lightDirection, Color lightcolor, float lightIntensity, Vector3 lightDirection2, Color lightcolor2, float lightIntensity2, bool debug = false)
	{
		Texture2D result;
		if (target == null)
		{
			result = null;
		}
		else
		{
			Quaternion rotation = target.transform.rotation;
			Vector3 position = target.transform.position;
			target.transform.position = new Vector3(0f, 1000f, 0f);
			target.transform.rotation = Quaternion.identity;
			Bounds gameObjectBounds = Picture.GetGameObjectBounds(target);
			if (debug)
			{
				Picture.DrawBounds(gameObjectBounds, target.transform.position);
			}
			Vector3 a = gameObjectBounds.center + target.transform.position;
			float num = Mathf.Max(new float[]
			{
				gameObjectBounds.size.x,
				gameObjectBounds.size.y,
				gameObjectBounds.size.z
			});
			Vector3 position2 = a + target.transform.TransformDirection(objectDirection).normalized * num;
			Transform[] componentsInChildren = target.GetComponentsInChildren<Transform>();
			Dictionary<Transform, int> dictionary = new Dictionary<Transform, int>();
			foreach (Transform transform in componentsInChildren)
			{
				dictionary.Add(transform, transform.gameObject.layer);
			}
			Light[] array2 = (Light[])UnityEngine.Object.FindObjectsOfType(typeof(Light));
			Dictionary<Light, bool> dictionary2 = new Dictionary<Light, bool>();
			foreach (Light light in array2)
			{
				dictionary2.Add(light, light.enabled);
				light.enabled = false;
			}
			Picture.SetLayer(target, LayerMask.NameToLayer("PictureLayer"), true);
			float reflectionIntensity = RenderSettings.reflectionIntensity;
			RenderSettings.reflectionIntensity = 1f;
			GameObject gameObject = new GameObject("GameObjectScreenShotLight");
			gameObject.transform.position = a + target.transform.TransformDirection(lightDirection).normalized * (num * 3f);
			gameObject.transform.rotation = Quaternion.LookRotation(a - gameObject.transform.position);
			Light light2 = gameObject.AddComponent<Light>();
			light2.type = LightType.Directional;
			light2.color = lightcolor;
			light2.intensity = lightIntensity;
			light2.range = num * 30f;
			if (debug)
			{
				Debug.DrawLine(gameObject.transform.position, gameObject.transform.position + (a - gameObject.transform.position).normalized * light2.range, Color.yellow, 30f);
			}
			GameObject gameObject2 = new GameObject("GameObjectScreenShotLight2");
			gameObject2.transform.position = a + target.transform.TransformDirection(lightDirection2).normalized * (num * 3f);
			Light light3 = gameObject2.AddComponent<Light>();
			light3.color = lightcolor2;
			light3.intensity = lightIntensity2;
			light3.range = num * 30f;
			if (debug)
			{
				Debug.DrawLine(gameObject2.transform.position, gameObject2.transform.position + (a - gameObject2.transform.position).normalized * light3.range, Color.cyan, 30f);
			}
			RenderTexture renderTexture = new RenderTexture(width, height, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
			GameObject gameObject3 = new GameObject("GameObjectScreenShotCamera");
			Camera camera = gameObject3.AddComponent<Camera>();
			gameObject3.transform.position = position2;
			gameObject3.transform.rotation = Quaternion.LookRotation(a - gameObject3.transform.position);
			camera.renderingPath = RenderingPath.Forward;
			camera.orthographic = true;
			camera.orthographicSize = num / 1.3f;
			camera.nearClipPlane = 0.01f;
			camera.farClipPlane = num * 2f;
			camera.cullingMask = 1 << LayerMask.NameToLayer("PictureLayer");
			camera.clearFlags = CameraClearFlags.Nothing;
			if (backgroundColor.a != 0f)
			{
				camera.clearFlags = CameraClearFlags.Color;
				camera.backgroundColor = backgroundColor;
			}
			camera.forceIntoRenderTexture = true;
			camera.targetTexture = renderTexture;
			Vector4 vector = Picture.GetGameObjectScreenSpaceAABB(target, camera);
			vector *= camera.orthographicSize;
			if (debug)
			{
				Debug.Log(vector);
			}
			Vector3 vector2 = new Vector3(Mathf.Lerp(vector.x, vector.z, 0.5f), Mathf.Lerp(vector.y, vector.w, 0.5f), 0f);
			vector2 = gameObject3.transform.TransformDirection(vector2);
			if (debug)
			{
				Debug.DrawLine(gameObject3.transform.position, gameObject3.transform.position + vector2, Color.yellow, 30f);
			}
			gameObject3.transform.position += vector2;
			if (debug)
			{
				Debug.DrawLine(gameObject3.transform.position, gameObject3.transform.position + (a - gameObject3.transform.position).normalized * 0.2f, Color.white, 30f);
			}
			RenderTexture.active = camera.targetTexture;
			camera.Render();
			//camera.Render();
			Texture2D texture2D = new Texture2D(width, height, TextureFormat.RGBA32, false);
			texture2D.ReadPixels(new Rect(0f, 0f, (float)width, (float)height), 0, 0);
			texture2D.Apply();
			RenderTexture.active = null;
			camera.targetTexture = null;
			foreach (KeyValuePair<Transform, int> keyValuePair in dictionary)
			{
				keyValuePair.Key.gameObject.layer = keyValuePair.Value;
			}
			foreach (KeyValuePair<Light, bool> keyValuePair2 in dictionary2)
			{
				keyValuePair2.Key.enabled = keyValuePair2.Value;
			}
			target.transform.rotation = rotation;
			target.transform.position = position;
			RenderSettings.reflectionIntensity = reflectionIntensity;
			UnityEngine.Object.DestroyImmediate(gameObject3, false);
			UnityEngine.Object.DestroyImmediate(gameObject, false);
			UnityEngine.Object.DestroyImmediate(gameObject2, false);
			UnityEngine.Object.DestroyImmediate(renderTexture, false);
			result = texture2D;
		}
		return result;
	}

	private static void DrawBounds(Bounds b, Vector3 offsetPos)
	{
		Vector3 size = b.size;
		Vector3 a = b.center + offsetPos;
		Vector3 vector = a + Vector3.Scale(size, new Vector3(1f, 1f, 1f)) * 0.5f;
		Vector3 vector2 = a + Vector3.Scale(size, new Vector3(-1f, 1f, 1f)) * 0.5f;
		Vector3 vector3 = a + Vector3.Scale(size, new Vector3(-1f, 1f, -1f)) * 0.5f;
		Vector3 vector4 = a + Vector3.Scale(size, new Vector3(1f, 1f, -1f)) * 0.5f;
		Vector3 vector5 = a + Vector3.Scale(size, new Vector3(1f, -1f, 1f)) * 0.5f;
		Vector3 vector6 = a + Vector3.Scale(size, new Vector3(-1f, -1f, 1f)) * 0.5f;
		Vector3 vector7 = a + Vector3.Scale(size, new Vector3(-1f, -1f, -1f)) * 0.5f;
		Vector3 vector8 = a + Vector3.Scale(size, new Vector3(1f, -1f, -1f)) * 0.5f;
		Debug.DrawLine(vector, vector2, Color.green, 30f);
		Debug.DrawLine(vector2, vector3, Color.green, 30f);
		Debug.DrawLine(vector3, vector4, Color.green, 30f);
		Debug.DrawLine(vector4, vector, Color.green, 30f);
		Debug.DrawLine(vector5, vector6, Color.green, 30f);
		Debug.DrawLine(vector6, vector7, Color.green, 30f);
		Debug.DrawLine(vector7, vector8, Color.green, 30f);
		Debug.DrawLine(vector8, vector5, Color.green, 30f);
		Debug.DrawLine(vector5, vector, Color.green, 30f);
		Debug.DrawLine(vector6, vector2, Color.green, 30f);
		Debug.DrawLine(vector7, vector3, Color.green, 30f);
		Debug.DrawLine(vector8, vector4, Color.green, 30f);
	}

	private static Bounds GetGameObjectBounds(GameObject obj)
	{
		Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
		Bounds result;
		if (obj == null)
		{
			result = bounds;
		}
		else
		{
			MeshFilter[] componentsInChildren = obj.GetComponentsInChildren<MeshFilter>();
			SkinnedMeshRenderer[] componentsInChildren2 = obj.GetComponentsInChildren<SkinnedMeshRenderer>();
			Vector3 b = new Vector3(1E+08f, 1E+08f, 1E+08f);
			Vector3 a = new Vector3(-1E+08f, -1E+08f, -1E+08f);
			foreach (MeshFilter meshFilter in componentsInChildren)
			{
				if (!(meshFilter.GetComponent<EffectTag>() != null))
				{
					if (!(meshFilter.GetComponent<IndicatorTag>() != null))
					{
						if (!(meshFilter.sharedMesh == null))
						{
							Vector3[] vertices = meshFilter.sharedMesh.vertices;
							foreach (Vector3 position in vertices)
							{
								Vector3 vector = obj.transform.InverseTransformPoint(meshFilter.transform.TransformPoint(position));
								b.x = Mathf.Min(b.x, vector.x);
								b.y = Mathf.Min(b.y, vector.y);
								b.z = Mathf.Min(b.z, vector.z);
								a.x = Mathf.Max(a.x, vector.x);
								a.y = Mathf.Max(a.y, vector.y);
								a.z = Mathf.Max(a.z, vector.z);
							}
						}
					}
				}
			}
			foreach (SkinnedMeshRenderer skinnedMeshRenderer in componentsInChildren2)
			{
				if (!(skinnedMeshRenderer.GetComponent<EffectTag>() != null))
				{
					if (!(skinnedMeshRenderer.GetComponent<IndicatorTag>() != null))
					{
						if (!(skinnedMeshRenderer.sharedMesh == null))
						{
							Mesh skinnedMesh = Picture.GetSkinnedMesh(skinnedMeshRenderer);
							Vector3[] vertices2 = skinnedMesh.vertices;
							foreach (Vector3 position2 in vertices2)
							{
								Vector3 vector2 = obj.transform.InverseTransformPoint(skinnedMeshRenderer.transform.TransformPoint(position2));
								b.x = Mathf.Min(b.x, vector2.x);
								b.y = Mathf.Min(b.y, vector2.y);
								b.z = Mathf.Min(b.z, vector2.z);
								a.x = Mathf.Max(a.x, vector2.x);
								a.y = Mathf.Max(a.y, vector2.y);
								a.z = Mathf.Max(a.z, vector2.z);
							}
							UnityEngine.Object.DestroyImmediate(skinnedMesh, false);
						}
					}
				}
			}
			bounds = new Bounds((a + b) * 0.5f, a - b);
			result = bounds;
		}
		return result;
	}

	private static Vector4 GetGameObjectScreenSpaceAABB(GameObject obj, Camera cam)
	{
		Vector4 vector = new Vector4(0f, 0f, 0f, 0f);
		Vector4 result;
		if (obj == null)
		{
			result = vector;
		}
		else
		{
			MeshFilter[] componentsInChildren = obj.GetComponentsInChildren<MeshFilter>();
			SkinnedMeshRenderer[] componentsInChildren2 = obj.GetComponentsInChildren<SkinnedMeshRenderer>();
			Vector3 vector2 = new Vector3(1E+08f, 1E+08f, 1E+08f);
			Vector3 vector3 = new Vector3(-1E+08f, -1E+08f, -1E+08f);
			foreach (MeshFilter meshFilter in componentsInChildren)
			{
				if (!(meshFilter.GetComponent<EffectTag>() != null))
				{
					if (!(meshFilter.GetComponent<IndicatorTag>() != null))
					{
						if (!(meshFilter.sharedMesh == null))
						{
							Vector3[] vertices = meshFilter.sharedMesh.vertices;
							foreach (Vector3 position in vertices)
							{
								Vector3 position2 = meshFilter.transform.TransformPoint(position);
								Vector3 vector4 = cam.WorldToViewportPoint(position2) - Vector3.one * 0.5f;
								if (vector4.z > 0f)
								{
									vector2.x = Mathf.Min(vector2.x, vector4.x);
									vector2.y = Mathf.Min(vector2.y, vector4.y);
									vector3.x = Mathf.Max(vector3.x, vector4.x);
									vector3.y = Mathf.Max(vector3.y, vector4.y);
								}
							}
						}
					}
				}
			}
			foreach (SkinnedMeshRenderer skinnedMeshRenderer in componentsInChildren2)
			{
				if (!(skinnedMeshRenderer.GetComponent<EffectTag>() != null))
				{
					if (!(skinnedMeshRenderer.GetComponent<IndicatorTag>() != null))
					{
						if (!(skinnedMeshRenderer.sharedMesh == null))
						{
							Mesh skinnedMesh = Picture.GetSkinnedMesh(skinnedMeshRenderer);
							Vector3[] vertices2 = skinnedMesh.vertices;
							foreach (Vector3 position3 in vertices2)
							{
								Vector3 position4 = skinnedMeshRenderer.transform.TransformPoint(position3);
								Vector3 vector5 = cam.WorldToViewportPoint(position4) - Vector3.one * 0.5f;
								if (vector5.z > 0f)
								{
									vector2.x = Mathf.Min(vector2.x, vector5.x);
									vector2.y = Mathf.Min(vector2.y, vector5.y);
									vector3.x = Mathf.Max(vector3.x, vector5.x);
									vector3.y = Mathf.Max(vector3.y, vector5.y);
								}
							}
							UnityEngine.Object.DestroyImmediate(skinnedMesh, false);
						}
					}
				}
			}
			vector = new Vector4(vector2.x, vector2.y, vector3.x, vector3.y);
			result = vector;
		}
		return result;
	}

	public static Mesh GetSkinnedMesh(SkinnedMeshRenderer r)
	{
		Mesh result;
		if (r == null || r.sharedMesh == null)
		{
			result = null;
		}
		else
		{
			Mesh mesh = new Mesh();
			r.BakeMesh(mesh);
			Vector3 lossyScale = r.transform.lossyScale;
			lossyScale.x = 1f / lossyScale.x;
			lossyScale.y = 1f / lossyScale.y;
			lossyScale.z = 1f / lossyScale.z;
			Vector3[] vertices = mesh.vertices;
			for (int i = 0; i < mesh.vertexCount; i++)
			{
				vertices[i] = Vector3.Scale(vertices[i], lossyScale);
			}
			mesh.vertices = vertices;
			result = mesh;
		}
		return result;
	}

	private static void SetLayer(GameObject obj, int l, bool recursive = false)
	{
		if (!(obj == null))
		{
			obj.layer = l;
			if (recursive)
			{
				IEnumerator enumerator = obj.transform.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj2 = enumerator.Current;
						Transform transform = (Transform)obj2;
						if (!(transform.GetComponent<EffectTag>() != null))
						{
							if (!(transform.GetComponent<IndicatorTag>() != null))
							{
								Picture.SetLayer(transform.gameObject, l, true);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
		}
	}
}
