﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace HBS
{
    public static class SchemaGenerator
    {
        private static List<Type> exportTypes; // a list of all default types we need to create schemas for
        private static string defaultSchemaDir = Application.dataPath + "/GeneratedCode/DefaultSchemas";
        public static void GenerateDefaultSchemas()
        {
            GetAssemblies();

            exportTypes = new List<Type>();
            foreach (Assembly loadedAssembly in loadedAssemblies)
            {
                GetClasses(loadedAssembly);
            }
            // exportTypes now contains all classes we should generate by default

            CreateBinder();
            foreach (Type generateType in exportTypes)
            {
                GenerateSchemaFile(generateType, defaultSchemaDir, true, true);
            }
            EndBinder();

        }

        #region manualBlacklisting

        // assemblies to get types from to serialise
        private static List<string> serialisableAssemblies = new List<string>
        {
            "UnityEngine",
            "UnityEngine.UI",
            //"Assembly-CSharp", // don't serialise own assembly. use reflected schemas instead
            "Assembly-CSharp-firstpass" // but do serialise firstpass. it contains things like vector3d etc we need schemas for... maybe?
        };

        private static List<string> manuallyBlacklistedNamespaces = new List<string>
        {
            "AOT",
            "JetBrains.Annotations",
            "UnityEngine.EventSystems",
            "UnityEngine.UI.CoroutineTween",
            "UnityEngine.Diagnostics",
            "UnityEngine.iOS",
            "UnityEngine.Apple.ReplayKit",
            "UnityEngine.Apple.TV",
            "UnityEngine.Tizen",
            "UnityEngine.Windows.Speech",
            "UnityEngine.WSA",
            "UnityEngine.Profiling",
            "UnityEngine.Networking",
            "UnityEngine.Networking.Types",
            "UnityEngine.Networking.Match",
            "UnityEngine.Networking.PlayerConnection",
            "UnityEngine.Analytics",
            "UnityEngine.CrashReportHandler",
            "UnityEngine.Reflection",
            "UnityEngine.VR",
            "UnityEngine.VR.WSA",
            "UnityEngine.VR.WSA.Persistence",
            "UnityEngine.VR.WSA.Sharing",
            "UnityEngine.VR.WSA.WebCam",
            "UnityEngine.VR.WSA.Input",
            "UnityEngine.Internal.VR",
            "UnityEngine.SocialPlatforms",
            "UnityEngine.SocialPlatforms.GameCenter",
            "UnityEngine.SocialPlatforms.Impl",
            "UnityEngine.Events",
            "UnityEngine.Assertions",
            "UnityEngine.Assertions.Comparers",
            "UnityEngine.Assertions.Must",
            "UnityEngine.Collections",
            "UnityEngine.Scripting",
            "UnityEngine.Scripting.APIUpdating",
            "UnityEngine.Serialization",
        };

        private static List<string> manuallyBlacklistedNames = new List<string>
        {
            "System.Collections.Generic.List",
            "SimpleJson",
            "UnityEngine.TextureCompressionQuality",
            "UnityEngine.GUI",
            "UnityEngine.ExitGUIException",
            "UnityEngine.FullScreenMovieControlMode",
            "UnityEngine.FullScreenMovieScalingMode",
            "UnityEngine.AndroidActivityIndicatorStyle",
            "UnityEngine.AndroidJavaException",
            "UnityEngine.TizenActivityIndicatorStyle",
            "UnityEngine.TouchScreenKeyboard",
            "UnityEngine.Gyroscope",
            "UnityEngine.PlayerPrefsException",
            "UnityEngine.Rendering.SphericalHarmonicsL2",
            "UnityEngine.ProceduralMaterial",
            "UnityEngine.Texture2DArray",
            "UnityEngine.CubemapArray",
            "UnityEngine.WWW",
            "UnityEngine.WWWForm",
            "UnityEngine.WWWAudioExtensions",
            "UnityEngine.Experimental.Director.ScriptPlayable",
            "UnityEngine.Experimental.Director.ScriptPlayableOutput",
            "UnityEngine.Experimental.Rendering.IRenderPipelineAsset",
            "UnityEngine.Experimental.Rendering.RenderPipelineAsset",
            "UnityEngine.iPhone",
            "UnityEngine.SamsungTV",
            "UnityEngine.Audio.AudioMixer",
            "UnityEngine.WebCam",
            "UnityEditorInternal.Animator",
            "UnityEngine.StateMachineBehaviour",
            "UnityEngine.Avatar",
            "UnityEngine.AnimatorControllerParameter",
            "UnityEngine.ExposedReference",
            "UnityEngine.RenderTargetSetup",
            "UnityEngine.Rendering.ShadowSamplingMode",
            "UnityEngine.ADBanner",
            "UnityEngine.ILogger",
            "UnityEngine.Logger",
            "UnityEngine.MeshSubsetCombineUtility",
        };

        #endregion

        private static List<Assembly> loadedAssemblies;

        private static void GetAssemblies()
        {
            loadedAssemblies = new List<Assembly>();
            foreach (Assembly loadedAssembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!serialisableAssemblies.Contains(loadedAssembly.GetName().Name)) { continue; } // skip if not on allowed assemblies
                loadedAssemblies.Add(loadedAssembly);
            }
        }


        private static void GetClasses(Assembly a)
        {
            Type[] types = a.GetExportedTypes();
            foreach (Type exportedType in types)
            {
                // blacklisted?
                if (!IsValidType(exportedType)) { continue; }

                // appropriate fields/properties?
                if (GetFields(exportedType, false).Length == 0 && GetProperties(exportedType, false).Length == 0) { continue; }

                exportTypes.Add(exportedType); // valid type, serialise it
            }
        }

        private static bool IsValidType(Type checkType)
        {
            if (checkType.IsDefined(typeof(ObsoleteAttribute), true)) { return false; } // don't gen schema for obselete
            if (checkType.BaseType == typeof(Attribute) || checkType.BaseType == typeof(PropertyAttribute)) { return false; } // don't gen schema for attributes

            // manual blacklist check
            if (manuallyBlacklistedNamespaces.Contains(checkType.Namespace)) { return false; } // manually blacklisted all from this namespace
            foreach (string manualBlacklistType in manuallyBlacklistedNames)
            {
                if (checkType.FullName.StartsWith(manualBlacklistType))
                {
                    return false; // manually blacklisted this exact type
                } 
            }

            return true;
        }



        // TODO cleanup
        private static FieldInfo[] GetFields(Type t, bool further = true)
        {
            var fields = t.GetFields();
            var ret = new List<FieldInfo>();
            foreach (var f in fields)
            {
                if (f.IsPrivate) { continue; }
                if (f.IsInitOnly) { continue; }
                if (f.IsLiteral) { continue; }
                if (f.IsStatic) { continue; }
                if (f.IsNotSerialized) { continue; }
                if (f.IsDefined(typeof(ObsoleteAttribute), true)) { continue; }
                if (InvalidVarName(f.Name)) { continue; }
                if (!IsValidType(f.FieldType)) { continue; }
                if (further && IsPrimitive(f.FieldType) == false && f.FieldType.IsArray == false)
                {
                    if (GetFields(f.FieldType, false).Length == 0 && GetProperties(f.FieldType, false).Length == 0) { continue; }
                }
                ret.Add(f);
            }
            return ret.ToArray();
        }

        // TODO cleanup
        private static PropertyInfo[] GetProperties(Type t, bool further = true)
        {
            var properties = t.GetProperties();
            var ret = new List<PropertyInfo>();
            foreach (var f in properties)
            {
                if (f.GetGetMethod() == null || f.GetSetMethod() == null) { continue; }
                if (f.GetGetMethod().IsStatic || f.GetSetMethod().IsStatic) { continue; }
                if (f.IsDefined(typeof(ObsoleteAttribute), true)) { continue; }
                if (InvalidVarName(f.Name)) { continue; }
                if (!IsValidType(f.PropertyType)) { continue; }
                if (further && IsPrimitive(f.PropertyType) == false && f.PropertyType.IsArray == false)
                {
                    if (GetFields(f.PropertyType, false).Length == 0 && GetProperties(f.PropertyType, false).Length == 0) { continue; }
                }
                ret.Add(f);
            }
            return ret.ToArray();
        }

        private static bool InvalidVarName(string name)
        {
            switch (name)
            {
                case "Item":
                    return true;
                case "mesh":
                    return true;
                case "material":
                    return true;
                case "materials":
                    return true;
                case "parent":
                    return true;
                case "runInEditMode":
                    return true;
                case "areaSize":
                    return true;
                case "bakeLightProbesForTrees":
                    return true;
                case "lightmapBakeType":
                    return true;
                case "scaledBackgrounds":
                    return true;
                case "targetDisplay":
                    return true;
                default:
                    return false;
            }
        }

        // TODO cleanup
        private static bool IsPrimitive(Type f)
        {
            if (f == typeof(IntPtr) || f == typeof(UIntPtr)) { return false; }
            return (f.IsPrimitive || f.Assembly.GetName().FullName.Contains("mscorlib"));
        }

        // TODO cleanup
        private static string FixName(string n)
        {
            var ret = n.Trim(Path.GetInvalidFileNameChars()).Replace("+", "_").Replace("]", "").Replace("[", "").Replace("$", "").Replace(".", "_");
            if (ret.Contains("`")) { ret = ret.Substring(0, ret.Length - 2); }
            return ret;
        }

        // TODO cleanup
        private static string FixFullName(string n)
        {
            var ret = n.Replace("+", ".");
            if (ret.Contains("`")) { ret = ret.Substring(0, ret.Length - 2); }
            return ret;
        }


        public static void GenerateSchemaFile(Type targetType, string outputDir, bool ignoreAttributes = false, bool bindSchema = false)
        {
            // create output directory if it doesn't exist
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }
            CopyBugPasteGenerateSchema(targetType, outputDir, ignoreAttributes, bindSchema);
        }



        private static StreamWriter binderScript;
        public static void CreateBinder()
        {
            // create output directory if it doesn't exist
            if (!Directory.Exists(defaultSchemaDir))
            {
                Directory.CreateDirectory(defaultSchemaDir);
            }
            binderScript = File.CreateText(defaultSchemaDir + "/SerializerBinder.cs");

            binderScript.WriteLine("using UnityEngine;");
            binderScript.WriteLine("using System;");
            binderScript.WriteLine("using System.Collections;");
            binderScript.WriteLine("using System.Collections.Generic;");
            binderScript.WriteLine("namespace HBS {");
            binderScript.WriteLine("    public static partial class SerializerBinder {");
            binderScript.WriteLine("        static SerializerBinder() {");
        }


        public static void EndBinder()
        {
            binderScript.WriteLine("        }");
            binderScript.WriteLine("    }");
            binderScript.WriteLine("}");
            binderScript.Close();
        }


        #region absolutely_fucking_filthy
        // unclean CopyBugPaste code below :(

        private static bool IsSpecialCase(Type f)
        {
            if (f == typeof(Mesh))
            {
                return true;
            }
            if (f == typeof(RevAudioClip))
            {
                return true;
            }
            return false;
            //return (HBS.Serializer.specialCaseSerializers.ContainsKey(f) && HBS.Serializer.specialCaseUnserializers.ContainsKey(f));
        }
        private static bool IsForResources(Type f, out string pathOffset)
        {
            pathOffset = "";
            if (f == typeof(Cubemap)) { return true; }
            if (f == typeof(Texture)) { return true; }
            if (f == typeof(Texture2D)) { return true; }
            if (f == typeof(Texture3D)) { return true; }
            if (f == typeof(RenderTexture)) { return true; }
            if (f == typeof(Material)) { return true; }
            if (f == typeof(SparseTexture)) { return true; }
            if (f == typeof(Font)) { return true; }
            if (f == typeof(AudioClip)) { pathOffset = "Audio"; return true; }
            if (f == typeof(Sprite)) { return true; }
            if (f == typeof(PhysicMaterial)) { return true; }
            if (f == typeof(Shader)) { return true; }
            if (f == typeof(AnimationClip)) { return true; }
            return false;
        }
        private static bool IsComponentOrGameObject(Type f)
        {
            if (f == typeof(GameObject)) { return true; }
            var b = f.BaseType;
            while (b != null)
            {
                if (b == typeof(Component)) { return true; }
                b = b.BaseType;
            }

            return false;
        }


        private static void CopyBugPasteGenerateSchema(Type targetType, string outputDir, bool ignoreAttributes, bool bindSchema)
        {
            var isForPart = false; //do we wana serialize a HB part?
            var isOnlyComponent = false; //do we jsut wana serialize the component without any values .. this to make the serialize put the component on it but then leave it alone...

            //if Assembly-CSharp then check for attribute
            if (!ignoreAttributes)
            {
                var attributes = targetType.GetCustomAttributes(typeof(HBS.SerializeAttribute), false);
                var partAttributes = targetType.GetCustomAttributes(typeof(HBS.SerializePartAttribute), false);
                var compOnlyAttributes = targetType.GetCustomAttributes(typeof(HBS.SerializeComponentOnlyAttribute), false);
                if (attributes.Length == 0 && partAttributes.Length == 0 && compOnlyAttributes.Length == 0)
                {
                    return;
                }
                if (partAttributes.Length > 0)
                {
                    isForPart = true;
                }
                if (compOnlyAttributes.Length > 0)
                {
                    isOnlyComponent = true;
                }
            }


            var properties = GetProperties(targetType);
            var fields = GetFields(targetType);

            var tFullName = FixFullName(targetType.FullName);
            var sName = FixName(targetType.FullName).ToLower();
            var resourcesOffsetPath = "";
            //if any invalids, continue;
            if (sName.ToCharArray().Any(x => Path.GetInvalidFileNameChars().Contains(x)))
            {
                return;
            }
            using (var script = File.CreateText(outputDir + "/Ser_" + sName + ".cs"))
            {
                script.WriteLine("using UnityEngine;");
                script.WriteLine("using UnityEngine.UI;");
                script.WriteLine("using System;");

                script.WriteLine("namespace HBS {");

                script.WriteLine("    public static class Ser_" + sName + " {");

                if (isOnlyComponent)
                {
                    script.WriteLine("        public static void Ser( HBS.Writer writer, object oo ) {}");
                    script.WriteLine("        public static object Res( HBS.Reader reader, object oo = null ) { return oo; }");
                }
                else if (targetType.IsEnum)
                {
                    //Debug.Log(targetType + " is enum");
                    script.WriteLine("        public static void Ser( HBS.Writer writer, object oo ) {");
                    script.WriteLine("            if( writer.WriteNull(oo) ) { return; }");
                    script.WriteLine("            " + tFullName + " o = (" + tFullName + ")oo;");
                    script.WriteLine("            writer.Write(o.ToString());");
                    script.WriteLine("        }");
                    script.WriteLine("        public static object Res( HBS.Reader reader, object o = null ) {");
                    script.WriteLine("            if(reader.ReadNull()){ return null; }");
                    script.WriteLine("            return (object)(" + tFullName + ")System.Enum.Parse(typeof(" + tFullName + "),(string)reader.Read());");
                    script.WriteLine("        }");
                }
                else if (IsForResources(targetType, out resourcesOffsetPath))
                {
                    script.WriteLine("        public static void Ser( HBS.Writer writer, object oo ) {");
                    script.WriteLine("            if( writer.WriteNull(oo)) { return; }");
                    script.WriteLine("            " + tFullName + " o = (" + tFullName + ")oo;");
                    script.WriteLine("            writer.Write(o.name);");
                    script.WriteLine("        }");
                    script.WriteLine("        public static object Res( HBS.Reader reader, object o = null) {");
                    script.WriteLine("            if(reader.ReadNull()){ return null; }");
                    if (resourcesOffsetPath == "")
                    {
                        script.WriteLine("            return (object)Resources.Load<" + tFullName + ">((string)reader.Read());");
                    }
                    else
                    {
                        script.WriteLine("            return (object)Resources.Load<" + tFullName + ">(\"" + resourcesOffsetPath + "/\"+(string)reader.Read());");
                    }
                    script.WriteLine("        }");
                }
                else
                {
                    script.WriteLine("        public static void Ser( HBS.Writer writer , object oo ) {");
                    script.WriteLine("            if( writer.WriteNull(oo)) { return; }");
                    script.WriteLine("            HBS.Writer writer_ASXDRGBHU;");
                    script.WriteLine("            " + tFullName + " o = (" + tFullName + ")oo;");

                    //write count ( calc difrent for part )
                    if (isForPart)
                    {
                        var totalCount = 1;
                        foreach (var f in fields)
                        {
                            if (f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts
                            totalCount++;
                        }
                        foreach (var f in properties)
                        {
                            if (f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts
                            totalCount++;
                        }
                        script.WriteLine("            writer.Write(" + totalCount.ToString() + ");");
                    }
                    else
                    {
                        script.WriteLine("            writer.Write(" + (fields.Length + properties.Length).ToString() + ");");
                    }

                    if (isForPart && targetType.BaseType == typeof(Part))
                    {
                        script.WriteLine("");
                        //script.WriteLine("            writer.Write(\"PartPropertiesBytes\");");
                        script.WriteLine("            writer.Write(\"PartProperties\");");
                        script.WriteLine("            writer_ASXDRGBHU = new HBS.Writer();");
                        //script.WriteLine("            o.PropertiesToBytes(writer_ASXDRGBHU); //writer_ASXDRGBHU.Write(o.PropertiesToString());");
                        script.WriteLine("            writer_ASXDRGBHU.Write(o.PropertiesToString());");
                        script.WriteLine("            writer.Write(writer_ASXDRGBHU.stream.ToArray());");
                        script.WriteLine("            writer_ASXDRGBHU.Close();");
                    }

                    #region save fields/properties

                    foreach (var f in fields)
                    {
                        if (isForPart && f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts
                        script.WriteLine("");
                        script.WriteLine("            writer.Write(\"" + f.Name + "\");");
                        script.WriteLine("            writer_ASXDRGBHU = new HBS.Writer();");
                        if (f.FieldType.IsArray)
                        {
                            script.WriteLine("            if( writer_ASXDRGBHU.WriteNull(o." + f.Name + ") == false ) {");
                            script.WriteLine("                writer_ASXDRGBHU.Write(o." + f.Name + ".Length);");
                            script.WriteLine("                for( int i = 0; i < o." + f.Name + ".Length; i++ ) {");
                            var et = f.FieldType.GetElementType();
                            if (IsSpecialCase(et))
                            {
                                script.WriteLine("                    HBS.Serializer.specialCaseSerializers[typeof(" + FixFullName(et.FullName) + ")].Invoke(writer_ASXDRGBHU,o." + f.Name + "[i]); //field special case");
                            }
                            else if (IsPrimitive(et))
                            {
                                script.WriteLine("                    writer_ASXDRGBHU.Write(o." + f.Name + "[i]); //field primitive");
                            }
                            else if (IsComponentOrGameObject(et))
                            {
                                script.WriteLine("                    HBS.Serializer.SerializePath(writer_ASXDRGBHU,o." + f.Name + "[i]); //field component or gameObject");
                            }
                            else
                            {
                                var fsname = FixName(et.FullName).ToLower();
                                script.WriteLine("                    HBS.Ser_" + fsname + ".Ser( writer_ASXDRGBHU , o." + f.Name + "[i]); //field");
                            }
                            script.WriteLine("                }");
                            script.WriteLine("            }");
                        }
                        else
                        {
                            if (IsSpecialCase(f.FieldType))
                            {
                                script.WriteLine("            HBS.Serializer.specialCaseSerializers[typeof(" + FixFullName(f.FieldType.FullName) + ")].Invoke(writer_ASXDRGBHU,o." + f.Name + "); //field special case");
                            }
                            else if (IsPrimitive(f.FieldType))
                            {
                                script.WriteLine("            writer_ASXDRGBHU.Write(o." + f.Name + "); //field primitive");
                            }
                            else if (IsComponentOrGameObject(f.FieldType))
                            {
                                script.WriteLine("            HBS.Serializer.SerializePath(writer_ASXDRGBHU,o." + f.Name + "); //field component or gameObject");
                            }
                            else
                            {
                                var fsname = FixName(f.FieldType.FullName).ToLower();
                                script.WriteLine("            HBS.Ser_" + fsname + ".Ser( writer_ASXDRGBHU , o." + f.Name + "); //field");
                            }
                        }
                        script.WriteLine("            writer.Write(writer_ASXDRGBHU.stream.ToArray());");
                        script.WriteLine("            writer_ASXDRGBHU.Close();");
                    }

                    foreach (var f in properties)
                    {
                        if (isForPart && f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts
                        script.WriteLine("");
                        script.WriteLine("            writer.Write(\"" + f.Name + "\");");
                        script.WriteLine("            writer_ASXDRGBHU = new HBS.Writer();");
                        if (f.PropertyType.IsArray)
                        {
                            script.WriteLine("            if( writer_ASXDRGBHU.WriteNull(o." + f.Name + ") == false ) {");
                            script.WriteLine("                writer_ASXDRGBHU.Write(o." + f.Name + ".Length);");
                            script.WriteLine("                for( int i = 0; i < o." + f.Name + ".Length; i++ ) {");
                            var et = f.PropertyType.GetElementType();
                            if (IsSpecialCase(et))
                            {
                                script.WriteLine("                    HBS.Serializer.specialCaseSerializers[typeof(" + FixFullName(et.FullName) + ")].Invoke(writer_ASXDRGBHU,o." + f.Name + "[i]); //property special case");
                            }
                            else if (IsPrimitive(et))
                            {
                                script.WriteLine("                    writer_ASXDRGBHU.Write(o." + f.Name + "[i]); //property primitive");
                            }
                            else if (IsComponentOrGameObject(et))
                            {
                                script.WriteLine("                    HBS.Serializer.SerializePath(writer_ASXDRGBHU,o." + f.Name + "[i]); //property component or gameObject");
                            }
                            else
                            {
                                var fsname = FixName(et.FullName).ToLower();
                                script.WriteLine("                    HBS.Ser_" + fsname + ".Ser( writer_ASXDRGBHU , o." + f.Name + "[i]); //property");
                            }
                            script.WriteLine("                }");
                            script.WriteLine("            }");
                        }
                        else
                        {
                            if (IsSpecialCase(f.PropertyType))
                            {
                                script.WriteLine("            HBS.Serializer.specialCaseSerializers[typeof(" + FixFullName(f.PropertyType.FullName) + ")].Invoke(writer_ASXDRGBHU,o." + f.Name + "); //property special case");
                            }
                            else if (IsPrimitive(f.PropertyType))
                            {
                                script.WriteLine("            writer_ASXDRGBHU.Write(o." + f.Name + "); //property primitive");
                            }
                            else if (IsComponentOrGameObject(f.PropertyType))
                            {
                                script.WriteLine("            HBS.Serializer.SerializePath(writer_ASXDRGBHU,o." + f.Name + "); //property component or gameObject");
                            }
                            else
                            {
                                var fsname = FixName(f.PropertyType.FullName).ToLower();
                                script.WriteLine("            HBS.Ser_" + fsname + ".Ser( writer_ASXDRGBHU , o." + f.Name + "); //property");
                            }
                        }
                        script.WriteLine("            writer.Write(writer_ASXDRGBHU.stream.ToArray());");
                        script.WriteLine("            writer_ASXDRGBHU.Close();");
                    }

                    #endregion save fields/properties

                    script.WriteLine("        }");

                    script.WriteLine("        public static object Res( HBS.Reader reader, object oo = null) {");
                    script.WriteLine("            if(reader.ReadNull()){ return null; }");

                    script.WriteLine("            HBS.Reader reader_ASXDRGBHU;");

                    if (IsComponentOrGameObject(targetType))
                    {
                        script.WriteLine("            " + tFullName + " o = (" + tFullName + ")oo;");
                    }
                    else
                    {
                        script.WriteLine("            " + tFullName + " o = new " + tFullName + "();");
                    }

                    script.WriteLine("            int count_ASXDRGBHU = (int)reader.Read();");
                    script.WriteLine("            for (int i_ASXDRGBHU = 0; i_ASXDRGBHU < count_ASXDRGBHU; i_ASXDRGBHU++) {");
                    script.WriteLine("                string name_ASXDRGBHU = \"\";");
                    script.WriteLine("                byte[] data_ASXDRGBHU = null;");
                    script.WriteLine("                try {");
                    script.WriteLine("                    name_ASXDRGBHU = (string)reader.Read();");
                    script.WriteLine("                    data_ASXDRGBHU = (byte[])reader.Read();");
                    script.WriteLine("                } catch { continue; }");

                    if (isForPart && targetType.BaseType == typeof(Part))
                    {
                        script.WriteLine("");
                        script.WriteLine("                if (name_ASXDRGBHU == \"PartProperties\") {");
                        script.WriteLine("                    try {");
                        script.WriteLine("                        reader_ASXDRGBHU = new HBS.Reader(data_ASXDRGBHU);");
                        script.WriteLine("                        o.PropertiesFromString((string)reader_ASXDRGBHU.Read());");
                        script.WriteLine("                        reader_ASXDRGBHU.Close();");
                        script.WriteLine("                    } catch { }");
                        script.WriteLine("                }");

                        //faster way to ser properties 19/02/20

                        script.WriteLine("                if (name_ASXDRGBHU == \"PartPropertiesBytes\") {");
                        script.WriteLine("                    try {");
                        script.WriteLine("                        reader_ASXDRGBHU = new HBS.Reader(data_ASXDRGBHU);");
                        script.WriteLine("                        o.BytesToProperties(reader_ASXDRGBHU);");
                        script.WriteLine("                        reader_ASXDRGBHU.Close();");
                        script.WriteLine("                    } catch { }");
                        script.WriteLine("                }");
                    }

                    #region load fields and properties

                    foreach (var f in fields)
                    {
                        if (isForPart && f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts

                        script.WriteLine("");
                        script.WriteLine("                if (name_ASXDRGBHU == \"" + f.Name + "\") {");
                        script.WriteLine("                    try {");
                        script.WriteLine("                        reader_ASXDRGBHU = new HBS.Reader(data_ASXDRGBHU);");

                        if (f.FieldType.IsArray)
                        {
                            script.WriteLine("                    if( reader_ASXDRGBHU.ReadNull() == false ) {");
                            script.WriteLine("                        " + FixFullName(f.FieldType.FullName) + " " + f.Name + "_arr = new " + FixFullName(f.FieldType.GetElementType().FullName) + "[(int)reader_ASXDRGBHU.Read()];");
                            script.WriteLine("                        for( int i = 0; i < " + f.Name + "_arr.Length; i++ ) {");

                            var et = f.FieldType.GetElementType();
                            if (IsSpecialCase(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Serializer.specialCaseUnserializers[typeof(" + FixFullName(et.FullName) + ")].Invoke(reader_ASXDRGBHU,typeof(" + FixFullName(et.FullName) + "),o." + f.Name + "); //field special case ");
                            }
                            else if (IsPrimitive(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")reader_ASXDRGBHU.Read(); //field primitive");
                            }
                            else if (IsComponentOrGameObject(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Serializer.UnserializePath(reader_ASXDRGBHU); //field component or gameobject");
                            }
                            else
                            {
                                var fsname = FixName(et.FullName).ToLower();
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Ser_" + fsname + ".Res( reader_ASXDRGBHU ); //field");
                            }
                            script.WriteLine("                            }");
                            script.WriteLine("                            o." + f.Name + " = " + f.Name + "_arr;");
                            script.WriteLine("                         }");
                        }
                        else
                        {
                            if (IsSpecialCase(f.FieldType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.FieldType.FullName) + ")HBS.Serializer.specialCaseUnserializers[typeof(" + FixFullName(f.FieldType.FullName) + ")].Invoke(reader_ASXDRGBHU,typeof(" + FixFullName(f.FieldType.FullName) + "),o." + f.Name + "); //field special case ");
                            }
                            else if (IsPrimitive(f.FieldType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.FieldType.FullName) + ")reader_ASXDRGBHU.Read(); //field primitive");
                            }
                            else if (IsComponentOrGameObject(f.FieldType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.FieldType.FullName) + ")HBS.Serializer.UnserializePath(reader_ASXDRGBHU); //field component or gameobject");
                            }
                            else
                            {
                                var fsname = FixName(f.FieldType.FullName).ToLower();
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.FieldType.FullName) + ")HBS.Ser_" + fsname + ".Res( reader_ASXDRGBHU ); //field");
                            }
                        }

                        script.WriteLine("                        reader_ASXDRGBHU.Close();");
                        script.WriteLine("                    } catch { }");
                        script.WriteLine("                }");
                    }

                    foreach (var f in properties)
                    {
                        if (isForPart && f.GetCustomAttributes(typeof(SerializePartVarAttribute), true).Length == 0) { continue; } //skip fields that dont have SerializePartVarAttribute if its for parts

                        script.WriteLine("");
                        script.WriteLine("                if (name_ASXDRGBHU == \"" + f.Name + "\") {");
                        script.WriteLine("                    try {");
                        script.WriteLine("                        reader_ASXDRGBHU = new HBS.Reader(data_ASXDRGBHU);");

                        if (f.PropertyType.IsArray)
                        {
                            script.WriteLine("                        if( reader_ASXDRGBHU.ReadNull() == false ) {");
                            script.WriteLine("                            " + FixFullName(f.PropertyType.FullName) + " " + f.Name + "_arr = new " + FixFullName(f.PropertyType.GetElementType().FullName) + "[(int)reader_ASXDRGBHU.Read()];");
                            script.WriteLine("                            for( int i = 0; i < " + f.Name + "_arr.Length; i++ ) {");
                            var et = f.PropertyType.GetElementType();
                            if (IsSpecialCase(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Serializer.specialCaseUnserializers[typeof(" + FixFullName(et.FullName) + ")].Invoke(reader_ASXDRGBHU,typeof(" + FixFullName(et.FullName) + "),o." + f.Name + "); //property special case ");
                            }
                            else if (IsPrimitive(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")reader_ASXDRGBHU.Read(); //property primitive");
                            }
                            else if (IsComponentOrGameObject(et))
                            {
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Serializer.UnserializePath(reader_ASXDRGBHU); //property component or gameobject");
                            }
                            else
                            {
                                var fsname = FixName(et.FullName).ToLower();
                                script.WriteLine("                                " + f.Name + "_arr[i] = (" + FixFullName(et.FullName) + ")HBS.Ser_" + fsname + ".Res( reader_ASXDRGBHU ); //property");
                            }
                            script.WriteLine("                            }");
                            script.WriteLine("                            o." + f.Name + " = " + f.Name + "_arr;");
                            script.WriteLine("                        }");
                        }
                        else
                        {
                            if (IsSpecialCase(f.PropertyType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.PropertyType.FullName) + ")HBS.Serializer.specialCaseUnserializers[typeof(" + FixFullName(f.PropertyType.FullName) + ")].Invoke(reader_ASXDRGBHU,typeof(" + FixFullName(f.PropertyType.FullName) + "),o." + f.Name + "); //property special case");
                            }
                            else if (IsPrimitive(f.PropertyType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.PropertyType.FullName) + ")reader_ASXDRGBHU.Read(); //property primitive");
                            }
                            else if (IsComponentOrGameObject(f.PropertyType))
                            {
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.PropertyType.FullName) + ")HBS.Serializer.UnserializePath(reader_ASXDRGBHU); //property component or gameobject");
                            }
                            else
                            {
                                var fsname = FixName(f.PropertyType.FullName).ToLower();
                                script.WriteLine("                        o." + f.Name + " = (" + FixFullName(f.PropertyType.FullName) + ")HBS.Ser_" + fsname + ".Res( reader_ASXDRGBHU ); //property");
                            }
                        }

                        script.WriteLine("                        reader_ASXDRGBHU.Close();");
                        script.WriteLine("                    } catch { }");
                        script.WriteLine("                }");
                    }

                    #endregion load fields and properties

                    script.WriteLine("            }");
                    script.WriteLine("            return (object)o;");
                    script.WriteLine("        }");
                }

                script.WriteLine("    }");
                script.WriteLine("}");
            }

            if (bindSchema)
            {
                binderScript.WriteLine("            bindsSer.Add(typeof(" + tFullName + "),new Action<Writer,object>(Ser_" + sName + ".Ser));");
                binderScript.WriteLine("            bindsRes.Add(typeof(" + tFullName + "),new Func<Reader,object,object>(Ser_" + sName + ".Res));");
            }
        }

        #endregion
    }
}
