using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
namespace HBS {
    public partial class SerializerBinder {
        public static Dictionary<Type,Action<Writer,object>> bindsSer = new Dictionary<Type,Action<Writer,object>>();
        public static Dictionary<Type,Func<Reader,object,object>> bindsRes = new Dictionary<Type,Func<Reader,object,object>>();

        public static void Serialize( HBS.Writer writer, object o ) 
        {

            if ( writer.WriteNull(o)) {
                return; 
            }

            Type objType = o.GetType();

            writer.Write('>');

            if (!bindsSer.ContainsKey(objType))
            {
                // reflected schema
                ReflectedSchema.ReflectedSerialise(writer, o);
                return;
            }

            bindsSer[o.GetType()].Invoke(writer,o);
        }
        //public static object Unserialize(HBS.Reader reader, Type t, object o = null)
        public static void Unserialize(HBS.Reader reader, Type t, object o = null)
        {
            if ( reader.ReadNull() ) { return; }
            if( reader.ReadNull() ) { return; }
            // check if bindings doesn't contain binding
            Type objType = o.GetType();

            if (!bindsRes.ContainsKey(objType))
            {
                // reflected schema
                ReflectedSchema.ReflectedDeserialise(reader, o);
                return;
            }

            bindsRes[objType].Invoke(reader, o);
        }
    }
}