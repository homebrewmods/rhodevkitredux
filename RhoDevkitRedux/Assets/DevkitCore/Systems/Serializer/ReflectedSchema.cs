﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Reflection;
//using UnityEngine;

//namespace HBS
//{
//	public static class ReflectedSchema
//	{
//		public static object ReflectedDeserialise(HBS.Reader reader, object oo = null)
//		{
//            if (oo.GetType().IsDefined(typeof(HBS.SerializeComponentOnlyAttribute), false))
//            {
//                return null; // component only
//            }

//            if (reader.ReadNull()) { return null; }
//            HBS.Reader reader_ReflectedRes;

//            bool serialisePart = false;
//            if (oo.GetType().IsDefined(typeof(HBS.SerializePartAttribute), false))
//            {
//                serialisePart = true;
//            }

//            MemberInfo[] fields = oo.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
//            MemberInfo[] props = oo.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

//            MemberInfo[] members = new MemberInfo[fields.Length + props.Length];

//            Array.Copy(fields, members, fields.Length);
//            Array.Copy(props, 0, members, fields.Length, props.Length);

//            int count_ReflectedRes = (int)reader.Read();
//            for (int i_ReflectedRes = 0; i_ReflectedRes < count_ReflectedRes; i_ReflectedRes++)
//            {
//                string name_ReflectedRes = "";
//                byte[] data_ReflectedRes = null;
//                try
//                {
//                    name_ReflectedRes = (string)reader.Read();
//                    data_ReflectedRes = (byte[])reader.Read();
//                }
//                catch { continue; }

//                if (name_ReflectedRes == "PartProperties")
//                {
//                    try
//                    {
//                        reader_ReflectedRes = new HBS.Reader(data_ReflectedRes);
//                        ((Part)oo).PropertiesFromString((string)reader_ReflectedRes.Read());
//                        reader_ReflectedRes.Close();
//                    }
//                    catch { }
//                }
//                if (name_ReflectedRes == "PartPropertiesBytes")
//                {
//                    try
//                    {
//                        reader_ReflectedRes = new HBS.Reader(data_ReflectedRes);
//                        ((Part)oo).BytesToProperties(reader_ReflectedRes);
//                        reader_ReflectedRes.Close();
//                    }
//                    catch { }
//                }

//                // reflection time
//                foreach (MemberInfo member in members)
//                {
//                    if (serialisePart && !member.IsDefined(typeof(HBS.SerializePartVarAttribute), false))
//                    {
//                        continue; // got serialisepart attrib, only deserialise fields with partvar attrib
//                    }

//                    if (member.Name == name_ReflectedRes)
//                    {
//                        // field name and deserialised name match
//                        Type varType = VariableType(member);
//                        if (varType.IsArray)
//                        {
//                            reader_ReflectedRes = new HBS.Reader(data_ReflectedRes);
//                            if (reader_ReflectedRes.ReadNull()) { return null; }

//                            Array varArray = Array.CreateInstance(varType.GetElementType(), (int)reader_ReflectedRes.Read());

//                            for (int i = 0; i < varArray.Length; i++)
//                            {
//                                varArray.SetValue(DeserialiseVar(reader_ReflectedRes, varType.GetElementType()), i);
//                            }

//                            SetValue(member, oo, varArray);

//                            varArray = null;

//                            reader_ReflectedRes.Close();
//                        }
//                        else
//                        {
//                            reader_ReflectedRes = new HBS.Reader(data_ReflectedRes);
//                            SetValue(member, oo, DeserialiseVar(reader_ReflectedRes, varType));
//                            reader_ReflectedRes.Close();
//                        }
//                    }
//                }
//            }

//            return oo;
//        }


//        public static void ReflectedSerialise(HBS.Writer writer, object oo = null)
//        {
//            if (oo.GetType().IsDefined(typeof(HBS.SerializeComponentOnlyAttribute), false))
//            {
//                return; // component only
//            }

//            if (writer.WriteNull(oo)) { return; }

//            HBS.Writer writer_ReflectedSer;

//            // if serialisePart, only serialise those fields which define serialisePartVar
//            bool serialisePart = false;
//            if (oo.GetType().IsDefined(typeof(HBS.SerializePartAttribute), false))
//            {
//                serialisePart = true;
//            }

//            MemberInfo[] fields = oo.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
//            MemberInfo[] props = oo.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

//            MemberInfo[] preMembers = new MemberInfo[fields.Length + props.Length];

//            Array.Copy(fields, preMembers, fields.Length);
//            Array.Copy(props, 0, preMembers, fields.Length, props.Length);

//            List<MemberInfo> serialisedMembers = new List<MemberInfo>();

//            // count serialisable members
//            int serialisableCount = 0;

//            foreach (MemberInfo member in preMembers)
//            {
//                if (serialisePart && !member.IsDefined(typeof(HBS.SerializePartVarAttribute), false))
//                {
//                    continue; // got serialisepart attrib, only deserialise fields with partvar attrib
//                }

//                if (blacklistMembers.Contains(member.Name))
//                {
//                    continue;
//                }


//                if (blacklistBaseDeclarers.Contains(member.DeclaringType))
//                {
//                    continue;
//                }

//                if (specialHandling.ContainsKey(member.DeclaringType))
//                {
//                    if (!specialHandling[member.DeclaringType].Contains(member.Name))
//                    {
//                        continue;
//                    }
//                }

//                serialisableCount++;
//                serialisedMembers.Add(member);
//            }

//            bool serialisePartProps = false;
//            if (serialisePart && oo.GetType().BaseType == typeof(Part))
//            {
//                serialisableCount++;
//                serialisePartProps = true;
//            }


//            writer.Write(serialisedMembers.Count);


//            if (serialisePartProps)
//            {
//                writer.Write("PartProperties");
//                writer_ReflectedSer = new HBS.Writer();
//                writer_ReflectedSer.Write(((Part)oo).PropertiesToString());
//                writer.Write(writer_ReflectedSer.stream.ToArray());
//                writer_ReflectedSer.Close();
//            }

//            foreach (MemberInfo member in serialisedMembers)
//            {

//                Type varType = VariableType(member);
//                object val = GetValue(member, oo);


//                writer.Write(member.Name);

//                if (varType.IsArray)
//                {
//                    writer_ReflectedSer = new HBS.Writer();

//                    if (!writer_ReflectedSer.WriteNull(val))
//                    {
//                        writer_ReflectedSer.Write(((object[])val).Length);
//                        for (int i = 0; i < ((object[])val).Length; i++)
//                        {
//                            SerialiseVar(writer_ReflectedSer, varType.GetElementType(), ((object[])val)[i]);
//                        }
//                    }
//                    writer.Write(writer_ReflectedSer.stream.ToArray());
//                    writer_ReflectedSer.Close();

//                }
//                else
//                {

//                    writer_ReflectedSer = new HBS.Writer();

//                    SerialiseVar(writer_ReflectedSer, varType, val);

//                    writer.Write(writer_ReflectedSer.stream.ToArray());
//                    writer_ReflectedSer.Close();
//                }



//            }

//        }


//        #region utils


//        private static List<string> blacklistMembers = new List<string>
//        {
//            "runInEditMode",
//            "isActiveAndEnabled",
//        };

//        private static List<Type> blacklistBaseDeclarers = new List<Type>
//        {
//            //typeof(UnityEngine.MonoBehaviour),
//        };


//        // special whitelists for specific types
//        private static Dictionary<Type, List<string>> specialHandling = new Dictionary<Type, List<string>>
//        {
//            { typeof(UnityEngine.MonoBehaviour), new List<string> 
//                {
//                    "useGUILayout"
//                } 
//            },
//            { typeof(UnityEngine.Behaviour), new List<string>
//                {
//                    "enabled"
//                }
//            },
//            { typeof(UnityEngine.Component), new List<string>
//                {
//                    "tag"
//                }
//            },
//            { typeof(UnityEngine.Object), new List<string>
//                {
//                    "name",
//                    "hideFlags"
//                }
//            }

//        };

//        private static void SerialiseVar(Writer w, Type varType, object val)
//        {
//            // primitive first
//            if (varType.IsPrimitive || varType == typeof(Decimal) || varType == typeof(String))
//            {
//                WritePrimitive(w, varType, val);
//                return;
//            }

//            //special type handling second
//            if (varType.IsEnum)
//            {
//                Debug.Log(val.ToString());
//                if (w.WriteNull(val.ToString())) { return; }
//                WritePrimitive(w, typeof(string), val.ToString());
//                return;
//            }

//            if (varType == typeof(UnityEngine.Transform) || varType == typeof(UnityEngine.GameObject))
//            {
//                HBS.Serializer.SerializePath(w, val);
//                return;
//            }



//            // bound schema types last
//            if (HBS.SerializerBinder.bindsRes.ContainsKey(varType))
//            {
//                HBS.SerializerBinder.bindsSer[varType].Invoke(w, val);
//                return;
//            }

//            //unbound type and not primitive, must reflect
//            ReflectedSchema.ReflectedSerialise(w, val);
//            return;

//        }

//        private static object DeserialiseVar(Reader r, Type varType)
//        {
//            object retValue = null;
//            // primitive first

//            if (varType.IsPrimitive || varType == typeof(Decimal) || varType == typeof(String))
//            {
//                retValue = r.Read();
//                return retValue;
//            }

//            // special type handling second
//            if (varType.IsEnum)
//            {
//                retValue = DeserialiseEnum(r, varType);
//                return retValue;
//            }

//            if (varType == typeof(UnityEngine.Transform) || varType == typeof(UnityEngine.GameObject))
//            {
//                retValue = HBS.Serializer.UnserializePath(r);
//                return retValue;
//            }



//            // bound schema types last
//            if (HBS.SerializerBinder.bindsRes.ContainsKey(varType))
//            {
//                retValue = HBS.SerializerBinder.bindsRes[varType].Invoke(r, null);
//                return retValue;
//            }

//            //unbound type and not primitive, must reflect
//            retValue = ReflectedSchema.ReflectedDeserialise(r, Activator.CreateInstance(varType));
//            return retValue;

//        }


//        private static object DeserialiseEnum(Reader r, Type enumType)
//        {
//            if (r.ReadNull()) { return null; }
//            return Enum.Parse(enumType, r.Read().ToString());
//        }

//        public static object GetValue(MemberInfo memberInfo, object forObject)
//        {
//            switch (memberInfo.MemberType)
//            {
//                case MemberTypes.Field:
//                    return ((FieldInfo)memberInfo).GetValue(forObject);
//                case MemberTypes.Property:
//                    return ((PropertyInfo)memberInfo).GetValue(forObject, null);
//                default:
//                    throw new NotImplementedException();
//            }
//        }

//        public static void SetValue(MemberInfo memberInfo, object forObject, object value)
//        {
//            switch (memberInfo.MemberType)
//            {
//                case MemberTypes.Field:
//                    ((FieldInfo)memberInfo).SetValue(forObject, value);
//                    return;
//                case MemberTypes.Property:
//                    ((PropertyInfo)memberInfo).SetValue(forObject, value, null);
//                    return;
//                default:
//                    throw new NotImplementedException();
//            }
//        }

//        public static Type VariableType(this MemberInfo memberInfo)
//        {
//            switch (memberInfo.MemberType)
//            {
//                case MemberTypes.Field:
//                    return ((FieldInfo)memberInfo).FieldType;
//                case MemberTypes.Property:
//                    return ((PropertyInfo)memberInfo).PropertyType;
//                default:
//                    throw new NotImplementedException();
//            }
//        }

//        #endregion

//        #region specialWriter

//        private static void WritePrimitive(Writer w, Type varType, object val)
//        {
//            // this could be a dictionary instead
//            typeof(Writer).GetMethod("Write", new Type[] { varType }).Invoke(w, new object[] { val });
//        }

//        #endregion
//    }
//}