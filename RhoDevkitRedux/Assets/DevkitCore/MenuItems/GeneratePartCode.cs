﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using UnityNamedPipes;
using System.Reflection;
using System.Linq;

namespace RhoDevktRedux
{
    public static class GeneratePartCode
    {

        static string codeGenExePath = Application.dataPath + "/DevkitCore/Systems/CodeGen/HBPartCodeGen.exe";

        static string hbAssemblyPath = DevkitSettingsManager.GetSetting("homebrewDataDirectory") + "/Managed/Assembly-CSharp.dll";

        static string modLoaderPath = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(Application.dataPath)), "ModAssemblyLoader\\bin\\Debug\\ModAssemblyLoader.dll");


        // disabled for now, needs a lot of work
        //[MenuItem("Devkit/Maintenance/Generate mod part code")]
        public static void GenerateModCode()
        {
            ModAssemblyManager.GenerateModStubs();
        }


        [MenuItem("Devkit/Maintenance/Generate Base game part code")]
        public static void GenerateBaseGameCode()
        {

            // if HB path not found then prompt user
            if (DevkitSettingsManager.GetSetting("homebrewDirectory") == null || DevkitSettingsManager.GetSetting("homebrewDirectory") == "")
            {
                HomebrewIntegration.SetHomebrewDirectory();
            }

            // did user actually set HB path?
            if (DevkitSettingsManager.GetSetting("homebrewDirectory") == null || DevkitSettingsManager.GetSetting("homebrewDirectory") == "")
            {
                EditorUtility.DisplayDialog("Homebrew root directory not set.", "Please Select root Homebrew install folder using 'Devkit/Settings/Set Homebrew directory'", "Close");
                return;
            }

            hbAssemblyPath = DevkitSettingsManager.GetSetting("homebrewDataDirectory") + "/Managed/Assembly-CSharp.dll";
            string devkitPartCodeGenPath = Application.dataPath + "/GeneratedCode/PartCode";

            if (!Directory.Exists(devkitPartCodeGenPath))
            {
                Directory.CreateDirectory(devkitPartCodeGenPath);
            }

            // clear cache

            // clear part stub cache
            foreach (FileInfo file in new DirectoryInfo(devkitPartCodeGenPath).GetFiles())
            {
                file.Delete();
            }

            InvokeGenerator(hbAssemblyPath, devkitPartCodeGenPath);

        }


        public static void InvokeGenerator(string assemblyPath, string outPath)
        {

            // get current main assembly
            Assembly curAssembly = null;
            foreach (Assembly assy in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assy.GetName().Name == "Assembly-CSharp") { curAssembly = assy; break; }
            }
            if (curAssembly == null) { Debug.LogError("could not find Assembly-CSharp"); return; }

            // find all serialisable types
            //List<Type> attribs = new List<Type> { typeof(HBS.SerializeAttribute), typeof(HBS.SerializePartAttribute), typeof(HBS.SerializeComponentOnlyAttribute) };
            Dictionary<string, Type> serTypes = new Dictionary<string, Type>();
            foreach (Type foundType in curAssembly.GetTypes())
            {
                //foreach (Type attrib in attribs)
                //{
                //    if (foundType.IsDefined(attrib, true))
                //    {
                //        if (!foundType.IsNested)
                //        {
                //Debug.Log(foundType.Name);
                if (!serTypes.ContainsKey(foundType.Name))
                {
                    serTypes.Add(foundType.Name, foundType);
                }
                //        }
                //    }
                //}

            }

            // set up temp file for classnames not to generate
            string tempFilePath = Path.GetTempFileName();
            FileInfo tempFile = new FileInfo(tempFilePath);
            tempFile.Attributes = FileAttributes.Temporary;
            StreamWriter streamWriter = File.AppendText(tempFilePath);

            foreach (KeyValuePair<string, Type> entry in serTypes)
            {
                streamWriter.WriteLine(entry.Key);
            }

            streamWriter.Flush();
            streamWriter.Close();




            // fire code generator for base HB assembly
            StartGenProc(assemblyPath, outPath, tempFilePath);

            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
        }

        public static void StartGenProc(string assemblyPath, string outPath, string knownClasses)
        {

            if (!File.Exists(codeGenExePath))
            {
                Debug.LogError("CodeGen Error: HBPartCodeGen.exe not found.");
                return;
            }

            TaskCompletionSource<bool> eventHandled = new TaskCompletionSource<bool>();

            try
            {
                System.Diagnostics.Process codeGenProcess = new System.Diagnostics.Process();
                codeGenProcess.StartInfo.FileName = codeGenExePath;
                codeGenProcess.StartInfo.Arguments = "-generatestubs \"" + assemblyPath + "\" \"" + outPath + "\" \"" + hbAssemblyPath + "\" \"" + modLoaderPath + "\" \"" + knownClasses + "\"";
                codeGenProcess.EnableRaisingEvents = true;
                //codeGenProcess.Exited += new EventHandler(GenProcEnd);
                codeGenProcess.Start();

                codeGenProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
                return;
            }

            AssetDatabase.Refresh();
        }

        //private static void GenProcEnd(object sender, System.EventArgs e)
        //{
        //    AssetDatabase.Refresh();
        //}
    }
}