﻿using UnityEngine;

public class HBLink : MonoBehaviour {

    //baseclass for all Links like FloatWire , DriveShaft , PressureDuckt

    public virtual void ReadFromPropertiesPassImmediate() {

    }
    public virtual void ReadFromPropertiesPass2() {

    }
    public virtual void ReadFromPropertiesPass3() {

    }

    public virtual void ReadFromPropertiesPassImmediateNew() {

    }
    public virtual void ReadFromPropertiesPass2New() {

    }
    public virtual void ReadFromPropertiesPass3New() {

    }
}
