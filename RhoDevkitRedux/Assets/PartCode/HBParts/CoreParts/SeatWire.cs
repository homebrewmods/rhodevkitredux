﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[HBS.SerializeComponentOnlyAttribute]
public class SeatWire : HBLink {

    public List<SeatWireInput> inputs = new List<SeatWireInput>();
    public List<SeatWireOutput> outputs = new List<SeatWireOutput>();

    public SeatWireOutput NewOutput(Property outputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new SeatWireOutput
        SeatWireOutput newOutput = new SeatWireOutput( new Property.PropertyRefrence(GetComponent<Part>(),outputProp.pname));
        ////NEW
        foreach( SeatWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public SeatWireInput NewInput(Property inputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new SeatWireOutput
        SeatWireInput newInput = new SeatWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));
        ////NEW
        foreach (SeatWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public SeatWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new SeatWireOutput
        SeatWireOutput newOutput = new SeatWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (SeatWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public SeatWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new SeatWireOutput
        SeatWireInput newInput = new SeatWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (SeatWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (SeatWireOutput output in outputs) {
            output.inputs.Clear();
        }
    }
    public void ClearInputsFromOutput(SeatWireOutput output) {
        output.inputs.Clear();
    }
    public void RemoveInputFromOutput(SeatWireOutput output, SeatWireInput input) {
        output.inputs.RemoveAll(SeatWireInput => SeatWireInput == input);
    }
    public void AssignInputToOutput(SeatWireOutput output, Property.PropertyRefrence inputProp) {
       
        SeatWire targetSeatWire = inputProp.part.GetComponent<SeatWire>();
        if (targetSeatWire == null) {
            Debug.LogError(gameObject.name + "target property doesnt have a seatwire : coudnt have made this link" + inputProp.part.gameObject.name + "  " + inputProp.propertyName);
            return;
        }

        SeatWireInput targetSeatWireInput = targetSeatWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetSeatWireInput == null) {
            Debug.LogError( gameObject.name + "targetSeatWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput: " + inputProp.propertyName);
            return;
        }

        //assign targetSeatWireInput to this SeatWireOutput
        output.inputs.Add(targetSeatWireInput);
        
    }

    public SeatWireOutput FindOutputFromPropertyName(string pname) {
        foreach (SeatWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public SeatWireInput FindInputFromPropertyName(string pname) {
        foreach (SeatWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }
   
    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for seatwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); //dont reset and only  ////NEW

        //cleanup inputs list
        List<SeatWireInput> blackList = new List<SeatWireInput>();
        foreach( SeatWireInput i in inputs ) {
            bool found = false;
            //find the input property refrence in the part 
            foreach(Property p in GetComponent<Part>().properties ) {
                if( p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if( found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach( SeatWireInput bl in blackList ) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<SeatWireOutput> blackList2 = new List<SeatWireOutput>();
        foreach (SeatWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (SeatWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }
       
    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other SeatWire properties on pass2

        //link each output to its inputs
        foreach (SeatWireOutput output in outputs) {
            if (output.propertyRefrence == null) { continue; }
            var prop = output.propertyRefrence.GetProperty();
            if(prop  == null) { continue; }
            foreach (Property.PropertyRefrence otiProp in prop.outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Seat) {
                    AssignInputToOutput(output, otiProp);
                }
                else {
                    Debug.LogError("Somehow linked a SeatWire output to a non SeatWire output property OR property Part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public class SeatWireInput {
        public Property.PropertyRefrence propertyRefrence;
        //to read values from
        public object value = null;
        public int seatedConnID;
        public SeatWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void Set(object v) {
            value = v;
        }
        public void SetSeatedConnID( int id) {
            seatedConnID = id;
        }
    }

    public class SeatWireOutput {
        public Property.PropertyRefrence propertyRefrence;
        public object curVal = null;
        public int curSeatedConnID = -1;
        //to apply values to
        public List<SeatWireInput> inputs = new List<SeatWireInput>();
        public SeatWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void Pass(object value) {
            curVal = value;
            foreach (SeatWireInput input in inputs) {
                input.Set(value);
            }
        }
        public void PassSeatedConnID(int id) {
            curSeatedConnID = id;
            foreach (SeatWireInput input in inputs) {
                input.SetSeatedConnID(id);
            }
        }
    }
}
