﻿using System.Collections.Generic;
using UnityEngine;

[HBS.SerializeComponentOnlyAttribute]
public class FloatWire : HBLink {

    //float wire simply passes floats from FloatWireOutputs to FloatWireInputs
    //FloatWireItself acts like a container to support multyple Outputs going to multyple Inputs , or Multyple Inputs can have multyple OUputs linked to them
    //so how to setup a Floatwire..
    //first reset the FloatWire
    //first let the part use the NewOutput and NewInput functions when it reads from properties and basicly setup our inputs outputs list for all FloatWires on all parts
    //then in pass 2 we can simply look at the property our outputs have and link them to the appropriate FLoatWireInput
    //on pass 3 all FloatWires on all Parts should have their outputs linked, we can then generate a mesh to visually display this link 
    public List<FloatWireInput> inputs = new List<FloatWireInput>();
    public List<FloatWireOutput> outputs = new List<FloatWireOutput>();

    public FloatWireOutput NewOutput(Property outputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireOutput newOutput = new FloatWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));
        ////NEW
        foreach (FloatWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FloatWireInput NewInput(Property inputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireInput newInput = new FloatWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));
        ////NEW
        foreach (FloatWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public FloatWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireOutput newOutput = new FloatWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (FloatWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FloatWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireInput newInput = new FloatWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (FloatWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public FloatWireOutput NewOutputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireOutput newOutput = new FloatWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        ////NEW
        foreach (FloatWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == pname) {
                //already found it return this one instead
                return o;
            }
        }
        //NEW


        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FloatWireInput NewInputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new FloatWireOutput
        FloatWireInput newInput = new FloatWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));
        ////NEW
        foreach (FloatWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == pname) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (FloatWireOutput output in outputs) {
            output.inputs.Clear();
        }
    }
    public void ClearInputsFromOutput(FloatWireOutput output) {
        output.inputs.Clear();
    }
    public void RemoveInputFromOutput(FloatWireOutput output, FloatWireInput input) {
        output.inputs.RemoveAll(FloatWireInput => FloatWireInput == input);
    }
    public void AssignInputToOutput(FloatWireOutput output, Property.PropertyRefrence inputProp) {

        FloatWire targetFloatWire = inputProp.part.GetComponent<FloatWire>();
        if (targetFloatWire == null) {
            Debug.LogError(gameObject.name + "target property doesnt have a floatwire : coudnt have made this link" + inputProp.part.gameObject.name + "  " + inputProp.propertyName);
            return;
        }

        //Debug.Log("FloatWire want to assign this :" +inputProp.GetProperty().descriptiveName+" input to this "+output.propertyRefrence.GetProperty().descriptiveName+" output");
        FloatWireInput targetFloatWireInput = targetFloatWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetFloatWireInput == null) {
            Debug.LogError(gameObject.name + "targetFloatWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput: " + inputProp.propertyName);
            return;
        }

        //assign targetFloatWireInput to this FloatWireOutput
        output.inputs.Add(targetFloatWireInput);

        //Debug.Log("FloatWire: linked " + output.propertyRefrence.GetProperty().pname+ " to " + inputProp.propertyName);
    }

    public FloatWireOutput FindOutputFromPropertyName(string pname) {
        foreach (FloatWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public FloatWireInput FindInputFromPropertyName(string pname) {
        foreach (FloatWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for floatwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); //dont reset and only  ////NEW

        //cleanup inputs list
        List<FloatWireInput> blackList = new List<FloatWireInput>();
        foreach (FloatWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (FloatWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<FloatWireOutput> blackList2 = new List<FloatWireOutput>();
        foreach (FloatWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (FloatWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }

    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other FloatWire properties on pass2

        //link each output to its inputs
        foreach (FloatWireOutput output in outputs) {
            if (output.propertyRefrence == null) { continue; }
            var prop = output.propertyRefrence.GetProperty();
            if (prop == null) { continue; }
            foreach (Property.PropertyRefrence otiProp in prop.outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Float) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a FLoatWire output to a non FloatWire output property OR property Part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public class FloatWireInput : IComparer<FloatWireInput> {
        public Property.PropertyRefrence propertyRefrence;
        //to read values from
        public float value = 0f;
        public FloatWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void Set(float v) {
            value = v;
        }
        public int Compare(FloatWireInput x, FloatWireInput y) {
            if (x == null) { return -1; }
            if (y == null) { return 1; }
            return x.value.CompareTo(y.value);
        }
    }

    public class FloatWireOutput {
        public Property.PropertyRefrence propertyRefrence;
        public float curVal = 0f;
        //to apply values to
        public List<FloatWireInput> inputs = new List<FloatWireInput>();
        public FloatWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void Pass(float value = 0f) {
            if (float.IsNaN(value)) { return; }
            if (float.IsInfinity(value)) { return; }
            curVal = value;
            foreach (FloatWireInput input in inputs) {
                input.Set(value);
            }
        }
    }
}
