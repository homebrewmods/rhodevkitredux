﻿using System.Collections.Generic;
using UnityEngine;

[HBS.SerializeComponentOnlyAttribute]
public class VectorWire : HBLink {

    //float wire simply passes floats from VectorWireOutputs to VectorWireInputs
    //VectorWireItself acts like a container to support multyple Outputs going to multyple Inputs , or Multyple Inputs can have multyple OUputs linked to them
    //so how to setup a VectorWire..
    //first reset the VectorWire
    //first let the part use the NewOutput and NewInput functions when it reads from properties and basicly setup our inputs outputs list for all VectorWires on all parts
    //then in pass 2 we can simply look at the property our outputs have and link them to the appropriate VectorWireInput
    //on pass 3 all VectorWires on all Parts should have their outputs linked, we can then generate a mesh to visually display this link 
    public List<VectorWireInput> inputs = new List<VectorWireInput>();
    public List<VectorWireOutput> outputs = new List<VectorWireOutput>();

    public VectorWireOutput NewOutput(Property outputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireOutput newOutput = new VectorWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));
        ////NEW
        foreach (VectorWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public VectorWireInput NewInput(Property inputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireInput newInput = new VectorWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));
        ////NEW
        foreach (VectorWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public VectorWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireOutput newOutput = new VectorWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (VectorWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return o;
            }
        }
        ////NEW
        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public VectorWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireInput newInput = new VectorWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        ////NEW
        foreach (VectorWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public VectorWireOutput NewOutputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireOutput newOutput = new VectorWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        ////NEW
        foreach (VectorWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == pname) {
                //already found it return this one instead
                return o;
            }
        }
        //NEW


        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public VectorWireInput NewInputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new VectorWireOutput
        VectorWireInput newInput = new VectorWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));
        ////NEW
        foreach (VectorWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == pname) {
                //already found it return this one instead
                return i;
            }
        }
        ////NEW
        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (VectorWireOutput output in outputs) {
            output.inputs.Clear();
        }
    }
    public void ClearInputsFromOutput(VectorWireOutput output) {
        output.inputs.Clear();
    }
    public void RemoveInputFromOutput(VectorWireOutput output, VectorWireInput input) {
        output.inputs.RemoveAll(VectorWireInput => VectorWireInput == input);
    }
    public void AssignInputToOutput(VectorWireOutput output, Property.PropertyRefrence inputProp) {

        VectorWire targetVectorWire = inputProp.part.GetComponent<VectorWire>();
        if (targetVectorWire == null) {
            Debug.LogError(gameObject.name + "target property doesnt have a VectorWire : coudnt have made this link" + inputProp.part.gameObject.name + "  " + inputProp.propertyName);
            return;
        }

        //Debug.Log("VectorWire want to assign this :" +inputProp.GetProperty().descriptiveName+" input to this "+output.propertyRefrence.GetProperty().descriptiveName+" output");
        VectorWireInput targetVectorWireInput = targetVectorWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetVectorWireInput == null) {
            Debug.LogError(gameObject.name + "targetVectorWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput: " + inputProp.propertyName);
            return;
        }

        //assign targetVectorWireInput to this VectorWireOutput
        output.inputs.Add(targetVectorWireInput);

        //Debug.Log("VectorWire: linked " + output.propertyRefrence.GetProperty().pname+ " to " + inputProp.propertyName);
    }

    public VectorWireOutput FindOutputFromPropertyName(string pname) {
        foreach (VectorWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public VectorWireInput FindInputFromPropertyName(string pname) {
        foreach (VectorWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for VectorWire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); //dont reset and only  ////NEW

        //cleanup inputs list
        List<VectorWireInput> blackList = new List<VectorWireInput>();
        foreach (VectorWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (VectorWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<VectorWireOutput> blackList2 = new List<VectorWireOutput>();
        foreach (VectorWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (VectorWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }

    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other VectorWire properties on pass2

        //link each output to its inputs
        foreach (VectorWireOutput output in outputs) {

            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Vector) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a VectorWire output to a non VectorWire output property OR property part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public class VectorWireInput : IComparer<VectorWireInput> {
        public Property.PropertyRefrence propertyRefrence;
        //to read values from
        public Vector3 value = Vector3.zero;
        public VectorWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void Set(Vector3 v) {
            value = v;
        }
        public int Compare(VectorWireInput x, VectorWireInput y) {
            if (x == null) { return -1; }
            if (y == null) { return 1; }
            return x.value.x.CompareTo(y.value.x) + x.value.y.CompareTo(y.value.y) + x.value.z.CompareTo(y.value.z);
        }
    }

    public class VectorWireOutput {
        public Property.PropertyRefrence propertyRefrence;
        public Vector3 curVal = Vector3.zero;
        //to apply values to
        public List<VectorWireInput> inputs = new List<VectorWireInput>();
        public VectorWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void Pass(Vector3 value) {
            if (float.IsNaN(value.x) || float.IsNaN(value.y) || float.IsNaN(value.z)) { return; }
            if (float.IsInfinity(value.x) || float.IsInfinity(value.y) || float.IsInfinity(value.z)) { return; }
            curVal = value;

            foreach (VectorWireInput input in inputs) {
                input.Set(value);
            }
        }
    }
}
