﻿using System.Collections.Generic;
using UnityEngine;

[HBS.SerializeComponentOnlyAttribute]
public class ElectricWire : HBLink {

    //electric wire simply passes floats from ElectricWireOutputs to ElectricWireInputs

    //how to use ElectricWireInput , UseWatt on Update , the supplyneed is in Watts/h , so basicly howmush W/h does this part need to run as it should , use totalPowerFactor to know howmush the powersystem is returning
    //how to use ElectricWireOutput , PassWatt on Update , grab howmush needs to be passed from totalWatt , this may be clamped

    public List<ElectricWireInput> inputs = new List<ElectricWireInput>();
    public List<ElectricWireOutput> outputs = new List<ElectricWireOutput>();

    public ElectricWireOutput NewOutput(Property outputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInput(Property inputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public ElectricWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public ElectricWireOutput NewOutputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == pname) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == pname) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (ElectricWireOutput output in outputs) {
            output.ClearLinks();
        }
    }
    public void ClearInputsFromOutput(ElectricWireOutput output) {
        output.ClearLinks();
    }
    public void RemoveInputFromOutput(ElectricWireOutput output, ElectricWireInput input) {
        output.UnassignInput(input);
    }
    public void AssignInputToOutput(ElectricWireOutput output, Property.PropertyRefrence inputProp) {

        ElectricWire targetElectricWire = inputProp.part.GetComponent<ElectricWire>();
        if (targetElectricWire == null) {
            Debug.LogError("target property doesnt have a electricwire : coudnt have made this link");
            return;
        }

        ElectricWireInput targetElectricWireInput = targetElectricWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetElectricWireInput == null) {
            Debug.LogError("targetElectricWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput");
            return;
        }

        //assign targetElectricWireInput to this ElectricWireOutput
        output.AssignInput(targetElectricWireInput);
        targetElectricWireInput.AssignOutput(output);

        //Debug.Log("linked " + output.propertyRefrence.propertyName+ " to " + inputProp.propertyName);
    }

    public ElectricWireOutput FindOutputFromPropertyName(string pname) {
        foreach (ElectricWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public ElectricWireInput FindInputFromPropertyName(string pname) {
        foreach (ElectricWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    void LateUpdate() {
        foreach (ElectricWireInput input in inputs) {
            input.LateUpdate();
        }
        foreach (ElectricWireOutput output in outputs) {
            output.LateUpdate();
        }
    }

    //void Update() {
    //    foreach (ElectricWireInput input in inputs) {
    //        input.Update();
    //    }
    //}

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for electricwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); //NEW

        //cleanup inputs list
        List<ElectricWireInput> blackList = new List<ElectricWireInput>();
        foreach (ElectricWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }

                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (ElectricWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<ElectricWireOutput> blackList2 = new List<ElectricWireOutput>();
        foreach (ElectricWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }

                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (ElectricWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }
    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other ElectricWire properties on pass2

        //link each output to its inputs
        foreach (ElectricWireOutput output in outputs) {
            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.ElectricWire) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a ElectricWire output to a non ElectricWire input property OR property Part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public class ElectricWireInput {

        public float totalPowerfactor;
        public float totalWatt;
        private float currentPowerFactor;
        private float currentWatt;

        public List<ElectricWireOutput> outputs = new List<ElectricWireOutput>();
        public Property.PropertyRefrence propertyRefrence;

        public ElectricWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void AssignOutput(ElectricWireOutput output) {
            if (outputs.Contains(output)) {
                //Debug.LogError("ASSIGNING OUTPUT TO INPUT ELEC ? BUT IT ALREADY EXISTS");
            } else {
                outputs.Add(output);
            }
        }
        public void UnassignOutput(ElectricWireOutput output) {
            outputs.RemoveAll(ElectricWireOutput => ElectricWireOutput == output);
        }

        public void ClearLinks() {
            outputs.Clear();
        }

        public void UseWatt(float Wt) {
            foreach (ElectricWireOutput o in outputs) {
                o.UseWatt(this, Wt / (float)outputs.Count);
            }
        }
        public void PassWatt(float pf, float Wt) {
            currentPowerFactor += pf / (float)outputs.Count;
            currentWatt += Wt;
        }

        public void LateUpdate() {
            totalPowerfactor = currentPowerFactor;
            currentPowerFactor = 0f;
            totalWatt = currentWatt;
            currentWatt = 0;
        }
    }

    public class ElectricWireOutput {
        public Property.PropertyRefrence propertyRefrence;

        public float totalWt;
        private float currentWt;
        private float theoreticalMaxWatt;
        public float totalTheoreticalMaxWatt;

        public List<ElectricWireInput> inputs = new List<ElectricWireInput>();
        public Dictionary<ElectricWireInput, float> inputWatts = new Dictionary<ElectricWireInput, float>();
        public Dictionary<ElectricWireInput, float> totalInputWatts = new Dictionary<ElectricWireInput, float>();
        public ElectricWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void AssignInput(ElectricWireInput input) {
            if (inputs.Contains(input)) {
                //Debug.LogError("ASSIGNING INPUT TO OUTPUT WIRE ELEC ? BUT IT ALREADY EXISTS");
            } else {
                inputs.Add(input);
            }
        }
        public void UnassignInput(ElectricWireInput input) {

            inputs.RemoveAll(ElectricWireInput => ElectricWireInput == input);
        }
        public void ClearLinks() {
            inputs.Clear();
        }

        public void UseWatt(ElectricWireInput i, float Wt) {
            if (inputWatts.ContainsKey(i) == false) {
                inputWatts.Add(i, Wt);
            }
            currentWt += Wt;
        }

        public void PassWatt(float Wt) {
            if (totalWt == 0) {
                return;
            }
            float powerFactor = Wt / totalWt;
            foreach (ElectricWireInput i in totalInputWatts.Keys) {
                i.PassWatt(powerFactor, totalInputWatts[i] * powerFactor);
            }
        }

        public void SetTheoreticalMaxWatt(float Wt) {
            theoreticalMaxWatt = Wt;
        }

        public void LateUpdate() {
            totalWt = currentWt;
            currentWt = 0;
            totalTheoreticalMaxWatt = theoreticalMaxWatt;
            theoreticalMaxWatt = 0;
            totalInputWatts.Clear();
            foreach (ElectricWireInput i in inputWatts.Keys) {
                totalInputWatts.Add(i, inputWatts[i]);
            }
            inputWatts.Clear();
        }

    }
}

/*
public class ElectricWire : HBLink {

    //electric wire simply passes floats from ElectricWireOutputs to ElectricWireInputs
    //ElectricWireItself acts like a container to support multyple Outputs going to multyple Inputs , or Multyple Inputs can have multyple OUputs linked to them
    //so how to setup a Electricwire..
    //first reset the ElectricWire
    //first let the part use the NewOutput and NewInput functions when it reads from properties and basicly setup our inputs outputs list for all ElectricWires on all parts
    //then in pass 2 we can simply look at the property our outputs have and link them to the appropriate FLoatWireInput
    //on pass 3 all ElectricWires on all Parts should have their outputs linked, we can then generate a mesh to visually display this link

    //how to use ElectricWireInput , PassSupplyNeeds on Update , the supplyneed is in Watts/h , so basicly howmush W/h does this part need to run as it should
    //how to use ElectricWireOutput , PassSupply on Update , the supply is then devided to the Inputs depeding on their supplyNeed

    public List<ElectricWireInput> inputs = new List<ElectricWireInput>();
    public List<ElectricWireOutput> outputs = new List<ElectricWireOutput>();

    public ElectricWireOutput NewOutput(Property outputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInput(Property inputProp) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public ElectricWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public ElectricWireOutput NewOutputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireOutput newOutput = new ElectricWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //NEW
        foreach (ElectricWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == pname) {
                //found return this one instead
                return o;
            }
        }
        //NEW

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public ElectricWireInput NewInputLua(string pname) {
        //reads straight from properties
        //only after this is read we may make the links

        //create new ElectricWireOutput
        ElectricWireInput newInput = new ElectricWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //NEW
        foreach (ElectricWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == pname) {
                //found return this one instead
                return i;
            }
        }
        //NEW

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (ElectricWireOutput output in outputs) {
            output.ClearLinks();
        }
    }
    public void ClearInputsFromOutput(ElectricWireOutput output) {
        output.ClearLinks();
    }
    public void RemoveInputFromOutput(ElectricWireOutput output, ElectricWireInput input) {
        output.UnassignInput(input);
    }
    public void AssignInputToOutput(ElectricWireOutput output, Property.PropertyRefrence inputProp) {

        ElectricWire targetElectricWire = inputProp.part.GetComponent<ElectricWire>();
        if (targetElectricWire == null) {
            Debug.LogError("target property doesnt have a electricwire : coudnt have made this link");
            return;
        }

        ElectricWireInput targetElectricWireInput = targetElectricWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetElectricWireInput == null) {
            Debug.LogError("targetElectricWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput");
            return;
        }

        //assign targetElectricWireInput to this ElectricWireOutput
        output.AssignInput(targetElectricWireInput);
        targetElectricWireInput.AssignOutput(output);

        //Debug.Log("linked " + output.propertyRefrence.propertyName+ " to " + inputProp.propertyName);
    }

    public ElectricWireOutput FindOutputFromPropertyName(string pname) {
        foreach (ElectricWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public ElectricWireInput FindInputFromPropertyName(string pname) {
        foreach (ElectricWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    void LateUpdate() {
        foreach (ElectricWireInput input in inputs) {
            input.LateUpdate();
        }
        foreach (ElectricWireOutput output in outputs) {
            output.LateUpdate();
        }
    }

    void Update() {
        foreach (ElectricWireInput input in inputs) {
            input.Update();
        }
    }

    public void CreateMeshes() {
        if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for electricwire meshes

        }
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); //NEW

        //cleanup inputs list
        List<ElectricWireInput> blackList = new List<ElectricWireInput>();
        foreach (ElectricWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }

                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (ElectricWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<ElectricWireOutput> blackList2 = new List<ElectricWireOutput>();
        foreach (ElectricWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }

                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (ElectricWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }
    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other ElectricWire properties on pass2

        //link each output to its inputs
        foreach (ElectricWireOutput output in outputs) {
            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty().linkType == PropertyLinkType.ElectricWire) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a ElectricWire output to a non ElectricWire input property");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public class ElectricWireInput {

        public float currentA = 0f;
        public float currentV = 0f;

        private float preV = 0f;
        private float preA = 0f;
        public List<ElectricWireOutput> outputs = new List<ElectricWireOutput>();
        public Property.PropertyRefrence propertyRefrence;

        public ElectricWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void AssignOutput(ElectricWireOutput output) {
            outputs.Add(output);
        }
        public void UnassignOutput(ElectricWireOutput output) {
            outputs.RemoveAll(ElectricWireOutput => ElectricWireOutput == output);
        }

        public void ClearLinks() {
            outputs.Clear();
        }
        public void PassA(float A) {
            preA = A;
        }

        void RealPassA() {

            int userCount = 0;
            foreach (ElectricWireOutput output in outputs) {
                if (output.curConnected) {
                    userCount++;
                }
            }
            if (currentA > 0) {
                foreach (ElectricWireOutput output in outputs) {
                    if (output.curConnected) {
                        output.SetA(currentA / userCount);
                    }
                }
            }

        }

        public void SetV(float V) {
            if (V > preV) {
                preV = V;
            }
        }

        public void Update() {
            RealPassA();
        }

        public void LateUpdate() {
            currentV = preV;
            preV = 0f;
            currentA = preA;
            preA = 0f;
        }

    }
    public class ElectricWireOutput {
        public Property.PropertyRefrence propertyRefrence;

        public float currentA = 0f;
        public float currentV = 0f;

        public bool curConnected = false;
        private bool preConnected = false;
        private float preA = 0f;

        public List<ElectricWireInput> inputs = new List<ElectricWireInput>();

        public ElectricWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }
        public void AssignInput(ElectricWireInput input) {
            inputs.Add(input);
        }
        public void UnassignInput(ElectricWireInput input) {

            inputs.RemoveAll(ElectricWireInput => ElectricWireInput == input);
        }
        public void ClearLinks() {
            inputs.Clear();
        }

        public void PassV(float V) {
            currentV = V;
            preConnected = true;
            foreach (ElectricWireInput input in inputs) {
                //float toA = input.currentA;
                //float thisA = currentA;

                //float ratioA = 0f;
                //if (thisA != 0f && toA != 0f) {
                //    ratioA = thisA / toA;
                //} 
                input.SetV(V);
            }
        }

        public void SetA(float A) {
            preA += A;
        }

        public void LateUpdate() {
            curConnected = preConnected;
            preConnected = false;
            currentA = preA;
            preA = 0f;
        }

    }
}
*/
