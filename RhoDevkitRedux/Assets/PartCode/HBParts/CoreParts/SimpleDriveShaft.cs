﻿using HBS;
using System;
using System.Collections.Generic;
using UnityEngine;

[SerializeComponentOnly]
public class SimpleDriveShaft : HBLink {
    public List<SimpleDriveShaftGear> gears = new List<SimpleDriveShaftGear>();

    public List<SimpleDriveShaftInput> inputs = new List<SimpleDriveShaftInput>();

    public List<SimpleDriveShaftOutput> outputs = new List<SimpleDriveShaftOutput>();

    public void AssignOutputHubToInputPropertyAndBackwards(SimpleDriveShaftOutput output, Property.PropertyRefrence inputProp) {
        var component = inputProp.part.GetComponent<SimpleDriveShaft>();
        if (component == null) {
            Debug.LogError("target property doesnt have a DriveShaft : coudnt have made this link :" + inputProp.part.gameObject.name);
            return;
        }
        var simpleDriveShaftInput = component.FindInputHubFromPropertyName(inputProp.propertyName);
        if (simpleDriveShaftInput == null) {
            Debug.LogError("targetDriveShaftHub couldnt find a propertyInput with this particular property assigned on AssignOutputHubToInputPropertyAndBackwards");
            return;
        }
        output.AssignInput(simpleDriveShaftInput);
        simpleDriveShaftInput.AssignOutput(output);
    }

    public void ClearGears() {
        foreach (var simpleDriveShaftGear in gears) {
            if (simpleDriveShaftGear.from != null) {
                simpleDriveShaftGear.from.gears.Clear();
            }
            if (simpleDriveShaftGear.to != null) {
                simpleDriveShaftGear.to.gears.Clear();
            }
        }
        gears.Clear();
    }

    public SimpleDriveShaftInput FindInputHubFromPropertyName(string pname) {
        foreach (var simpleDriveShaftInput in inputs) {
            if (simpleDriveShaftInput.propertyRefrence.propertyName == pname) {
                return simpleDriveShaftInput;
            }
        }
        return null;
    }

    public SimpleDriveShaftOutput FindOutputHubFromPropertyName(string pname) {
        foreach (var output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }

    public SimpleDriveShaftGear NewGear(SimpleDriveShaftInput fromHub, SimpleDriveShaftOutput toHub, float ratio = 1f, bool inverseRotation = false) {
        var gear = new SimpleDriveShaftGear(fromHub, toHub, ratio, inverseRotation);
        gears.Add(gear);
        fromHub.gears.Add(gear);
        toHub.gears.Add(gear);
        return gear;
    }

    public SimpleDriveShaftInput NewInput(Property inputProp) {
        foreach (var input in inputs) {
            if (input.propertyRefrence.propertyName == inputProp.pname) {
                return input;
            }
        }
        var input2 = new SimpleDriveShaftInput(new Property.PropertyRefrence(base.GetComponent<Part>(), inputProp.pname));
        inputs.Add(input2);
        return input2;
    }

    public SimpleDriveShaftOutput NewOutput(Property outputProp) {
        foreach (var output in outputs) {
            if (output.propertyRefrence.propertyName == outputProp.pname) {
                return output;
            }
        }
        var output2 = new SimpleDriveShaftOutput(new Property.PropertyRefrence(base.GetComponent<Part>(), outputProp.pname));
        outputs.Add(output2);
        return output2;
    }

    public override void ReadFromPropertiesPass2() {
        foreach (var output in outputs) {
            foreach (var propReference in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (propReference.GetProperty() != null && propReference.GetProperty().linkType == PropertyLinkType.SimpleDriveshaft) {
                    AssignOutputHubToInputPropertyAndBackwards(output, propReference);
                } else {
                    Debug.LogError("Somehow linked a FLoatWire output to a non FloatWire output property OR property part is null");
                }
            }
        }
    }

    public override void ReadFromPropertiesPass3() {
    }

    public override void ReadFromPropertiesPassImmediate() {
        ClearGears();
        var list = new List<SimpleDriveShaftInput>();
        foreach (var input in inputs) {
            var flag = false;
            foreach (var property in base.GetComponent<Part>().properties) {
                if (property.type == PropertyType.PInput
                    && property.pname == input.propertyRefrence.propertyName
                    && property.inputToOutputProperties.Count > 0) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                list.Add(input);
            }
        }
        foreach (var input2 in list) {
            foreach (var output in input2.hubs) {
                output.hubs.Remove(input2);
            }
            inputs.Remove(input2);
        }
        var list2 = new List<SimpleDriveShaftOutput>();
        foreach (var output in outputs) {
            var flag2 = false;
            foreach (var prop in base.GetComponent<Part>().properties) {
                if (prop.type == PropertyType.POutput && prop.pname == output.propertyRefrence.propertyName && prop.outputToInputProperties.Count > 0) {
                    flag2 = true;
                    break;
                }
            }
            if (!flag2) {
                list2.Add(output);
            }
        }
        foreach (var simpleDriveShaftOutput3 in list2) {
            foreach (var simpleDriveShaftInput3 in simpleDriveShaftOutput3.hubs) {
                simpleDriveShaftInput3.hubs.Remove(simpleDriveShaftOutput3);
            }
            outputs.Remove(simpleDriveShaftOutput3);
        }
        foreach (var simpleDriveShaftGear in gears) {
            if (simpleDriveShaftGear.from != null) {
                simpleDriveShaftGear.from.gears.Remove(simpleDriveShaftGear);
            }
            if (simpleDriveShaftGear.to != null) {
                simpleDriveShaftGear.to.gears.Remove(simpleDriveShaftGear);
            }
        }
        gears.Clear();
    }

    private void FixedUpdate() {
        foreach (var simpleDriveShaftInput in inputs) {
            simpleDriveShaftInput.Update();
        }
        foreach (var simpleDriveShaftOutput in outputs) {
            simpleDriveShaftOutput.Update();
        }
    }

    [Serializable]
    public class SimpleDriveShaftGear {
        public SimpleDriveShaftInput from;

        public bool invert;

        public float ratio;

        public SimpleDriveShaftOutput to;

        public float torqueOffset;

        public SimpleDriveShaftGear(SimpleDriveShaftInput fromHub, SimpleDriveShaftOutput toHub, float r, bool i) {
            from = fromHub;
            to = toHub;
            ratio = r;
            invert = i;
        }

        public void AdjustGearRatio(float gearRatio) {
            ratio = gearRatio;
        }

        public void PassTorque(float torque, float maxW) {
            if (Mathf.Abs(ratio) < 1E-07f) {
                return;
            }
            to.AddTorque((torque + torqueOffset) / ratio * ((!invert) ? 1f : -1f), maxW * ratio);
        }

        public void PassW(float W) {
            if (Mathf.Abs(ratio) < 1E-07f) {
                return;
            }
            from.SetW(W / ratio * ((!invert) ? 1f : -1f));
        }

        public void SetTorqueOffset(float torque) {
            torqueOffset = torque;
        }
    }

    [Serializable]
    public class SimpleDriveShaftInput {
        public float currentMaxW;

        public float currentT;

        public float currentW;

        public List<SimpleDriveShaftGear> gears = new List<SimpleDriveShaftGear>();

        public List<SimpleDriveShaftOutput> hubs = new List<SimpleDriveShaftOutput>();

        public Property.PropertyRefrence propertyRefrence;

        private float totalMaxW;

        private float totalT;

        private float totalW;

        public SimpleDriveShaftInput(Property.PropertyRefrence pref) {
            propertyRefrence = pref;
        }

        public void AssignGear(SimpleDriveShaftGear g) {
            if (!gears.Contains(g)) {
                gears.Add(g);
            }
        }

        public void AssignOutput(SimpleDriveShaftOutput o) {
            foreach (var simpleDriveShaftOutput in hubs) {
                if (simpleDriveShaftOutput.propertyRefrence.part == o.propertyRefrence.part && simpleDriveShaftOutput.propertyRefrence.propertyName == o.propertyRefrence.propertyName) {
                    hubs.Remove(simpleDriveShaftOutput);
                    hubs.Add(o);
                    return;
                }
            }
            hubs.Add(o);
        }

        public void PassTorque(float torque, float maxW) {
            foreach (var simpleDriveShaftGear in gears) {
                simpleDriveShaftGear.PassTorque(torque / (float)gears.Count, maxW);
            }
            totalT += torque;
            if (Mathf.Abs(totalMaxW) > Mathf.Abs(maxW)) {
                totalMaxW = maxW;
            }
        }

        public void SetW(float W) {
            var count = hubs.Count;
            for (var i = 0; i < count; i++) {
                hubs[i].PassW(W);
            }
            if (Mathf.Abs(W) < Mathf.Abs(totalW)) {
                totalW = W;
            }
        }

        public void Update() {
            currentT = totalT;
            currentMaxW = totalMaxW;
            totalT = 0f;
            totalMaxW = 9999999f;
            if (Mathf.Abs(totalW) < 9999999f) {
                currentW = totalW;
            } else {
                currentW = 0f;
            }
            totalW = 9999999f;
        }
    }

    [Serializable]
    public class SimpleDriveShaftOutput {
        public float currentMaxW;

        public float currentT;

        public float currentW;

        public List<SimpleDriveShaftGear> gears = new List<SimpleDriveShaftGear>();

        public List<SimpleDriveShaftInput> hubs = new List<SimpleDriveShaftInput>();

        public Property.PropertyRefrence propertyRefrence;

        private float totalMaxW;

        private float totalT;

        private float totalW;

        public SimpleDriveShaftOutput(Property.PropertyRefrence pref) {
            propertyRefrence = pref;
        }

        public void AddTorque(float torque, float maxW = 0f) {
            totalT += torque;
            if (Mathf.Abs(totalMaxW) > Mathf.Abs(maxW)) {
                totalMaxW = maxW;
            }
            if (hubs.Count == 0) {
                PassW(HandleWhere(torque, maxW));
                return;
            }
            foreach (var simpleDriveShaftInput in hubs) {
                simpleDriveShaftInput.PassTorque(torque / (float)hubs.Count, maxW);
            }
        }

        public void AssignGear(SimpleDriveShaftGear g) {
            if (!gears.Contains(g)) {
                gears.Add(g);
            }
        }

        public void AssignInput(SimpleDriveShaftInput i) {
            foreach (var simpleDriveShaftInput in hubs) {
                if (simpleDriveShaftInput.propertyRefrence.part == i.propertyRefrence.part
                    && simpleDriveShaftInput.propertyRefrence.propertyName == i.propertyRefrence.propertyName) {
                    hubs.Remove(simpleDriveShaftInput);
                    hubs.Add(i);
                    return;
                }
            }
            hubs.Add(i);
            if (!hubs.Contains(i)) {
                hubs.Add(i);
            }
        }

        public void PassW(float W) {
            foreach (var simpleDriveShaftGear in gears) {
                simpleDriveShaftGear.PassW(W);
            }
            if (Mathf.Abs(W) < Mathf.Abs(totalW)) {
                totalW = W;
            }
        }

        public void Update() {
            currentT = totalT;
            currentMaxW = totalMaxW;
            totalT = 0f;
            totalMaxW = 9999999f;
            if (Mathf.Abs(totalW) < 9999999f) {
                currentW = totalW;
            } else {
                currentW = 0f;
            }
            totalW = 9999999f;
        }

        private float HandleWhere(float torque, float maxW) {
            var num = currentW + (torque / 1f * Time.fixedDeltaTime);
            if (Mathf.Abs(torque) < 0.01f) {
                num -= Mathf.Clamp(num * 0.003f * 1f, -Mathf.Abs(num), Mathf.Abs(num));
            }
            return Mathf.Clamp(num, -Mathf.Abs(maxW), Mathf.Abs(maxW));
        }
    }
}