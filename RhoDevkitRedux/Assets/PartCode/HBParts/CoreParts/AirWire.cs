﻿using System.Collections.Generic;
using UnityEngine;
[HBS.SerializeComponentOnlyAttribute]
public class AirWire : HBLink {

    public List<AirWireInput> inputs = new List<AirWireInput>();
    public List<AirWireOutput> outputs = new List<AirWireOutput>();

    public AirWireOutput NewOutput(Property outputProp) {
        AirWireOutput newOutput = new AirWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));
        outputs.Add(newOutput);

        return newOutput;
    }
    public AirWireInput NewInput(Property inputProp) {
        AirWireInput newInput = new AirWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));
        inputs.Add(newInput);

        return newInput;
    }
    public AirWireOutput NewOutput(string id) {
        AirWireOutput newOutput = new AirWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        outputs.Add(newOutput);

        return newOutput;
    }
    public AirWireInput NewInput(string id) {
        AirWireInput newInput = new AirWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));
        inputs.Add(newInput);

        return newInput;
    }
    public AirWireOutput NewOutputLua(string pname) {
        AirWireOutput newOutput = new AirWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));
        outputs.Add(newOutput);

        return newOutput;
    }
    public AirWireInput NewInputLua(string pname) {
        AirWireInput newInput = new AirWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));
        inputs.Add(newInput);

        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (AirWireOutput output in outputs) {
            output.ClearLinks();
        }
    }
    public void ClearInputsFromOutput(AirWireOutput output) {
        output.ClearLinks();
    }
    public void RemoveInputFromOutput(AirWireOutput output, AirWireInput input) {
        output.UnassignOutput(input);
    }
    public void AssignInputToOutput(AirWireOutput output, Property.PropertyRefrence inputProp) {

        AirWire targetAirWire = inputProp.part.GetComponent<AirWire>();
        if (targetAirWire == null) {
            Debug.LogError("target property doesnt have a AirWire : coudnt have made this link");
            return;
        }

        AirWireInput targetAirWireInput = targetAirWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetAirWireInput == null) {
            Debug.LogError("targetAirWire couldnt find a propertyInput:" + inputProp.propertyName);
            return;
        }

        output.AssignInput(targetAirWireInput);
        targetAirWireInput.AssignOutput(output);

    }

    void Update() {
        foreach (AirWireInput input in inputs) {
            input.Update();
        }
        foreach (AirWireOutput output in outputs) {
            output.Update();
        }
    }
    void LateUpdate() {
        foreach (AirWireInput input in inputs) {
            input.LateUpdate();
        }
        foreach (AirWireOutput output in outputs) {
            output.LateUpdate();
        }
    }

    public AirWireOutput FindOutputFromPropertyName(string pname) {
        foreach (AirWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public AirWireInput FindInputFromPropertyName(string pname) {
        foreach (AirWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for electricwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        Reset();
    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other AirWire properties on pass2

        //link each output to its inputs
        foreach (AirWireOutput output in outputs) {
            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Air) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a AirWire output to a non AirWire input property OR property part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }


    public class AirWireInput {

        public float curLiterIntake = 0f; //Ls
        private float literIntake = 0f;
        public float curLiterDemand = 0f; //Ls
        public float curFlowRate = 1f;
        private float flowRate = 1f;
        private int flowRateCount = 0;
        private float fuelDemand = 0f; //Ls

        public List<AirWireOutput> outputs = new List<AirWireOutput>();
        public Property.PropertyRefrence propertyRefrence;

        public AirWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignOutput(AirWireOutput output) {
            if (outputs.Contains(output) == false) {
                outputs.Add(output);
            }
        }
        public void UnassignOutput(AirWireOutput output) {
            outputs.RemoveAll(AirWireOutput => AirWireOutput == output);
        }
        public void ClearOutptus() {
            outputs.Clear();
        }

        public void Set(float gotWhatAskedFac, float literFlowRate) {
            flowRate += literFlowRate; //sum up the burn rate , to avg on LateUpdate
            flowRateCount++;
            if (gotWhatAskedFac > 0) {//dont mattre what fac , if we have a druplet of fuel jsut spend it
                literIntake = curLiterDemand; //sum up the fuel intake
            } else {
                literIntake = 0f;
            }
        }
        void Pass(float demand) {

            //spread demand over outptus wich have fuel
            int outputCount = 0;
            foreach (AirWireOutput output in outputs) {
                if (output.curLiters > 0f) {
                    outputCount++;
                }
            }

            foreach (AirWireOutput output in outputs) {
                if (output.curLiters > 0f) {
                    output.Set(demand / (float)outputCount);
                }
            }
        }
        public float GetPowerFactor() {
            if (curLiterDemand == 0) {
                return 0f;
            }
            if (curLiterIntake == 0) {
                return 0f;
            }
            return (curLiterIntake / curLiterDemand) * curFlowRate;
        }
        public void LateUpdate() {
            curLiterDemand = fuelDemand;
            fuelDemand = 0f;

            curLiterIntake = literIntake;
            literIntake = 0f;

            //take avg burnrate of the fuel intakes we got
            curFlowRate = flowRate / (float)flowRateCount;
            flowRateCount = 0;
            flowRate = 0f;
        }
        public void Update() {
            Pass(curLiterDemand);
        }

        public void Consume(float litersPerSec) {
            fuelDemand = litersPerSec;
        }

    }

    public class AirWireOutput {

        public float curLiters = 0f; //L
        public float curFlowRate = 1f;
        private float literDemand = 0f;//Ls
        public float curLiterDemand = 0f;//Ls use this to take away from curFuel at the fueltank, then use SetFuel to update the curFuel

        public List<AirWireInput> inputs = new List<AirWireInput>();
        public Property.PropertyRefrence propertyRefrence;
        public AirWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignInput(AirWireInput input) {
            if (inputs.Contains(input) == false) {
                inputs.Add(input);
            }
        }
        public void UnassignOutput(AirWireInput input) {
            inputs.RemoveAll(AirWireInput => AirWireInput == input);
        }
        public void ClearLinks() {
            inputs.Clear();
        }

        public void Set(float litersPerSecDemand) {
            literDemand += litersPerSecDemand;

        }

        public void Update() {
            Pass();
        }

        public void LateUpdate() {
            curLiterDemand = literDemand;
            literDemand = 0f;
        }

        void Pass() {
            if (curLiters > 0) {
                foreach (AirWireInput input in inputs) {
                    input.Set(1f, curFlowRate);
                }
            }
        }

        public void SetLiters(float litersLeft, float flowRate) {
            curLiters = litersLeft;
            curFlowRate = flowRate;
        }
    }
}