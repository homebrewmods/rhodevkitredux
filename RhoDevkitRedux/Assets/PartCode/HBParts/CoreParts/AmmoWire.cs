﻿using System.Collections.Generic;
using UnityEngine;
[HBS.SerializeComponentOnlyAttribute]
public class AmmoWire : HBLink {

    //AmmoFLoatWireOutput.SetAmmo() <- in whatever interval ( slowupdate or someting )
    //at the ammo box check for ammoUsed , to substract from own ammo , after doing so reset the ammoUsed back to 0
    //so......SlowUpdate() myAmmo -= outputWire.usedAmmo; outputWire.usedAmmo = 0; outputWire.SetAmmo(myAmmo);
    //at the gun use AmmoWireInput.GetAvailableAmmo() to check if we may shoot and use AmmoWireInput.ConsumeAmmo(bulletWeight) on shoot

    public List<AmmoWireInput> inputs = new List<AmmoWireInput>();
    public List<AmmoWireOutput> outputs = new List<AmmoWireOutput>();

    public AmmoWireOutput NewOutput(Property outputProp) {
        //reads straight from properties

        //NEW
        foreach (AmmoWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //found return this
                return o;
            }
        }
        //NEW

        //create new AmmoWireOutput
        AmmoWireOutput newOutput = new AmmoWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public AmmoWireInput NewInput(Property inputProp) {
        //reads straight from properties

        //NEW
        foreach (AmmoWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //found return this
                return i;
            }
        }
        //NEW


        //create new AmmoWireOutput
        AmmoWireInput newInput = new AmmoWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public AmmoWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties

        //NEW
        foreach (AmmoWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //found return this
                return o;
            }
        }
        //NEW


        //create new AmmoWireOutput
        AmmoWireOutput newOutput = new AmmoWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public AmmoWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties

        //NEW
        foreach (AmmoWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //found return this
                return i;
            }
        }
        //NEW

        //create new AmmoWireOutput
        AmmoWireInput newInput = new AmmoWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }

    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (AmmoWireOutput output in outputs) {
            output.ClearLinks();
        }
    }
    public void ClearInputsFromOutput(AmmoWireOutput output) {
        output.ClearLinks();
    }
    public void RemoveInputFromOutput(AmmoWireOutput output, AmmoWireInput input) {
        output.UnassignOutput(input);
    }
    public void AssignInputToOutput(AmmoWireOutput output, Property.PropertyRefrence inputProp) {

        AmmoWire targetAmmoWire = inputProp.part.GetComponent<AmmoWire>();
        if (targetAmmoWire == null) {
            Debug.LogError("target property doesnt have a AmmoWire : coudnt have made this link");
            return;
        }

        //Debug.Log("found target AmmoWire :" + targetAmmoWire.gameObject.ToString());

        AmmoWireInput targetAmmoWireInput = targetAmmoWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetAmmoWireInput == null) {
            Debug.LogError("targetAmmoWire couldnt find a propertyInput:" + inputProp.propertyName);
            return;
        }

        //assign targetAmmoWireInput to this AmmoWireOutput
        output.AssignInput(targetAmmoWireInput);
        targetAmmoWireInput.AssignOutput(output);

        //Debug.Log("AmmoWire linked " + output.propertyRefrence.GetProperty().pname + " to " + inputProp.propertyName);
    }

    public AmmoWireOutput FindOutputFromPropertyName(string pname) {
        foreach (AmmoWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public AmmoWireInput FindInputFromPropertyName(string pname) {
        foreach (AmmoWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for electricwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); NEW

        //cleanup inputs list
        List<AmmoWireInput> blackList = new List<AmmoWireInput>();
        foreach (AmmoWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (AmmoWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<AmmoWireOutput> blackList2 = new List<AmmoWireOutput>();
        foreach (AmmoWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (AmmoWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }

    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other AmmoWire properties on pass2

        //link each output to its inputs
        foreach (AmmoWireOutput output in outputs) {
            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Ammo) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a AmmoWire output to a non AmmoWire input property OR property part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }


    public class AmmoWireInput {

        public List<AmmoWireOutput> outputs = new List<AmmoWireOutput>();
        public Property.PropertyRefrence propertyRefrence;

        public AmmoWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignOutput(AmmoWireOutput output) {
            if (outputs.Contains(output) == false) {
                outputs.Add(output);
            }
        }
        public void UnassignOutput(AmmoWireOutput output) {
            outputs.RemoveAll(AmmoWireOutput => AmmoWireOutput == output);
        }
        public void ClearOutptus() {
            outputs.Clear();
        }

        public float GetAvailableAmmo() {
            float ret = 0f;
            foreach (AmmoWireOutput output in outputs) {
                ret += Mathf.Clamp(output.curAmmo - output.curUsedAmmo, 0, 10000000f);
            }
            return ret;
        }

        public void ConsumeAmmo(float demand) {
            if (float.IsNaN(demand)) {
                demand = 1f;
            }

            //Ammo deplete one by one
            foreach (AmmoWireOutput output in outputs) {
                if (output.curAmmo - output.curUsedAmmo > demand) {
                    output.Pass(demand);
                    break;
                }
            }
        }

    }

    public class AmmoWireOutput {

        public float curAmmo = 0f; //L
        public float curUsedAmmo = 0f;

        public List<AmmoWireInput> inputs = new List<AmmoWireInput>();
        public Property.PropertyRefrence propertyRefrence;
        public AmmoWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignInput(AmmoWireInput input) {
            if (inputs.Contains(input) == false) {
                inputs.Add(input);
            }
        }
        public void UnassignOutput(AmmoWireInput input) {
            inputs.RemoveAll(AmmoWireInput => AmmoWireInput == input);
        }
        public void ClearLinks() {
            inputs.Clear();
        }


        public void Pass(float demand) {
            curUsedAmmo += demand;
        }

        public void SetAmmo(float volumeLeft) {
            curAmmo = volumeLeft;
        }
    }
}


