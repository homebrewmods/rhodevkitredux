﻿using System.Collections.Generic;
using UnityEngine;

[HBS.SerializeComponentOnlyAttribute]
public class FuelWire : HBLink {

    //fuel wire simply passes floats from FuelWireOutputs to FuelWireInputs
    //FuelWireItself acts like a container to support multyple Outputs going to multyple Inputs , or Multyple Inputs can have multyple OUputs linked to them
    //so how to setup a Fuelwire..
    //first reset the FuelWire
    //first let the part use the NewOutput and NewInput functions when it reads from properties and basicly setup our inputs outputs list for all FuelWires on all parts
    //then in pass 2 we can simply look at the property our outputs have and link them to the appropriate FLoatWireInput
    //on pass 3 all FuelWires on all Parts should have their outputs linked, we can then generate a mesh to visually display this link

    //how to use FuelWireInput , Consume on Update or SlowUpdate, use bool haveFuel to check run part
    //how to use FuelWireOutput , Pass curFuel present in the fueltank on Update or SlowUpdate

    //the output passes float of fuel given / consumer demands and a bool gotFuel , use the Input.haveFuel to know if the part can be runned , while the part runs , use Input.Consume(liter) , 
    //the consumers demand will be counted up at the outputs until they pass their fuel again , when they pass it the method returns hoowmush was consumed, use this to decrease the curfuel of the fueltank
    //when a consumer breaks it must set consume(0) so it stops "eating" fuel

    public List<FuelWireInput> inputs = new List<FuelWireInput>();
    public List<FuelWireOutput> outputs = new List<FuelWireOutput>();

    public FuelWireOutput NewOutput(Property outputProp) {
        //reads straight from properties

        //NEW
        foreach (FuelWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == outputProp.pname) {
                //found return this
                return o;
            }
        }
        //NEW

        //create new FuelWireOutput
        FuelWireOutput newOutput = new FuelWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), outputProp.pname));

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FuelWireInput NewInput(Property inputProp) {
        //reads straight from properties

        //NEW
        foreach (FuelWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == inputProp.pname) {
                //found return this
                return i;
            }
        }
        //NEW


        //create new FuelWireOutput
        FuelWireInput newInput = new FuelWireInput(new Property.PropertyRefrence(GetComponent<Part>(), inputProp.pname));

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public FuelWireOutput NewOutput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties

        //NEW
        foreach (FuelWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == id) {
                //found return this
                return o;
            }
        }
        //NEW


        //create new FuelWireOutput
        FuelWireOutput newOutput = new FuelWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FuelWireInput NewInput(string id) {
        //create new in or output with a custom id ( will be hookedup manually )

        //reads straight from properties

        //NEW
        foreach (FuelWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == id) {
                //found return this
                return i;
            }
        }
        //NEW

        //create new FuelWireOutput
        FuelWireInput newInput = new FuelWireInput(new Property.PropertyRefrence(GetComponent<Part>(), id));

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public FuelWireOutput NewOutputLua(string pname) {
        //reads straight from properties

        //NEW
        foreach (FuelWireOutput o in outputs) {
            if (o.propertyRefrence.propertyName == pname) {
                //found return this
                return o;
            }
        }
        //NEW


        //create new FuelWireOutput
        FuelWireOutput newOutput = new FuelWireOutput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //add this output to the list
        outputs.Add(newOutput);

        //return this new output
        return newOutput;
    }
    public FuelWireInput NewInputLua(string pname) {
        //reads straight from properties

        //NEW
        foreach (FuelWireInput i in inputs) {
            if (i.propertyRefrence.propertyName == pname) {
                //found return this
                return i;
            }
        }
        //NEW

        //create new FuelWireOutput
        FuelWireInput newInput = new FuelWireInput(new Property.PropertyRefrence(GetComponent<Part>(), pname));

        //add this output to the list
        inputs.Add(newInput);

        //return this new output
        return newInput;
    }
    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (FuelWireOutput output in outputs) {
            output.ClearLinks();
        }
    }
    public void ClearInputsFromOutput(FuelWireOutput output) {
        output.ClearLinks();
    }
    public void RemoveInputFromOutput(FuelWireOutput output, FuelWireInput input) {
        output.UnassignOutput(input);
    }
    public void AssignInputToOutput(FuelWireOutput output, Property.PropertyRefrence inputProp) {

        FuelWire targetFuelWire = inputProp.part.GetComponent<FuelWire>();
        if (targetFuelWire == null) {
            Debug.LogError("target property doesnt have a FuelWire : coudnt have made this link");
            return;
        }

        //Debug.Log("found target fuelwire :" + targetFuelWire.gameObject.ToString());

        FuelWireInput targetFuelWireInput = targetFuelWire.FindInputFromPropertyName(inputProp.propertyName);
        if (targetFuelWireInput == null) {
            Debug.LogError("targetFuelWire couldnt find a propertyInput:" + inputProp.propertyName);
            return;
        }

        //assign targetFuelWireInput to this FuelWireOutput
        output.AssignInput(targetFuelWireInput);
        targetFuelWireInput.AssignOutput(output);

        //Debug.Log("FuelWire linked " + output.propertyRefrence.GetProperty().pname + " to " + inputProp.propertyName);
    }

    void Update() {
        foreach (FuelWireInput input in inputs) {
            input.Update();
        }
        foreach (FuelWireOutput output in outputs) {
            output.Update();
        }
    }
    void LateUpdate() {
        foreach (FuelWireInput input in inputs) {
            input.LateUpdate();
        }
        foreach (FuelWireOutput output in outputs) {
            output.LateUpdate();
        }
    }

    public FuelWireOutput FindOutputFromPropertyName(string pname) {
        foreach (FuelWireOutput output in outputs) {
            if (output.propertyRefrence.propertyName == pname) {
                return output;
            }
        }
        return null;
    }
    public FuelWireInput FindInputFromPropertyName(string pname) {
        foreach (FuelWireInput input in inputs) {
            if (input.propertyRefrence.propertyName == pname) {
                return input;
            }
        }
        return null;
    }

    public void CreateMeshes() {
        //if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for electricwire meshes

        //}
    }

    public override void ReadFromPropertiesPassImmediate() {
        //Reset(); NEW

        //cleanup inputs list
        List<FuelWireInput> blackList = new List<FuelWireInput>();
        foreach (FuelWireInput i in inputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.PInput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.inputToOutputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (FuelWireInput bl in blackList) {
            inputs.Remove(bl);
        }


        //cleanup output list
        List<FuelWireOutput> blackList2 = new List<FuelWireOutput>();
        foreach (FuelWireOutput i in outputs) {
            bool found = false;
            //find the input property refrence in the part 
            foreach (Property p in GetComponent<Part>().properties) {
                if (p.type == PropertyType.POutput && p.pname == i.propertyRefrence.propertyName) {
                    if (p.outputToInputProperties.Count > 0) {
                        found = true;
                        break;
                    }
                }
            }
            if (found == false) {
                blackList2.Add(i);
            }
        }
        //remove inputs with property refrences that dont exist anymore in the proeprties of the part
        foreach (FuelWireOutput bl in blackList2) {
            outputs.Remove(bl);
        }

    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other FuelWire properties on pass2

        //link each output to its inputs
        foreach (FuelWireOutput output in outputs) {
            foreach (Property.PropertyRefrence otiProp in output.propertyRefrence.GetProperty().outputToInputProperties) {
                if (otiProp.GetProperty() != null && otiProp.GetProperty().linkType == PropertyLinkType.Fuel) {
                    AssignInputToOutput(output, otiProp);
                } else {
                    Debug.LogError("Somehow linked a FuelWire output to a non FuelWire input property OR propert part is null");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }


    public class FuelWireInput {

        public float curFuelIntake = 0f; //Ls
        private float fuelIntake = 0f;
        public float curFuelDemand = 0f; //Ls
        public float curBurnRate = 1f;
        private float burnRate = 1f;
        private int burnRateCount = 0;
        private float fuelDemand = 0f; //Ls

        public List<FuelWireOutput> outputs = new List<FuelWireOutput>();
        public Property.PropertyRefrence propertyRefrence;

        public FuelWireInput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignOutput(FuelWireOutput output) {
            if (outputs.Contains(output) == false) {
                outputs.Add(output);
            }
        }
        public void UnassignOutput(FuelWireOutput output) {
            outputs.RemoveAll(FuelWireOutput => FuelWireOutput == output);
        }
        public void ClearOutptus() {
            outputs.Clear();
        }

        public void Set(float gotWhatAskedFac, float fuelBurnRate = 1f) {
            if (float.IsNaN(fuelBurnRate)) {
                fuelBurnRate = 1f;
            }
            burnRate += fuelBurnRate; //sum up the burn rate , to avg on LateUpdate
            burnRateCount++;
            if (gotWhatAskedFac > 0) {//dont mattre what fac , if we have a druplet of fuel jsut spend it
                fuelIntake = curFuelDemand; //sum up the fuel intake
            } else {
                fuelIntake = 0f;
            }
        }
        void Pass(float demand) {
            if (float.IsNaN(demand)) {
                demand = 1f;
            }
            //spread demand over outptus wich have fuel
            int outputCount = 0;
            foreach (FuelWireOutput output in outputs) {
                if (output.curFuel > 0f) {
                    outputCount++;
                }
            }

            foreach (FuelWireOutput output in outputs) {
                if (output.curFuel > 0f) {
                    output.Set(demand / (float)outputCount);
                }
            }
        }
        public float GetPowerFactor() {
            if (curFuelDemand == 0) {
                return 0f;
            }
            if (curFuelIntake == 0) {
                return 0f;
            }
            //ok so this if fckt the numbers are not stable so , jsut reutn a fcking 1 * curburnrate pfff
            return 1f *curBurnRate;
            //return (curFuelIntake / curFuelDemand) * curBurnRate;
        }
        public void LateUpdate() {
            curFuelDemand = fuelDemand;
            fuelDemand = 0f;

            curFuelIntake = fuelIntake;
            fuelIntake = 0f;

            //take avg burnrate of the fuel intakes we got
            curBurnRate = burnRate / (float)burnRateCount;
            burnRateCount = 0;
            burnRate = 0f;
        }
        public void Update() {
            Pass(curFuelDemand);
        }

        public void Consume(float litersPerSec) {
            if (float.IsNaN(litersPerSec)) { litersPerSec = 0f; }
            fuelDemand = litersPerSec;
        }

    }

    public class FuelWireOutput {

        public float curFuel = 0f; //L
        public float curBurnRate = 1f;
        private float fuelDemand = 0f;//Ls
        public float curFuelDemand = 0f;//Ls use this to take away from curFuel at the fueltank, then use SetFuel to update the curFuel

        public List<FuelWireInput> inputs = new List<FuelWireInput>();
        public Property.PropertyRefrence propertyRefrence;
        public FuelWireOutput(Property.PropertyRefrence p) {
            propertyRefrence = p;
        }

        public void AssignInput(FuelWireInput input) {
            if (inputs.Contains(input) == false) {
                inputs.Add(input);
            }
        }
        public void UnassignOutput(FuelWireInput input) {
            inputs.RemoveAll(FuelWireInput => FuelWireInput == input);
        }
        public void ClearLinks() {
            inputs.Clear();
        }

        public void Set(float litersPerSecDemand) {
            fuelDemand += litersPerSecDemand;
        }

        public void Update() {
            Pass();
        }

        public void LateUpdate() {
            curFuelDemand = fuelDemand;
            fuelDemand = 0f;
        }

        void Pass() {
            if (curFuel > 0) {
                foreach (FuelWireInput input in inputs) {
                    input.Set(1f,curBurnRate);
                }
            }
        }

        public void SetFuel(float litersLeft, float burnRate = 1f) {
            curFuel = litersLeft;
            curBurnRate = burnRate;
        }
    }
}







/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FuelWire : HBLink {

    //FuelWire transfers fuel values to other FuelWires , one FuelWireOutput can go to multyple FuelWireInputs
    //parts may have multyple FuelWireOutputs 
    //the FuelWire tslef transfers a bool haveFuel , and a combustion factor
    //so how to setup FuelWires
    //first Reset the FuelWire 
    //then let the part create its Inputs and Outputs when reading from properties via the NewInput/Ouput functions 
    //on pass 2 let all the outputs link to their inputs

    public List<FuelWireOutput> outputs = new List<FuelWireOutput>();
    public List<FuelWireInput> inputs = new List<FuelWireInput>();

    public FuelWireInput NewInput(Property prop) {
        FuelWireInput newInput = new FuelWireInput(prop);
        inputs.Add(newInput);
        return newInput;
    }
    public FuelWireOutput NewOutput(Property prop) {
        FuelWireOutput newOutput = new FuelWireOutput(prop);
        outputs.Add(newOutput);
        return newOutput;
    }

    public void Reset() {
        inputs.Clear();
        outputs.Clear();
    }
    public void ClearAllInputsToOutputs() {
        foreach (FuelWireOutput output in outputs) {
            output.inputs.Clear();
        }
    }
    public void ClearInputsFromOutput(FuelWireOutput output) {
        output.inputs.Clear();
    }
    public void RemoveInputFromOutput(FuelWireOutput output, FuelWireInput input) {
        output.inputs.RemoveAll(FuelWireInput => FuelWireInput == input);
    }
    public void AssignInputToOutput(FuelWireOutput output, Property inputProp) {
        if (inputProp.parent == null) {
            Debug.LogError("property doesnt have a (Properties) parent");
            return;
        }

        FuelWire targetFuelWire = inputProp.parent.GetComponent<FuelWire>();
        if (targetFuelWire == null) {
            Debug.LogError("target property doesnt have a fuelwire : coudnt have made this link");
            return;
        }

        FuelWireInput targetFuelWireInput = targetFuelWire.FindInputFromProperty(inputProp);
        if (targetFuelWireInput == null) {
            Debug.LogError("targetFuelWire couldnt find a propertyInput with this particular property assigned on AssignInputToOutput");
            return;
        }

        //assign targetFloatWireInput to this FloatWireOutput
        output.inputs.Add(targetFuelWireInput);

        Debug.Log("linked " + output.prop.pname + " to " + inputProp.pname);
    }

    public override void ReadFromPropertiesPassImmediate() {
        Reset();
    }
    public override void ReadFromPropertiesPass2() {

        //auto link to other FloatWire properties on pass2

        //link each output to its inputs
        foreach (FuelWireOutput output in outputs) {
            foreach (Property otiProp in output.prop.outputToInputProperties) {
                if (otiProp.parent != null) {
                    if (otiProp.linkType == PropertyLinkType.Fuel) {
                        AssignInputToOutput(output, otiProp);
                    }
                    else {
                        Debug.LogError("Somehow linked a FLoatWire output to a non FloatWire output property");
                    }
                }
                else {
                    Debug.LogError("Property doesnt have a (Properties) parent");
                }
            }
        }
    }
    public override void ReadFromPropertiesPass3() {
        //Create meshes after everyting is linked
        CreateMeshes();
    }

    public void CreateMeshes() {
        if (HomebrewManager.HBLinkMeshLOD > 2) { //settings needs to be on highest quality for floatwire meshes

        }
    }

    public FuelWireOutput FindOutputFromProperty(Property prop) {
        foreach (FuelWireOutput output in outputs) {
            if (output.prop == prop) {
                return output;
            }
        }
        return null;
    }
    public FuelWireInput FindInputFromProperty(Property prop) {
        foreach (FuelWireInput input in inputs) {
            if (input.prop == prop) {
                return input;
            }
        }
        return null;
    }

    public class FuelWireOutput {
        public Property prop;
        public List<FuelWireInput> inputs = new List<FuelWireInput>();
        public float consumerRate = 1f;//howmush liter/hour is beeing consumed from this output
        private bool curHaveFuel = false;
        private float curCombustionFactor = 1f;
        
        public FuelWireOutput(Property myProp) {
            prop = myProp;
        }
       
        public void Pass(bool haveFuel = false, float combustionFactor = 1f ) {
            if (haveFuel == curHaveFuel && combustionFactor == curCombustionFactor) {
                foreach (FuelWireInput input in inputs) {
                    input.Set(haveFuel, combustionFactor,this);
                }
            }
        }
    }
    public class FuelWireInput {
        public Property prop;
        public bool haveFuel = false;
        public float combustionFactor = 1f;

        private List<FuelWirePackage> fuelPacks = new List<FuelWirePackage>();

        public FuelWireInput(Property myProp) {
            prop = myProp;
        }

        public void Consume(float consumeRate) {
            //spread consume rate over all outputs
            foreach (FuelWirePackage pack in fuelPacks) {
                pack.from.consumerRate = consumeRate / fuelPacks.Count;
            }
        }
        
        public void Set(bool fuel, float combustFactor , FuelWireOutput from ) {
            foreach (FuelWirePackage pack in fuelPacks) {
                if (pack.from == from) {
                    pack.haveFuel = fuel;
                    pack.combustionFactor = combustFactor;
                    return;
                }
            }

            //not found a fualpack for this FuelWireOutput
            FuelWirePackage newPack = new FuelWirePackage(fuel, combustFactor, from);
            fuelPacks.Add(newPack);

            //calc the total fuel values
            bool hf = false;
            float avgCF = 0f;
            foreach (FuelWirePackage pack in fuelPacks) {
                if (hf == false && pack.haveFuel == true) {
                    hf = true;
                }
                avgCF += pack.combustionFactor;
            }
            avgCF /= (float)fuelPacks.Count;

            haveFuel = hf;
            combustionFactor = avgCF;
        }

        class FuelWirePackage {
            public FuelWireOutput from;
            public bool haveFuel;
            public float combustionFactor;
            public FuelWirePackage(bool fuel, float combustFactor, FuelWireOutput f) {
                from = f;
                haveFuel = fuel;
                combustionFactor = combustFactor;
            }
        }
    }
}
*/
