using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBWeight : Part
{
    [HBS.SerializePartVar]
    public bool useScaleMethod;
    [HBS.SerializePartVar]
    public HBWeight.ScaleMethod scaleMethod;
    public override void ReadFromPropertiesPassImmediateNew() {}
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    [System.Serializable]
    public class ScaleMethod : object
    {
        public GameObject[] discs;
        public float discMassToOneScale;
        public float discThicknessToOneScale;
        public int maxDiscCount;
        public Vector3 localStackAxis;
        public void Configure(float mass) {}
    }
}
