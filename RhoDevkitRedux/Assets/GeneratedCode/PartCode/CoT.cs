using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class CoT : MonoBehaviour
{
    public Vector3 thrust;
    public Vector3 cotOffset;
    public Quaternion cotRotationOffset;
    public bool inBuilder;
    public void SetThrustPoint(Vector3 localPos, Quaternion localRot) {}
    public Vector3 GetThrustPoint()
    {
        return Vector3.zero;
    }
    public Vector3 GetThrust()
    {
        return Vector3.zero;
    }
}
