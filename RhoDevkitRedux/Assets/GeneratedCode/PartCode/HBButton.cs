using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBButton : Part
{
    [HBS.SerializePartVar]
    public Transform[] moveables;
    [HBS.SerializePartVar]
    public bool constantlyUpdateOutputs;
    public FloatWire floatWire;
    public List<FloatWire.FloatWireOutput> outputs;
    public List<float> outputValues;
    public void UpdateOutputs() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void ReadFromPropertiesPass2New() {}
}
