using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBJointNob : Part
{
    public bool debug;
    [HBS.SerializePartVar]
    public bool isNew;
    [HBS.SerializePartVar]
    public AudioClip pistonSound;
    [HBS.SerializePartVar]
    public float minVolume;
    [HBS.SerializePartVar]
    public float maxVolume;
    [HBS.SerializePartVar]
    public float minPitch;
    [HBS.SerializePartVar]
    public float maxPitch;
    [HBS.SerializePartVar]
    public float maxAudioMovement;
    [HBS.SerializePartVar]
    public float audioFac;
    [HBS.SerializePartVar]
    public Transform jointPivot;
    [HBS.SerializePartVar]
    public HBJointNob.SpringTransform[] currentSprings;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    [SLua.CustomLuaClass]
    [HBS.SerializePart]
    [System.Serializable]
    public class SpringTransform : object
    {
        [HBS.SerializePartVar]
        public GameObject gameObject;
        [HBS.SerializePartVar]
        public Transform end;
        [HBS.SerializePartVar]
        public Transform from;
        [HBS.SerializePartVar]
        public Transform to;
        [HBS.SerializePartVar]
        public Joint joint;
        [HBS.SerializePartVar]
        public bool isFixedSpring;
        public void Destroy() {}
        public void CreateJoint(bool fixedSpring, float breakForce, float springForce, float springDamping, float length, float spread) {}
        public void Break() {}
        public void Apply(float force, float damper, float minDistance, float maxDistance) {}
        public void Update(bool isBuildable, float minDistance, float maxDistance, float minDistanceUnclamped, float maxDistanceUnclamped) {}
    }
}
