using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBWheel : Part
{
    public bool debug;
    [HBS.SerializePartVar]
    public float radius;
    [HBS.SerializePartVar]
    public float width;
    [HBS.SerializePartVar]
    public float mass;
    [HBS.SerializePartVar]
    public float spring;
    [HBS.SerializePartVar]
    public float damp;
    [HBS.SerializePartVar]
    public float springLength;
    [HBS.SerializePartVar]
    public LayerMask hitMask;
    [HBS.SerializePartVar]
    public Transform visualWheel;
    [HBS.SerializePartVar]
    public float sidewaysFrictionFactor;
    [HBS.SerializePartVar]
    public float forwardFrictionFactor;
    [HBS.SerializePartVar]
    public AnimationCurve rollingResistanceCurve;
    [HBS.SerializePartVar]
    public AnimationCurve sidewaysSlipCurve;
    [HBS.SerializePartVar]
    public AnimationCurve sidewaysSlipSkidCurve;
    [HBS.SerializePartVar]
    public AnimationCurve forwardSlipCurve;
    [HBS.SerializePartVar]
    public float massMultiplier;
    [HBS.SerializePartVar]
    public float radiusMultiplier;
    [HBS.SerializePartVar]
    public float widthMultiplier;
    public Rigidbody body;
    [UnityEngine.Header("values to set")]
    public float maxW;
    public float driveTorque;
    public float brakeTorque;
    public float forceFactor;
    [UnityEngine.Header("value to get")]
    public float W;
    public GroundSurfaceMaster.GroundSurface groundSurface;
    public bool isGrouned;
    public RaycastHit hit;
    public float relativeForwardVelocity;
    public float relativeSidewaysVelocity;
    public float springDistance;
}
