using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBSolidRocketMount : Part
{
    [HBS.SerializePartVar]
    public AnimationCurve forceCurve;
    [HBS.SerializePartVar]
    public AnimationCurve volumeCurve;
    [HBS.SerializePartVar]
    public float volumeMultplier;
    [HBS.SerializePartVar]
    public Gradient effectColor;
    [HBS.SerializePartVar]
    public AudioClip detachSound;
    [HBS.SerializePartVar]
    public AudioClip burnLoop;
    [HBS.SerializePartVar]
    public Transform rocketPivot;
    [HBS.SerializePartVar]
    public float totalForceAtDefaultSize;
    [HBS.SerializePartVar]
    public float totalBurnTimeAtDefaultSize;
    [HBS.SerializePartVar]
    public float watt;
    [HBS.SerializePartVar]
    public string preRocketName;
    [HBS.SerializePartVar]
    public GameObject rocketGameObject;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
