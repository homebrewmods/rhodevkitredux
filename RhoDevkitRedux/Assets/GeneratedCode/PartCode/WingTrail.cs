using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[SLua.CustomLuaClass]
public class WingTrail : MonoBehaviour
{
    public HBWing wing;
    public GameObject obj;
    public TrailRenderer renderer;
    public AnimationCurve curve;
    public float min;
    public float max;
    public float time;
    public float effect;
    public Color color;
    public Vector3 offset;
}
