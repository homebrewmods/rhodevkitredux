using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBHemiRotator : Part
{
    [HBS.SerializePartVar]
    public Transform target;
    [HBS.SerializePartVar]
    public Vector3 targetLocalRotationAxis;
    [HBS.SerializePartVar]
    public Vector3 targetLocalRotationAxis2;
    [HBS.SerializePartVar]
    public bool useDirectionalAngleLimits;
    [HBS.SerializePartVar]
    public float pitchLimit;
    [HBS.SerializePartVar]
    public float yawLimit;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
