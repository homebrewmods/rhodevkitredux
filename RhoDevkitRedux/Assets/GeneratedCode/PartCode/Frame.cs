using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public class Frame : MonoBehaviour
    {
        public BodyContainer bodyContainer
        {
            get
            {
                return new BodyContainer();
            }
        }
        [UnityEngine.HideInInspector]
        public Vector3[] verts;
        [UnityEngine.HideInInspector]
        public int[] lines;
        [UnityEngine.HideInInspector]
        public CurveHandle[] handles;
        [UnityEngine.HideInInspector]
        public LineData[] linesData;
        public Action onChange;
        public void TriggerChange() {}
        public void Copy(Frame other) {}
        public int FindLineDataMatch(string match)
        {
            return 0;
        }
        public int FindLineData(int vertIndex1, int vertIndex2)
        {
            return 0;
        }
        public int AddLineData(int vertIndex1, int vertIndex2, string data)
        {
            return 0;
        }
        public void RemoveLineData(int index) {}
        public void SetLineData(int index, string data) {}
        public string GetLineData(int index)
        {
            return "";
        }
        public int FindHandle(int vertIndex1, int vertIndex2)
        {
            return 0;
        }
        public int AddHandle(Vector3 direction, int vertIndex1, int vertIndex2, Space space)
        {
            return 0;
        }
        public void RemoveHandle(int index) {}
        public CurveHandle GetHandle(int index)
        {
            return new CurveHandle();
        }
        public Vector3 GetHandleDirection(int index, Space space)
        {
            return Vector3.zero;
        }
        public Vector3 GetHandlePosition(int index, Space space)
        {
            return Vector3.zero;
        }
        public void MoveHandle(int index, Vector3 direction, Space space) {}
        public void MoveHandlePosition(int index, Vector3 position, Space space) {}
        public int FindVert(Vector3 point, int ignoreIndex, float tolerance, Space space)
        {
            return 0;
        }
        public int AddVert(Vector3 point, Space space)
        {
            return 0;
        }
        public int InsertVert(int index1, int index2, Vector3 point, Space space)
        {
            return 0;
        }
        public void MoveVert(int index, Vector3 point, Space space) {}
        public Vector3 GetVert(int index, Space space)
        {
            return Vector3.zero;
        }
        public void RemoveUnconnectedVerts() {}
        public void RemoveVert(int index) {}
        public void AddLine(int index1, int index2) {}
        public int[] FindLines(int index)
        {
            return new int[0];
        }
        public void ReconnectLine(int indexOld, int indexNew) {}
        public void RemoveLines(int index) {}
        public void RemoveLine(int index1, int index2) {}
        public void Clear() {}
        public bool RaycastVerts(Ray ray, float vertRadius, out int index1, out Vector3 point1, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            point1 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
        public bool RaycastLines(Ray ray, float lineRadius, out int index1, out int index2, out Vector3 point1, out Vector3 point2, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            index2 = 0;
            point1 = Vector3.zero;
            point2 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
    }
}
