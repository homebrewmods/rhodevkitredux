using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBClock : Part
{
    public HBClockType type;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
