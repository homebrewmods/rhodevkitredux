using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    [System.Serializable]
    public class HullGeneratorType : object
    {
        public Material material;
        public float thickness;
    }
}
