using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.UI;
[HBS.SerializePart]
public class HBNumberPlate : Part
{
    public string numberText;
    public string topText;
    public string botText;
    public Text text;
    public Text tText;
    public Text bText;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
