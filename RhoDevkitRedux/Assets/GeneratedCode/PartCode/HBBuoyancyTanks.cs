using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBBuoyancyTanks : Part
{
    public float flowRate;
    public float waterFlowRate;
    public float filledFactor;
    public AirWire airWire;
    public AirWire.AirWireInput airInput;
    public FloatWire floatWire;
    public FloatWire.FloatWireInput axisInput;
    public FloatWire.FloatWireOutput currentFilledOutput;
    public float currentFilled;
    public float weight;
    public float buoyancyForce;
    public CoG cog;
    public Buoyancy[] buoyancies;
    public Vector3 localOffset;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
