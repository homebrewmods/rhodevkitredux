using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class ServerData : UnityEngine.Networking.MessageBase
    {
        public int connID;
        public string serverID;
        public string serverName;
        public bool update;
        public bool isPassworded;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
