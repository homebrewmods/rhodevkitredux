using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using UnityEngine.Audio;
[HBS.SerializeComponentOnly]
public class AudioMaster : MonoBehaviour
{
    public float volumeMultiplier
    {
        get
        {
            return 0;
        }
        set
        {
        }
    }
    public List<AudioMaster.AudioGroup> groups;
    public AudioMixerGroup mixer;
    public float spatialBlend;
    public List<AudioMaster.AudioOneshotGroup> oneshots;
    public void PlayOneshot(AudioClip clip, float volume, bool keepAlive, float range) {}
    public void RegisterAudioSource(AudioSourceParented a) {}
    public void UnRegisterAudioSource(AudioSourceParented a) {}
    public class AudioGroup : object
    {
        public AudioSource au;
        public List<AudioSourceParented> sources;
        public void UpdateAudio(float volumeMultiplier) {}
    }
    public class AudioOneshotGroup : object
    {
        public AudioSource au;
        public AudioClip clip;
        public bool keepAlive;
        public float time;
        public float initialVolume;
        public void PlayOneShot() {}
    }
}
