using System;
using UnityEngine;
using HBS;
using SLua;
[UnityEngine.RequireComponent(typeof(UnityEngine.Collider))]
[HBS.Serialize]
public class SurfaceTag : MonoBehaviour
{
    public GroundSurfaceType type;
    public GroundSurfaceType[] splatTypes;
}
