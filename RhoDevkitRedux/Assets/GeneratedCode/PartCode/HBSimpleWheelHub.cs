using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBSimpleWheelHub : Part
{
    public bool debug;
    [HBS.SerializePartVar]
    public Transform wheelPivot;
    [HBS.SerializePartVar]
    public Transform suspentionBone;
    [HBS.SerializePartVar]
    public Vector3 suspentionBoneTargetOffset;
    [HBS.SerializePartVar]
    public Transform steerPivot;
    [HBS.SerializePartVar]
    public Vector3 steerAxis;
    [HBS.SerializePartVar]
    public GameObject wheelGameObject;
    [HBS.SerializePartVar]
    public string preWheelName;
    [HBS.SerializePartVar]
    public GameObject curArrow;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
