using System;
using UnityEngine;
using HBS;
using SLua;
public class AudioOneshotGroup : object
{
    public AudioSource au;
    public AudioClip clip;
    public bool keepAlive;
    public float time;
    public float initialVolume;
    public void PlayOneShot() {}
}
