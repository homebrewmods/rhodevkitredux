using System;
using UnityEngine;
using HBS;
using SLua;
using System.Xml.Linq;
using System.Collections.Generic;
[System.Serializable]
public class HBuildNode : object
{
    public BuildNodeType type;
    public string ID;
    public RefrenceID parentID;
    public RefrenceID splitterID;
    public HBuildablePart buildablePart;
    public Vector3 localPosition;
    public Vector3 normal;
    public Vector3 up;
    [UnityEngine.HideInInspector]
    public bool isWelded;
    public HFace[] GetFaces()
    {
        return new HFace[0];
    }
    public void Weld() {}
    public void UnWeld() {}
    public void FromWeldXElement(XElement parent) {}
    public void ToWeldXElement(XElement parent) {}
    public void ToBakeXElement(XElement parent) {}
    public void FromBakeXElement(XElement parent) {}
    public void GetForwardWeldChain(List<Transform> forwardWeldChain) {}
    public void GetReverseWeldChain(List<Transform> reverseWeldChain) {}
    public void GetReverseTolerantWeldChain(List<Transform> reverseWeldChain, Transform notMe) {}
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
    public Quaternion GetRotation()
    {
        return Quaternion.identity;
    }
    public Quaternion GetLocalRotation()
    {
        return Quaternion.identity;
    }
    public Vector3 GetNormal()
    {
        return Vector3.zero;
    }
    public Vector3 GetUp()
    {
        return Vector3.zero;
    }
}
