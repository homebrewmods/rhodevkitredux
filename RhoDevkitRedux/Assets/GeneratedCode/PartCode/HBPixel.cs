using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBPixel : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireInput colorR;
    public FloatWire.FloatWireInput colorG;
    public FloatWire.FloatWireInput colorB;
    public FloatWire.FloatWireInput intensity;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
