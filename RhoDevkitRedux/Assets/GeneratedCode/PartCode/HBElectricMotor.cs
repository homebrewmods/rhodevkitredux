using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBElectricMotor : Part
{
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public float electricEngineTorqueCurveWatt;
    [HBS.SerializePartVar]
    public TorqueCurve motorTorque;
    [HBS.SerializePartVar]
    public HBAudioLoop motorLoop;
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public AudioClip engineThrottleLoop;
    public override void StartPart() {}
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
