using System;
using UnityEngine;
using HBS;
using SLua;
namespace Vectrosity
{
    public enum EndCap
    {
        Front = 0,
        Both = 1,
        Mirror = 2,
        Back = 3,
        None = 4
    }
}
