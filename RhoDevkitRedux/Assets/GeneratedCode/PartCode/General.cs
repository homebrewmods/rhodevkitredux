using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBNetworking
{
    [HBS.SerializeComponentOnly]
    [UnityEngine.RequireComponent(typeof(HBNetworking.NetworkBase))]
    public class General : MonoBehaviour
    {
        public void GeneralAddComponent(string comp) {}
        public void GeneralAddComponent(string comp, int connID) {}
        public void GeneralSendData(byte[] data) {}
    }
}
