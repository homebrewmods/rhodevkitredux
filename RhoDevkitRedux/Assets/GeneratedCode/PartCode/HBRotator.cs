using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBRotator : Part
{
    [HBS.SerializePartVar]
    public Transform target;
    [HBS.SerializePartVar]
    public Vector3 targetLocalRotationAxis;
    [HBS.SerializePartVar]
    public Quaternion initialRotation;
    [HBS.SerializePartVar]
    public bool useNewRotationMethod;
    public void ResetPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
