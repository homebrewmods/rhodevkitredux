using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
[System.Serializable]
public class SpringTransform : object
{
    [HBS.SerializePartVar]
    public GameObject gameObject;
    [HBS.SerializePartVar]
    public Transform end;
    [HBS.SerializePartVar]
    public Transform from;
    [HBS.SerializePartVar]
    public Transform to;
    [HBS.SerializePartVar]
    public Joint joint;
    [HBS.SerializePartVar]
    public bool isFixedSpring;
    public void Destroy() {}
    public void CreateJoint(bool fixedSpring, float breakForce, float springForce, float springDamping, float length, float spread) {}
    public void Break() {}
    public void Apply(float force, float damper, float minDistance, float maxDistance) {}
    public void Update(bool isBuildable, float minDistance, float maxDistance, float minDistanceUnclamped, float maxDistanceUnclamped) {}
}
