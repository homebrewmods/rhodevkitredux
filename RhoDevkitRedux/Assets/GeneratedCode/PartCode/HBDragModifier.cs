using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBDragModifier : Part
{
    public float curValue;
    public float drag;
    public float angularDrag;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
