using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class HBLensFlare : MonoBehaviour
{
    [UnityEngine.Header("Light")]
    public Light l;
    [UnityEngine.Header("Color")]
    public bool useGlobalColor;
    public Color globalColor;
    [UnityEngine.Header("Distance")]
    public bool useDistance;
    public Vector2 range;
    public Vector2 scale;
    public Color colorFrom;
    public Color colorTo;
    [UnityEngine.Header("SoftParticle")]
    public bool useSoftParticle;
    public Vector2 softParticleRange;
    public Vector2 softParticle;
    [UnityEngine.Header("Angle")]
    public bool useAngle;
    public bool useVertical;
    public bool useDoubleSided;
    public Vector2 angleRange;
    public Vector2 angleScale;
    public Color angleColorFrom;
    public Color angleColorTo;
    [UnityEngine.Header("Time")]
    public bool offAtDay;
    public Vector2 blinkTime;
    public bool useBlink;
    [UnityEngine.Header("Blend: blends 'scale' and 'color' between 'distance' and 'angle'")]
    public bool blend;
    [UnityEngine.Header("Properties")]
    public string shaderColorPropertyName;
    public Vector3 rotationOffset;
}
