using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBHinge : Part
{
    [HBS.SerializePartVar]
    public Vector3 targetLocalRotationAxis;
    [HBS.SerializePartVar]
    public Transform pivot;
    [HBS.SerializePartVar]
    public HingeJoint joint;
    [HBS.SerializePartVar]
    public GameObject jointTarget;
    [HBS.SerializePartVar]
    public GameObject rotationGizmo;
    [HBS.SerializePartVar]
    public GameObject limitMinGizmo;
    [HBS.SerializePartVar]
    public GameObject limitMaxGizmo;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
