using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class AddComponentData : UnityEngine.Networking.MessageBase
    {
        public string id;
        public string comp;
        public int connID;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
