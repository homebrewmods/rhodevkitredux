using System;
using UnityEngine;
using HBS;
using SLua;
using System.IO;
[HBS.SerializePart]
public class HBMemoryBox : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireInput resetInput;
    public FloatWire.FloatWireInput setInput;
    public FloatWire.FloatWireInput valueInput;
    public float value;
    public FloatWire.FloatWireOutput output;
    public bool addValue;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
