using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[SLua.CustomLuaClass]
public class CoF : MonoBehaviour
{
    public Vector3 offset;
    public Vector3 force;
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
}
