using System;
using UnityEngine;
using HBS;
using SLua;
public enum MathMethod
{
    Round = 0,
    Ceiling = 1,
    Floor = 2,
    Lerp = 3,
    LerpAngle = 4,
    InverseLerp = 5,
    DeltaAngle = 6,
    Log = 7,
    Pow = 8,
    Sqrt = 9,
    Mod = 10,
    Sign = 11,
    Abs = 12,
    Min = 13,
    Max = 14,
    MoveTowards = 15,
    MoveTowardsAngle = 16,
    Cos = 17,
    Sin = 18,
    Tan = 19,
    Acos = 20,
    Asin = 21,
    Atan = 22,
    Atan2 = 23,
    Remainder = 24
}
