using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class PingData : UnityEngine.Networking.MessageBase
    {
        public float deltaTime;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
