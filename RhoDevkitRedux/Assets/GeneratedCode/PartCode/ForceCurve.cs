using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[System.Serializable]
public class ForceCurve : object
{
    [HBS.SerializePartVar]
    public AnimationCurve curve;
    [HBS.SerializePartVar]
    public float maxSpeed;
    [HBS.SerializePartVar]
    public float maxForce;
    public float GetForce(float speed)
    {
        return 0;
    }
}
