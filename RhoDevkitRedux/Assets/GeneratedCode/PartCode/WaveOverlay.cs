using System;
using UnityEngine;
using HBS;
using SLua;
namespace Ceto
{
    public class WaveOverlay : object
    {
        public bool Kill
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public bool Hide
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public Vector3 Position
        {
            get
            {
                return Vector3.zero;
            }
            set
            {
            }
        }
        public Vector2 HalfSize
        {
            get
            {
                return Vector2.zero;
            }
            set
            {
            }
        }
        public float Rotation
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public float Creation
        {
            get
            {
                return 0;
            }
        }
        public float Age
        {
            get
            {
                return 0;
            }
        }
        public float Duration
        {
            get
            {
                return 0;
            }
        }
        public float NormalizedAge
        {
            get
            {
                return 0;
            }
        }
        public Vector4[] Corners
        {
            get
            {
                return new Vector4[0];
            }
        }
        public OverlayHeightTexture HeightTex
        {
            get
            {
                return new OverlayHeightTexture();
            }
            set
            {
            }
        }
        public OverlayNormalTexture NormalTex
        {
            get
            {
                return new OverlayNormalTexture();
            }
            set
            {
            }
        }
        public OverlayFoamTexture FoamTex
        {
            get
            {
                return new OverlayFoamTexture();
            }
            set
            {
            }
        }
        public OverlayClipTexture ClipTex
        {
            get
            {
                return new OverlayClipTexture();
            }
            set
            {
            }
        }
        public Matrix4x4 LocalToWorld
        {
            get
            {
                return new Matrix4x4();
            }
        }
        public Bounds BoundingBox
        {
            get
            {
                return new Bounds();
            }
        }
        public void Reset(Vector3 pos, float rotation, Vector2 halfSize, float duration) {}
        public virtual void UpdateOverlay() {}
        public virtual void CalculateLocalToWorld() {}
        public virtual void CalculateBounds() {}
        public bool Contains(float x, float z)
        {
            return false;
        }
        public bool Contains(float x, float z, out float u, out float v)
        {
            u = 0;
            v = 0;
            return false;
        }
    }
}
