using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[HBS.SerializePart]
public class HBValueBox : Part
{
    public FloatWire floatWire;
    public List<float> floats;
    public List<FloatWire.FloatWireOutput> outputs;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
