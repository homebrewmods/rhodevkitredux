using System;
using UnityEngine;
using HBS;
using SLua;
namespace Vectrosity
{
    public enum LineType
    {
        Continuous = 0,
        Discrete = 1,
        Points = 2
    }
}
