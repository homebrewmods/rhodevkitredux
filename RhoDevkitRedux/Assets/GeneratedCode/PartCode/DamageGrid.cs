using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[HBS.SerializeComponentOnly]
public class DamageGrid : MonoBehaviour
{
    public int groupId
    {
        get
        {
            return 0;
        }
    }
    public int InstanceID;
    public Bounds bounds;
    public float initialHealth;
    public float health;
    public float armor;
    public float boxMass;
    public Rigidbody body;
    public float createTime;
    public float damageTimeout;
    public DamageManager.GridData gridData;
    public List<DamageGrid> entangledGrids;
    public void Init(float massKg, float volumeM3) {}
    public void DealDamage(DamageManager.DamageData data) {}
    public void SetHealth(float hp) {}
    public float ApplySpeedDamageFactor(DamageManager.DamageData data)
    {
        return 0;
    }
    public void SetBounds() {}
    public void SetBoundsNew() {}
}
