using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBTransmitter : Part
{
    public int channel;
    public float curWatt;
    public float value;
    public bool gotPower;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
