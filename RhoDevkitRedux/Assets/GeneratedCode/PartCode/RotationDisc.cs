using System;
using UnityEngine;
using HBS;
using SLua;
public class RotationDisc : MonoBehaviour
{
    public void Occlude() {}
    public void SeeTrough() {}
    public void ShowDisc() {}
    public void HideDisc() {}
    public void EnableCollision() {}
    public void DisableCollision() {}
}
