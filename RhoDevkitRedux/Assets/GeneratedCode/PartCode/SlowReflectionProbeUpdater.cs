using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[UnityEngine.RequireComponent(typeof(UnityEngine.ReflectionProbe))]
public class SlowReflectionProbeUpdater : MonoBehaviour
{
    public float timeInterval;
}
