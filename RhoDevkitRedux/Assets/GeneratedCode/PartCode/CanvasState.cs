using System;
using UnityEngine;
using HBS;
using SLua;
namespace Vectrosity
{
    public enum CanvasState
    {
        None = 0,
        OnCanvas = 1,
        OffCanvas = 2
    }
}
