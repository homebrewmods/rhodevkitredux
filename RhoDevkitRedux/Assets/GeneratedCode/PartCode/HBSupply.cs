using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[SLua.CustomLuaClass]
public class HBSupply : MonoBehaviour
{
    public string ID;
    public float supply;
}
