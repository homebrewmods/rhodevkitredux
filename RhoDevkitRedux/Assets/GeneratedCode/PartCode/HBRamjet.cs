using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBRamjet : Part
{
    [HBS.SerializePartVar]
    public float minPitch;
    [HBS.SerializePartVar]
    public float maxPitch;
    [HBS.SerializePartVar]
    public float minVolume;
    [HBS.SerializePartVar]
    public float maxVolume;
    [HBS.SerializePartVar]
    public ForceCurve forceCurve;
    [HBS.SerializePartVar]
    public float consumerRate;
    [HBS.SerializePartVar]
    public Transform thrustPivot;
    [HBS.SerializePartVar]
    public Gradient burnColor;
    [HBS.SerializePartVar]
    public AudioClip rocketLoop;
    [HBS.SerializePartVar]
    public GameObject curBurnEffect;
    public GameObject burnEffect;
    public override void StartPart() {}
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
