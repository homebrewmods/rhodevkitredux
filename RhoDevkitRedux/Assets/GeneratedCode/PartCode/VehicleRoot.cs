using System;
using UnityEngine;
using HBS;
using SLua;
using HBNetworking;
using System.Collections;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class VehicleRoot : MonoBehaviour
{
    public bool isNew;
    public void Init() {}
    public void DropImmediatly() {}
    [System.Diagnostics.DebuggerHidden]
    public IEnumerator Drop(float time)
    {
        yield return null;
    }
    public void PassOWnershipIfNeeded() {}
    public void ConfigForSeated() {}
    public void ConfigForUnSeated() {}
    public void RegisterCall(VehicleRootCalls call, Action del, Action<VehicleRoot> del2) {}
    public void UnregisterCall(VehicleRootCalls call, Action del, Action<VehicleRoot> del2) {}
}
