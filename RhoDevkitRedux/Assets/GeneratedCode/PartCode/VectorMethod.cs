using System;
using UnityEngine;
using HBS;
using SLua;
public enum VectorMethod
{
    Add = 0,
    Substract = 1,
    Multiply = 2,
    Divide = 3,
    Angle = 4,
    Cross = 5,
    Distance = 6,
    Dot = 7,
    Lerp = 8,
    LerpUnclamped = 9,
    Max = 10,
    Min = 11,
    MoveTowards = 12,
    Normalize = 13,
    Project = 14,
    ProjectOnPlane = 15,
    Reflect = 16,
    RotateTowards = 17,
    Scale = 18,
    Slerp = 19,
    SlerpUnclamped = 20,
    Back = 21,
    Down = 22,
    Forward = 23,
    Left = 24,
    Right = 25,
    Up = 26,
    One = 27,
    Zero = 28,
    SplitXYZ = 29,
    Construct = 30
}
