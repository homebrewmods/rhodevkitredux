using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBBattery : Part
{
    [HBS.SerializePartVar]
    public float wattHour;
    [HBS.SerializePartVar]
    public AnimationCurve voltCurve;
    public override void OnTakeDamage() {}
    public void SetupNetworkbase() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
