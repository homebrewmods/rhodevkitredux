using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBSelector : Part
{
    public FloatWire floatWire;
    public List<FloatWire.FloatWireOutput> outputs;
    public FloatWire.FloatWireInput input;
    public FloatWire.FloatWireInput input2;
    public float outputValue;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
