using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public class AnimatedTexture : MonoBehaviour
{
    public int totalTiles;
    public int tilesX;
    public string texName;
    public float FPS;
}
