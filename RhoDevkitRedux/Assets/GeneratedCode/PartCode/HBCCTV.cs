using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBCCTV : Part
{
    public int channel;
    public RenderTexture rt;
    public string randomID;
    public Camera cam;
    public bool Hide;
    public bool TurnedOn;
    public bool ShouldRender;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public void RenderCamera() {}
}
