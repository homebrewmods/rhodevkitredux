using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBDistancemeter : Part
{
    public float distance;
    public DistanceTypes type;
    public void OnDrawGizmos() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
