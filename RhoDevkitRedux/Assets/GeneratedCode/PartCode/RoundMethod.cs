using System;
using UnityEngine;
using HBS;
using SLua;
public enum RoundMethod
{
    Round = 0,
    Ceiling = 1,
    Floor = 2
}
