using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBPropellor : Part
{
    public bool debug;
    public MeshRenderer bladeEdgeEffect;
    public Transform bladePrefab;
    [HBS.SerializePartVar]
    public Transform nozzle;
    [HBS.SerializePartVar]
    public bool isSupersonicPropeller;
    [HBS.SerializePartVar]
    public int bladeCount;
    [HBS.SerializePartVar]
    public float bladePitch;
    [HBS.SerializePartVar]
    public float bladeChord;
    [HBS.SerializePartVar]
    public float bladeRadius;
    [HBS.SerializePartVar]
    public float nozzleWeight;
    [HBS.SerializePartVar]
    public float bladeScale;
    [HBS.SerializePartVar]
    public float nozzleScale;
    [HBS.SerializePartVar]
    public float bladeWeight;
    [UnityEngine.HideInInspector]
    public Vector3 forwardForceSidewaysForceTorque;
    [UnityEngine.HideInInspector]
    public float W;
    [UnityEngine.HideInInspector]
    public float currentT;
    public void Init() {}
    public void InitNew() {}
    public void CalcNow() {}
}
