using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public class WorldLocation : object
{
    public string name;
    public string data;
    public Color color;
    public double x;
    public double y;
    public double z;
}
