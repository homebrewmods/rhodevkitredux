using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
namespace Ceto
{
    public class AddWaveOverlayBase : MonoBehaviour
    {
        public IEnumerable<WaveOverlay> Overlays
        {
            get
            {
                yield return null;
            }
        }
        public WaveOverlay Overlay
        {
            get
            {
                return new WaveOverlay();
            }
        }
        public virtual void Translate(Vector3 amount) {}
    }
}
