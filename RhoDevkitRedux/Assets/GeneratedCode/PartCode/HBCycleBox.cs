using System;
using UnityEngine;
using HBS;
using SLua;
using System.IO;
[HBS.SerializePart]
public class HBCycleBox : Part
{
    public override void ReadFromPropertiesPassImmediate() {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
