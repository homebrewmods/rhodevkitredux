using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBNoteblock : Part
{
    public float maxVolume;
    public int midiInstrument;
    public int noteOffset;
    public AudioSource m_audio;
    public int prevInstrument;
    public bool CanRead;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
