using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[SLua.CustomLuaClass]
public class HBRaceTrack : MonoBehaviour
{
    public int checkpointCount
    {
        get
        {
            return 0;
        }
    }
    public HBTrigger start;
    public HBTrigger finish;
    public HBTrigger[] checkpoints;
    public Action<GameObject, GameObject, int> onCheckpointStart;
    public Action<GameObject, GameObject, int> onCheckpointFinish;
    public Action<GameObject, GameObject, int> onCheckpoint;
}
