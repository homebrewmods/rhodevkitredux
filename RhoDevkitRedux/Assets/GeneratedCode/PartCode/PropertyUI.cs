using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using Vectrosity;
using UnityEngine.UI;
public class PropertyUI : MonoBehaviour
{
    public List<Part> sideParts;
    public string propertyName;
    [UnityEngine.HideInInspector]
    public GameObject arrayButtons;
    public List<VectorLine> curLines;
    public void Initialize() {}
    public void Assign(Part part, Property property) {}
    public void AssignForKismet(Part part, Property property) {}
    public void Refresh() {}
    public void ClearLinkLines() {}
    public void CreateLinKLines() {}
    public void Dublicate() {}
    public void Delete() {}
    public void OnInputFieldChange(InputField input) {}
    public void OnSliderChange(Slider slider) {}
    public void OnBoolChange(Toggle tog) {}
    public void OnEnumChange(string selected) {}
    public void OnKeybindChange(string keybindData) {}
    public void OnUpInput() {}
    public void OnUpOutput() {}
    public void OnClickInput() {}
    public void OnDownInput() {}
    public void OnClickOutput() {}
    public void OnDownOutput() {}
    public void OnButtonClick() {}
    public void OnOpenAsset() {}
    public void OnKeyBind() {}
    public void OnLink() {}
    public void OnLinkedDot(Property dotProperty) {}
    public Property GetOneProperty()
    {
        return new Property();
    }
}
