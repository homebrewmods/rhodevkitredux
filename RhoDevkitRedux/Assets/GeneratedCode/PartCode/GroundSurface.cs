using System;
using UnityEngine;
using HBS;
using SLua;
[System.Serializable]
public class GroundSurface : object
{
    public GroundSurfaceType type;
    [UnityEngine.Header("Player contact")]
    public float playerGripMultyplier;
    public AudioClip[] footstepSounds;
    public HBAudioLoop footstepSlideLoop;
    public AudioClip footstepSlideSound;
    public GameObject footstepDebrisParticlePrefab;
    [UnityEngine.Header("Wheel contact")]
    public bool wheelLeaveSparks;
    public float skidThresholdMultiplier;
    public float wheelFrictionCoefficient;
    public float wheelShakeFactor;
    public HBAudioLoop wheelRollLoop;
    public AudioClip wheelRollSound;
    public HBAudioLoop wheelSkidLoop;
    public AudioClip wheelSkidSound;
    public Material wheelTireSkidMaterial;
    public Material wheelRimSkidMaterial;
    public GameObject wheelDebrisParticlePrefab;
    public GameObject wheelSparksPariclePrefab;
    [UnityEngine.Header("Phy contact")]
    public AudioClip PhyHitSound;
    public HBAudioLoop PhySlideLoop;
    public AudioClip phySlideSound;
    public ParticleSystem phyDebrisParticle;
    public ParticleSystem phySparkParticle;
    public float hitPitchRandomness;
    public float hitVolumeRandomness;
    public float slidePitchRandomness;
    public float slideVolumeRandomness;
    public bool scaleVolumeOnHit;
    [UnityEngine.Header("Bullet contact")]
    public string impactSoundType;
}
