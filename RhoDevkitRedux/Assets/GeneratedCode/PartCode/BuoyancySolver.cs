using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class BuoyancySolver : MonoBehaviour
{
    public Bounds bounds;
    public void Start() {}
    public void Init() {}
    public float GetVolume()
    {
        return 0;
    }
    public Vector3 GetLocalCenter()
    {
        return Vector3.zero;
    }
    public Vector3 GetCenter()
    {
        return Vector3.zero;
    }
    public float GetForce()
    {
        return 0;
    }
    public bool GetVoxelInWater(Vector3 worldPosition, out float factor)
    {
        factor = 0;
        return false;
    }
    public class BuoyancyVoxel : object
    {
        public Vector3 point;
        public Vector3 worldPoint;
        public float buoyancyForce;
        public Vector3 dragForce;
    }
}
