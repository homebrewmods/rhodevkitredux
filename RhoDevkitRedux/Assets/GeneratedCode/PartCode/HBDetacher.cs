using System;
using UnityEngine;
using HBS;
using SLua;
using System.IO;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBDetacher : Part
{
    [HBS.SerializePartVar]
    public GameObject jointTarget;
    public override void StartPart() {}
    public void SetupNetworkbase() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
