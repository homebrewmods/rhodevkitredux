using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBGunMount : Part
{
    [HBS.SerializePartVar]
    public Transform target1;
    [HBS.SerializePartVar]
    public bool smooth;
    [HBS.SerializePartVar]
    public float rotationSpeed;
    [HBS.SerializePartVar]
    public bool useDirectionalAngleLimits;
    [HBS.SerializePartVar]
    public float pitchLimit;
    [HBS.SerializePartVar]
    public float yawLimit;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
