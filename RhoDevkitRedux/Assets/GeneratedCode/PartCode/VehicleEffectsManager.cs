using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializeComponentOnly]
[SLua.CustomLuaClass]
public class VehicleEffectsManager : MonoBehaviour
{
    public BuoyancySolver[] buoyancySolvers;
    public HBWing[] wings;
    public EffectAudio wingSound;
    public string wingSoundID;
    public float wingSoundVolume;
    public float wingSoundTotalForce;
    public float wingSoundMaxForce;
    public float wingSoundMaxForceSmooth;
    public Vector3 wingSoundOffset;
    public HBWheel[] wheels;
    public HBRocketEngine[] rockets;
    public HBGasTurbineEngine[] turbines;
    public HBJetEngine[] jets;
    public HBRamjet[] ramJets;
    public HBSolidRocketMount[] solidRockets;
    public HBPropellor[] propellors;
    public HBHeliPropellor[] heliPropellors;
    public HBParticleLooper[] particles;
    public VehiclePiece piece;
    public Rigidbody body;
    public string speedEffectID;
    public float camDistance;
    public void Init() {}
}
