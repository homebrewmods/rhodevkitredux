using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    [System.Serializable]
    public class LineGeneratorType : object
    {
        public Material material;
        public float width;
        public float lengthMultiplier;
        public Vector3 eulerRotationOffset;
        public PrimitiveType primitiveType;
    }
}
