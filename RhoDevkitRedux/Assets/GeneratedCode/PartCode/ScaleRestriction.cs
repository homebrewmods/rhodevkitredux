using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public enum ScaleRestriction
{
    Limited = 0,
    LimitedUniform = 1,
    UnlimitedUniform = 2,
    None = 3,
    Fixed = 4
}
