using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class HullGenerator : MonoBehaviour
    {
        public HullGeneratorType[] hullTypes;
        public void Generate() {}
        public void Clear() {}
    }
}
