using System;
using UnityEngine;
using HBS;
using SLua;
public enum DistanceTypes
{
    Kilometre = 0,
    Metre = 1,
    Mile = 2,
    Yard = 3,
    Foot = 4,
    Inch = 5,
    NauticalMile = 6
}
