using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBGyrometre : Part
{
    public GyroSensorType type;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
