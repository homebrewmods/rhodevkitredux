using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class LineData : object
    {
        public int vertIndex1;
        public int vertIndex2;
        public string data;
    }
}
