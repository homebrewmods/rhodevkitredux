using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializeComponentOnly]
public class AudioSourceParented : MonoBehaviour
{
    public float pitch
    {
        get
        {
            return 0;
        }
        set
        {
        }
    }
    public AudioClip myClip
    {
        get
        {
            return new AudioClip();
        }
        set
        {
        }
    }
    public float volume;
    public void PlayOneShot(AudioClip clip, float volume, bool keepAlive, float range) {}
}
