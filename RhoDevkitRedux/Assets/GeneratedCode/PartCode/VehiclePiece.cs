using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializeComponentOnly]
public class VehiclePiece : MonoBehaviour
{
    public Bounds bounds
    {
        get
        {
            return new Bounds();
        }
    }
    public Rigidbody body
    {
        get
        {
            return new Rigidbody();
        }
    }
    public void RecalculateBounds() {}
    public void OnBecomeNonOwner() {}
}
