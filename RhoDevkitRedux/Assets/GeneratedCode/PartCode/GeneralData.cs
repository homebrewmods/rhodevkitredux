using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class GeneralData : UnityEngine.Networking.MessageBase
    {
        public string id;
        public byte[] data;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
