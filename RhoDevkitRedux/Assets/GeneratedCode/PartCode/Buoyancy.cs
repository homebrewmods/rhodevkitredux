using System;
using UnityEngine;
using HBS;
using SLua;
using Ceto;
public class Buoyancy : Part
{
    public GameObject particlePrefab;
    public AddFoamTrail foamTrail;
    public float emissionPerForce;
    public float radius;
    public float buoyancy;
    public float bounceforce;
    public override void StartPart() {}
}
