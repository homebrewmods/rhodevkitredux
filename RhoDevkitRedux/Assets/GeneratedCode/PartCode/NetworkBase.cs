using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBNetworking
{
    [HBS.SerializeComponentOnly]
    public class NetworkBase : MonoBehaviour
    {
        public int connID
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public string netID;
        public bool instanceOwner;
        public bool owner;
        public void ReRegister() {}
        public void UnRegister() {}
        public void NetworkStart() {}
        public void NetworkUpdate() {}
        public void NetworkConnect() {}
        public void NetworkConnecting() {}
        public void NetworkDisconnect() {}
        public void NetworkConnectFail() {}
        public void NetworkForceReconnect() {}
        public void NetworkOtherJoined(int connID) {}
        public void NetworkOtherLeft(int connID) {}
        public void NetworkOwnerSwitch() {}
        public void NetworkAskStateSync(int connID) {}
        public void NetworkStateSync(byte[] data) {}
        public void NetworkAddComponent(AddComponentData data) {}
        public void NetworkGeneral(GeneralData data) {}
        public void NetworkPosition(PositionData data) {}
        public void NetworkPing(PingData data) {}
        public void NetworkRemove() {}
        public void NetworkTransfered(string path) {}
        public void NetworkTransfering(TransferData tr) {}
        public void NetworkTransferProgress(TransferData tr, float p) {}
        public void NetworkCreated(int connID) {}
        public void NetworkAdminLogin(bool succes) {}
        public void NetworkApplyServerSettings() {}
        public void NetworkServerInitializeData(ServerData data) {}
        public void NetworkChatMessage(ChatMessage data) {}
        public void RegisterCall(NetworkCallType type, Action<ServerData> del) {}
        public void RegisterCall(NetworkCallType type, Action<string> del) {}
        public void RegisterCall(NetworkCallType type, Action<TransferData> del) {}
        public void RegisterCall(NetworkCallType type, Action<TransferData, float> del) {}
        public void RegisterCall(NetworkCallType type, Action<PingData> del) {}
        public void RegisterCall(NetworkCallType type, Action<PositionData> del) {}
        public void RegisterCall(NetworkCallType type, Action<GeneralData> del) {}
        public void RegisterCall(NetworkCallType type, Action<AddComponentData> del) {}
        public void RegisterCall(NetworkCallType type, Action<byte[]> del) {}
        public void RegisterCall(NetworkCallType type, Action<int> del) {}
        public void RegisterCall(NetworkCallType type, Action<bool> del) {}
        public void RegisterCall(NetworkCallType type, Action del) {}
        public void RegisterCall(NetworkCallType type, Action<ChatMessage> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<ServerData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<string> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<TransferData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<TransferData, float> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<PingData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<PositionData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<GeneralData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<AddComponentData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<byte[]> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<int> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<bool> del) {}
        public void UnRegisterCall(NetworkCallType type, Action del) {}
        public void UnRegisterCall(NetworkCallType type, Action<ChatMessage> del) {}
        public void RegisterCall(NetworkCallType type, Action<NetworkTimeSyncData> del) {}
        public void UnRegisterCall(NetworkCallType type, Action<NetworkTimeSyncData> del) {}
    }
}
