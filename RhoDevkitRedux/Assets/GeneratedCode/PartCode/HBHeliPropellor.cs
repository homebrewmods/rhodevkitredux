using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBHeliPropellor : Part
{
    public bool debug;
    [HBS.SerializePartVar]
    public bool isSupersonicPropeller;
    public Transform pivot;
    [HBS.SerializePartVar]
    public Transform nozzle;
    public MeshRenderer bladeEdgeEffect;
    public Transform bladePrefab;
    [HBS.SerializePartVar]
    public int bladeCount;
    [HBS.SerializePartVar]
    public float bladePitch;
    [HBS.SerializePartVar]
    public float bladeChord;
    [HBS.SerializePartVar]
    public float bladeRadius;
    [HBS.SerializePartVar]
    public float bladeScale;
    [HBS.SerializePartVar]
    public float nozzleScale;
    [HBS.SerializePartVar]
    public float nozzleWeight;
    [HBS.SerializePartVar]
    public float bladeWeight;
    [UnityEngine.HideInInspector]
    public Vector3 forwardForceSidewaysForceTorque;
    [UnityEngine.HideInInspector]
    public float W;
    [UnityEngine.HideInInspector]
    public float currentT;
    public Rigidbody body;
    public Vector3 localOffset;
    public float dot;
    public void Init() {}
    public void InitNew() {}
    public Vector3 GetOffset(Vector3 _offset)
    {
        return Vector3.zero;
    }
    public Vector3 GetForcePosition()
    {
        return Vector3.zero;
    }
    public void CalcNow() {}
}
