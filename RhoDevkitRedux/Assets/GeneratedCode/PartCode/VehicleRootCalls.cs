using System;
using UnityEngine;
using HBS;
using SLua;
public enum VehicleRootCalls
{
    EnterSeat = 0,
    LeaveSeat = 1,
    EnterSeatThis = 2,
    LeaveSeatThis = 3
}
