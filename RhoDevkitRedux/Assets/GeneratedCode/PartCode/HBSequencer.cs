using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using System.IO;
[HBS.SerializePart]
public class HBSequencer : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireOutput floatwireOutput;
    public FloatWire.FloatWireInput floatwireInput;
    public List<float> outputs;
    public float curTime;
    public float timeToNext;
    public bool alwaysRun;
    public bool smoothOutput;
    public bool resetOnInput;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
