using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class Adjustable : MonoBehaviour
    {
        public string adjustableTypeName;
        public string data;
        public Node[] nodes;
        public Action onChange;
        public void TriggerChange() {}
        public void Copy(Adjustable other) {}
        public int[] FindNodes(string search)
        {
            return new int[0];
        }
        public int[] FindNodes(Vector3 point, float tolerance, Space space)
        {
            return new int[0];
        }
        public int FindNode(string search)
        {
            return 0;
        }
        public int FindNode(Vector3 point, float tolerance, Space space)
        {
            return 0;
        }
        public int AddNode(Vector3 point, Quaternion rotation, string data, Space space)
        {
            return 0;
        }
        public void RemoveNode(int index) {}
        public int NodeCount()
        {
            return 0;
        }
        public void MoveNode(int index, Vector3 point, Space space) {}
        public void RotateNode(int index, Quaternion rotation, Space space) {}
        public void SetNodeData(int index, string data) {}
        public string GetNodeData(int index)
        {
            return "";
        }
        public Vector3 GetNodePosition(int index, Space space)
        {
            return Vector3.zero;
        }
        public Quaternion GetNodeRotation(int index, Space space)
        {
            return Quaternion.identity;
        }
        public int AddHandle(int nodeIndex, Vector3 point, string data, Space space)
        {
            return 0;
        }
        public void RemoveHandle(int nodeIndex, int index) {}
        public int HandleCount(int nodeIndex)
        {
            return 0;
        }
        public void MoveHandle(int nodeIndex, int index, Vector3 point, Space space) {}
        public Vector3 GetHandlePosition(int nodeIndex, int index, Space space)
        {
            return Vector3.zero;
        }
        public string GetHandleData(int nodeIndex, int index)
        {
            return "";
        }
    }
}
