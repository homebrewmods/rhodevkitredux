using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class TransferData : UnityEngine.Networking.MessageBase
    {
        public string url;
        public string filename;
        public string md5;
        public int connID;
        public int myConnID;
        public string id;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
