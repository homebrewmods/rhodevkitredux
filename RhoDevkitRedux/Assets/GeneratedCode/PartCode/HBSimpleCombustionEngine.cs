using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBSimpleCombustionEngine : Part
{
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public float electricEngineGeneratorWatt;
    [HBS.SerializePartVar]
    public float electricEngineWatt;
    [HBS.SerializePartVar]
    public float fuelConsumption;
    [HBS.SerializePartVar]
    public TorqueCurve engineTorque;
    [HBS.SerializePartVar]
    public HBAudioLoop engineLoop;
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public string startWWiseLoop;
    [HBS.SerializePartVar]
    public string stopWWiseLoop;
    [HBS.SerializePartVar]
    public float wwiseRPM;
    [HBS.SerializePartVar]
    public AudioClip engineThrottleLoop;
    [HBS.SerializePartVar]
    public AudioClip engineIdleLoop;
    [HBS.SerializePartVar]
    public AudioClip igniteEngine;
    [HBS.SerializePartVar]
    public AudioClip unigniteEngine;
    [HBS.SerializePartVar]
    public float flywheelInertia;
    [HBS.SerializePartVar]
    public float clutchMaxTorque;
    [HBS.SerializePartVar]
    public float revSmoothInternalAudioThrottleSpeedFactor;
    [HBS.SerializePartVar]
    public float revSmoothIneralRPMSpeedFactor;
    [HBS.SerializePartVar]
    public bool useWWise;
    [HBS.SerializePartVar]
    public bool useRev;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void OnTakeDamage() {}
    public void ConfigureAudio() {}
}
