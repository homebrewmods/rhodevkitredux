using System;
using UnityEngine;
using HBS;
using SLua;
using System.IO;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBSolarPanel : Part
{
    [HBS.SerializePartVar]
    public Bounds bounds;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
