using System;
using UnityEngine;
using HBS;
using SLua;
public class EffectAudio : object
{
    public string id;
    public GameObject target;
    public Vector3 offset;
    public Quaternion offsetRotation;
    public AudioClip clip;
    public AudioSource source;
    public bool loop;
    public void LateUpdate() {}
    public void Destroy() {}
}
