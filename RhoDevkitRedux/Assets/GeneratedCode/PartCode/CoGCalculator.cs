using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[SLua.CustomLuaClass]
[HBS.SerializeComponentOnly]
public class CoGCalculator : MonoBehaviour
{
    public float mass;
    public Vector3 cogOffset;
    public Vector3 inertiaTensor;
    public List<CoG> cogs;
    public Bounds bounds;
    public bool forBuildable;
    public void ForceCalc() {}
    public void ForceCalcNew() {}
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
    public void Calc() {}
}
