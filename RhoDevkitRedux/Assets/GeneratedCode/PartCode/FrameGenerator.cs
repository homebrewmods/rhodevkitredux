using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class FrameGenerator : MonoBehaviour
    {
        public LineGeneratorType[] lineTypes;
        public void Generate() {}
        public void Clear() {}
    }
}
