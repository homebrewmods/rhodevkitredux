using System;
using UnityEngine;
using HBS;
using SLua;
namespace Ceto
{
    public enum OVERLAY_MASK_MODE
    {
        WAVES = 0,
        OVERLAY = 1,
        WAVES_AND_OVERLAY = 2,
        WAVES_AND_OVERLAY_BLEND = 3
    }
}
