using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBEquationBox : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireInput[] inputs;
    public FloatWire.FloatWireOutput[] outputs;
    public FloatWire.FloatWireOutput output;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
