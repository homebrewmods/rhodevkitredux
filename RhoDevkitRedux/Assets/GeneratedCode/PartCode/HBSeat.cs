using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using System.IO;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBSeat : Part
{
    public Vector3 Velocity
    {
        get
        {
            return Vector3.zero;
        }
    }
    public GameObject character;
    [HBS.SerializePartVar]
    public GameObject playerGoal;
    [HBS.SerializePartVar]
    public GameObject playerCameraGoal;
    [HBS.SerializePartVar]
    public GameObject bodyGoal;
    [HBS.SerializePartVar]
    public GameObject lookAtGoal;
    [HBS.SerializePartVar]
    public GameObject leftHandGoal;
    [HBS.SerializePartVar]
    public GameObject leftHandBendGoal;
    [HBS.SerializePartVar]
    public GameObject rightHandGoal;
    [HBS.SerializePartVar]
    public GameObject rightHandBendGoal;
    [HBS.SerializePartVar]
    public GameObject leftFootGoal;
    [HBS.SerializePartVar]
    public GameObject leftFootBendGoal;
    [HBS.SerializePartVar]
    public GameObject rightFootGoal;
    [HBS.SerializePartVar]
    public GameObject rightFootBendGoal;
    [HBS.SerializePartVar]
    public GameObject steeringWheelHubGameObject;
    public int thirdPersonCameraMode;
    public bool inControle;
    public bool inOtherControle;
    public List<HBSeat.SeatKey> seatkeys;
    public int seatedConnID;
    public bool isOwnerSeat;
    [UnityEngine.HideInInspector]
    public float fieldOfViewMin;
    public float fieldOfViewMax;
    [UnityEngine.HideInInspector]
    public bool lookaround;
    public override void StartPart() {}
    public void OnDropVehicle() {}
    public override void OnTakeDamage() {}
    public void BailSeatNow() {}
    public void BailSeat() {}
    public void LeaveSeat() {}
    public void EnterSeat() {}
    public void DestroyCharacter() {}
    public void CreateCharacter(int colorIndex) {}
    public void FindIkGoalsByName() {}
    public void DestroyCharacterIKGoals() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void ReadFromPropertiesPass2New() {}
    public void FindSteetingWheelHub() {}
    public void LoadKeybindPresetNew(KeybindPreset preset) {}
    public void LoadKeybindPreset(KeybindPreset preset) {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    [System.Serializable]
    public class SeatKey : object
    {
        public FloatWire.FloatWireOutput output;
        public HBKeyBind keyBind;
        public float curValue;
        public void UpdateKey(bool smoothOutput) {}
        public void ForceKey(float keyValue) {}
    }
}
