using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    [System.Serializable]
    public struct Handle
    
    {
        public Vector3 position;
        public string data;
    }
}
