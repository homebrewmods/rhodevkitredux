using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBGenerator : Part
{
    [HBS.SerializePartVar]
    public Transform[] inputRotatables;
    [HBS.SerializePartVar]
    public Transform[] outputRotatables;
    [HBS.SerializePartVar]
    public Vector3 rotatablesAxis;
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
