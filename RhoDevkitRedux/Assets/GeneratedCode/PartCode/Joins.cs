using System;
using UnityEngine;
using HBS;
using SLua;
namespace Vectrosity
{
    public enum Joins
    {
        Fill = 0,
        Weld = 1,
        None = 2
    }
}
