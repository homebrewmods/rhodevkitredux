using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public enum GroundSurfaceType
{
    Grass = 0,
    Sand = 1,
    Mud = 2,
    Dirt = 3,
    Asphalt = 4,
    Rock = 5,
    Ice = 6,
    Snow = 7,
    Wood = 8,
    Stone = 9,
    Metal = 10,
    Default = 11
}
