using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public class RotateGameObject : MonoBehaviour
{
    public float rot_speed_x;
    public float rot_speed_y;
    public float rot_speed_z;
    public bool local;
}
