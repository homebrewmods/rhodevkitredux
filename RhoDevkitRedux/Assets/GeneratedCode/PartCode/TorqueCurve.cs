using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[System.Serializable]
public class TorqueCurve : object
{
    [HBS.SerializePartVar]
    [UnityEngine.Header("Easy Curve")]
    public float idleRPM;
    [HBS.SerializePartVar]
    public float torqueAtIdle;
    [HBS.SerializePartVar]
    public float heapStartRPM;
    [HBS.SerializePartVar]
    public float heapEndRPM;
    [HBS.SerializePartVar]
    public float torqueMax;
    [HBS.SerializePartVar]
    public float redlineRPM;
    [HBS.SerializePartVar]
    public float torqueAtRedline;
    [HBS.SerializePartVar]
    [UnityEngine.Header("Flat Curve")]
    public bool useFlat;
    [HBS.SerializePartVar]
    public float flatTorque;
    [UnityEngine.Header("Linear Curve")]
    [HBS.SerializePartVar]
    public bool useLinear;
    [HBS.SerializePartVar]
    public float linearMaxRPM;
    [HBS.SerializePartVar]
    public float linearTorque;
    [HBS.SerializePartVar]
    [UnityEngine.Header("Custom Curve")]
    public bool useCustom;
    [HBS.SerializePartVar]
    public AnimationCurve customTorqueCurve;
    [HBS.SerializePartVar]
    public float customIdleRPM;
    [HBS.SerializePartVar]
    public float customMaxRPM;
    [HBS.SerializePartVar]
    public float customTorque;
    public float GetTorque(float W)
    {
        return 0;
    }
    public float GetRedlineRPM()
    {
        return 0;
    }
    public float GetIdleRPM()
    {
        return 0;
    }
}
