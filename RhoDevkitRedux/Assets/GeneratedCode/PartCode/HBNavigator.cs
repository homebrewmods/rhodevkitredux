using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBNavigator : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireOutput distanceOutput;
    public FloatWire.FloatWireOutput headingOutput;
    public FloatWire.FloatWireInput inputX;
    public FloatWire.FloatWireInput inputZ;
    public float inputx_float;
    public float inputz_float;
    public bool useFloats;
    public Vector3 direction;
    public float distance;
    public float heading;
    public GyroSensorType type;
    public DistanceTypes distanceType;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
