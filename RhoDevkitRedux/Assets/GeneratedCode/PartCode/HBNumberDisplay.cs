using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.UI;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBNumberDisplay : Part
{
    public string additionalText;
    public Text text;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
