using System;
using UnityEngine;
using HBS;
using SLua;
public enum GyroSensorType
{
    Degrees = 0,
    Vector = 1,
    FullDegrees = 2,
    FullVector = 3,
    Percent = 4,
    FullPercent = 5
}
