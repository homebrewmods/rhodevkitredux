using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBTransistorBox : Part
{
    public float TriggerValue;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
