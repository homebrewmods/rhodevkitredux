using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public struct CurveHandle
    
    {
        public Vector3 direction;
        public int vertIndex1;
        public int vertIndex2;
    }
}
