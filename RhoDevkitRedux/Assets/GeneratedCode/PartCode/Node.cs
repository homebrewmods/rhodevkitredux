using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    [System.Serializable]
    public struct Node
    
    {
        public Vector3 position;
        public Quaternion rotation;
        public string data;
        public Handle[] handles;
    }
}
