using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class CoB : MonoBehaviour
{
    public float buoyancy
    {
        get
        {
            return 0;
        }
    }
    public Vector3 offset
    {
        get
        {
            return Vector3.zero;
        }
    }
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
}
