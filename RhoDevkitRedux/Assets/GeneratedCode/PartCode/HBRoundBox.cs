using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBRoundBox : Part
{
    public FloatWire floatWire;
    public RoundMethod method;
    public FloatWire.FloatWireInput input;
    public FloatWire.FloatWireOutput output;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
