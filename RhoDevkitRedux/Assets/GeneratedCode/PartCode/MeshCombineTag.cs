using System;
using UnityEngine;
using HBS;
using SLua;
using System.Xml.Linq;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class MeshCombineTag : MonoBehaviour
{
    public MeshCombine combineState;
    public void ToBakeXElement(XElement parent) {}
    public void FromBakeXElement(XElement parent) {}
}
