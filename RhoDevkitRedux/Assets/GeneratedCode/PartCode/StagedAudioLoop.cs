using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Audio;
[HBS.Serialize]
public class StagedAudioLoop : MonoBehaviour
{
    public AudioMixerGroup mixer;
    public float volume;
    public float pitch;
    public StagedAudioLoop.Stage[] stages;
    public bool useAudioSourceParented;
    public void Play(float fac) {}
    public void Stop() {}
    [HBS.Serialize]
    [System.Serializable]
    public class Stage : object
    {
        public AudioClip clip;
        public float bleed;
        public float fromVolume;
        public float toVolume;
        public float fromPitch;
        public float toPitch;
        public float toFactor;
        public void AssignAudio(AudioSource a, AudioMixerGroup mixer) {}
        public void AssignAudioParented(AudioSourceParented a) {}
        public void SetFromFactor(float fac) {}
        public void HandleAudio(float fac, float volume, float pitch) {}
        public void HandleAudioParented(float fac, float volume, float pitch) {}
    }
}
