using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBHotAirBalloon : Part
{
    [HBS.SerializePartVar]
    public float maxLiftForce;
    [HBS.SerializePartVar]
    public Transform[] releaseRotators;
    [HBS.SerializePartVar]
    public Vector3 releaseRotatorsAxis;
    [HBS.SerializePartVar]
    public float fuelConsumption;
    [HBS.SerializePartVar]
    public GameObject burnEffect;
    [HBS.SerializePartVar]
    public AudioClip burnLoop;
    [HBS.SerializePartVar]
    public Transform thrustPivot;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
