using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class QuadData : object
    {
        public int vertIndex1;
        public int vertIndex2;
        public int vertIndex3;
        public int vertIndex4;
        public string data;
    }
}
