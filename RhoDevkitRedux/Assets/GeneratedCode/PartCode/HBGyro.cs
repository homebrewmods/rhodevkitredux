using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBGyro : Part
{
    [HBS.SerializePartVar]
    public float rotSpeed;
    [HBS.SerializePartVar]
    public float angularDrag;
    [HBS.SerializePartVar]
    public float watt;
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public Vector3 rotatablesAxis;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
