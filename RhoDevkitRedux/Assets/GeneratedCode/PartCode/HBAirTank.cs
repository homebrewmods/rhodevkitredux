using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBAirTank : Part
{
    public float volume;
    public float flowRate;
    public float curLiters;
    public AirWire airWire;
    public AirWire.AirWireOutput airOutput;
    public AirWire.AirWireInput airInput;
    public FloatWire floatWire;
    public FloatWire.FloatWireOutput flowRateOutput;
    public FloatWire.FloatWireOutput litersLeftOutput;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
