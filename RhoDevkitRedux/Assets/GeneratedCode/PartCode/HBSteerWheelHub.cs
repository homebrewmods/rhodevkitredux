using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBSteerWheelHub : Part
{
    [HBS.SerializePartVar]
    public Transform steerWheelPivot;
    [HBS.SerializePartVar]
    public Vector3 steerWheelAxis;
    [HBS.SerializePartVar]
    public string preSteerWheelName;
    [HBS.SerializePartVar]
    public GameObject steerWheelGameObject;
    [HBS.SerializePartVar]
    public GameObject leftHandGoal;
    [HBS.SerializePartVar]
    public GameObject rightHandGoal;
    public GameObject steerWheel;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public void FindIkGoalsByName() {}
}
