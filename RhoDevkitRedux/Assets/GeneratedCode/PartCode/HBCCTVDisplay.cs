using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBCCTVDisplay : Part
{
    public int channel;
    public bool IsScreenVisible(RectTransform rectTransform)
    {
        return false;
    }
    public void SetupNetworkbase() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
