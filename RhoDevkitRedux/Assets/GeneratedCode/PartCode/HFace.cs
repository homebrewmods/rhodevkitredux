using System;
using UnityEngine;
using HBS;
using SLua;
using System.Xml.Linq;
public class HFace : object
{
    public Vector3 normal;
    public Vector3 up;
    public string ID;
    public HFace forwardWeldFace;
    public HFace backwardWeldFace;
    public HBuildNode node;
    public RotationDisc rotationDisc;
    public bool isWelded;
    public void Weld() {}
    public void UnWeld() {}
    public void ToWeldXElement(XElement parent) {}
    public void FromWeldXElement(XElement parent) {}
    public Vector3 GetNormal()
    {
        return Vector3.zero;
    }
    public Vector3 GetUp()
    {
        return Vector3.zero;
    }
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
    public void ApplyIDRefrencesAfterLoad(HBuildablePart[] assemblyParts) {}
}
