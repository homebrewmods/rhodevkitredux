using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using System.Xml.Linq;
public class AdjustablePart : MonoBehaviour
{
    [UnityEngine.HideInInspector]
    public List<MovableNode> movableNodes;
    public void FindMovableNodes() {}
    public virtual void OnAdjustment() {}
    public virtual void OnAdjustmentLoad() {}
    public virtual void ToXElement(XElement parent) {}
    public virtual void FromXElement(XElement parent) {}
    public virtual void ShowCurves() {}
    public virtual void HideCurves() {}
    public void MoveWeldChain(Vector3 delta, HBuildablePart bp, GameObject nodeParent) {}
    public void SetMeetlat(GameObject fromGameObject, GameObject toGameObject, int id) {}
    public GameObject CreateCurve(HBCurve curve, Color col)
    {
        return new GameObject();
    }
}
