using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public class TriData : object
    {
        public int vertIndex1;
        public int vertIndex2;
        public int vertIndex3;
        public string data;
    }
}
