using System;
using UnityEngine;
using HBS;
using SLua;
using System.Xml.Linq;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class CoG : MonoBehaviour
{
    [UnityEngine.SerializeField]
    private float _mass;
    public float mass
    {
        get
        {
            return _mass;
        }
        set
        {
            _mass = value;
        }
    }
    public Vector3 cogOffset;
    public Vector3 GetPosition()
    {
        return Vector3.zero;
    }
    public void ToXElement(XElement parent) {}
    public void FromXElement(XElement parent) {}
}
