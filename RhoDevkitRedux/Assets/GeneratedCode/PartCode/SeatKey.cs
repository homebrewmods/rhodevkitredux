using System;
using UnityEngine;
using HBS;
using SLua;
[System.Serializable]
public class SeatKey : object
{
    public FloatWire.FloatWireOutput output;
    public HBKeyBind keyBind;
    public float curValue;
    public void UpdateKey(bool smoothOutput) {}
    public void ForceKey(float keyValue) {}
}
