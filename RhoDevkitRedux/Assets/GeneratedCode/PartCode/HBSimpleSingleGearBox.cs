using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBSimpleSingleGearBox : Part
{
    [HBS.SerializePartVar]
    public Transform[] inputRotatables;
    [HBS.SerializePartVar]
    public Transform[] outputRotatables;
    [HBS.SerializePartVar]
    public Vector3 rotatablesAxis;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
