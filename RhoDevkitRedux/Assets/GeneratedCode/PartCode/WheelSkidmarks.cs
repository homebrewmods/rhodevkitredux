using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public class WheelSkidmarks : MonoBehaviour
{
    public WheelSkidmarks.TireSettings tireSettings;
    public bool isConnected;
    public void SetWheel(HBWheel ww) {}
    [HBS.Serialize]
    [System.Serializable]
    public class TireSettings : object
    {
        public float tireMarkLength;
        public float tireFadeTime;
        public float tireMarkGap;
        public float tireMarkHeight;
        public float slipThreshold;
        public float rimWidth;
        public float tireWidth;
        public bool isPopped;
    }
}
