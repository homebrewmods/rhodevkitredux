using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class HBTrigger : MonoBehaviour
{
    public Collider collider;
    public bool triggerOnPlayer;
    public bool triggerOnVehicle;
    public bool triggerOnSelf;
    public bool triggerOnOther;
    public Action<GameObject> onTrigger;
}
