using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBMathBox : Part
{
    public FloatWire floatWire;
    public MathMethod method;
    public FloatWire.FloatWireInput inputA;
    public FloatWire.FloatWireInput inputB;
    public FloatWire.FloatWireInput inputT;
    public FloatWire.FloatWireOutput output;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
