using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public class StringDataContainer : MonoBehaviour
    {
        public string stringData;
    }
}
