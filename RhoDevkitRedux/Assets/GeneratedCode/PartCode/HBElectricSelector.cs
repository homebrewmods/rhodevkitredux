using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[HBS.SerializePart]
public class HBElectricSelector : Part
{
    public FloatWire floatWire;
    public ElectricWire electricWire;
    public List<ElectricWire.ElectricWireOutput> outputs;
    public FloatWire.FloatWireInput select;
    public ElectricWire.ElectricWireInput electricInput;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
