using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBWeapon : Part
{
    public int groupId
    {
        get
        {
            return 0;
        }
    }
    [UnityEngine.Header("round type 0=AP 1=HE 2=HEAT 3=SABOT")]
    [HBS.SerializePartVar]
    public int roundType;
    [UnityEngine.Header("projectile properties")]
    [HBS.SerializePartVar]
    public float Cd;
    [HBS.SerializePartVar]
    public float caseDiameterMM;
    [HBS.SerializePartVar]
    public float totalLengthMM;
    [HBS.SerializePartVar]
    public float slugLengthMM;
    [HBS.SerializePartVar]
    public bool isTracer;
    [UnityEngine.Header("weapon properties")]
    [HBS.SerializePartVar]
    public float accuracyCircleMMat100M;
    [HBS.SerializePartVar]
    public int roundsPerMinute;
    [HBS.SerializePartVar]
    public bool enableAP;
    [HBS.SerializePartVar]
    public bool enableHE;
    [HBS.SerializePartVar]
    public bool enableHEAT;
    [HBS.SerializePartVar]
    public bool enableSABOT;
    [HBS.SerializePartVar]
    public float penetrationConstantAP;
    [HBS.SerializePartVar]
    public float penetrationConstantHE;
    [HBS.SerializePartVar]
    public float penetrationConstantHEAT;
    [HBS.SerializePartVar]
    public float penetrationConstantSABOT;
    [UnityEngine.Header("Rotatables")]
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public Vector3[] rotatableAxis;
    [HBS.SerializePartVar]
    public float[] rotateDegreesPerShot;
    [HBS.SerializePartVar]
    [UnityEngine.Header("Movables")]
    public Transform[] movables;
    [HBS.SerializePartVar]
    public Vector3[] movablesAxis;
    [HBS.SerializePartVar]
    public float[] movablesDistance;
    [UnityEngine.Header("spoolup weapons, 0 will ignore electricity")]
    [HBS.SerializePartVar]
    public int Watt;
    [UnityEngine.Header("audio for oneshot")]
    [HBS.SerializePartVar]
    public string firePlay;
    [UnityEngine.Header("audio for loop")]
    [HBS.SerializePartVar]
    public string fireStart;
    [HBS.SerializePartVar]
    public string fireStop;
    [UnityEngine.Header("shoot offset and direction ( look at the gizmo )]")]
    [HBS.SerializePartVar]
    public Vector3 shootOffset;
    [HBS.SerializePartVar]
    public Vector3 shootDirection;
    public FloatWire.FloatWireInput charges;
    public override void StartPart() {}
    public void Shoot(int connID) {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
