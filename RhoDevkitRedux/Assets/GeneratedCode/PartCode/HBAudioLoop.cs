using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[System.Serializable]
public class HBAudioLoop : object
{
    [HBS.SerializePartVar]
    public float minPitch;
    [HBS.SerializePartVar]
    public float maxPitch;
    [HBS.SerializePartVar]
    public float minVolume;
    [HBS.SerializePartVar]
    public float maxVolume;
    public float GetVolume(float W, float maxRPM, float engineVolumeMultiplier)
    {
        return 0;
    }
    public float GetPitch(float W, float maxRPM)
    {
        return 0;
    }
}
