using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBFuelTank : Part
{
    [HBS.SerializePartVar]
    public float combustionFactor;
    [HBS.SerializePartVar]
    public float burnRate;
    [HBS.SerializePartVar]
    public float weightPerLiter;
    [HBS.SerializePartVar]
    public Bounds fuelBounds;
    public float volume;
    public void SetupNetworkbase() {}
    public override void OnTakeDamage() {}
    public void RecalcFuel() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
