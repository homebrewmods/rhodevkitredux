using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
public class LOD : MonoBehaviour
{
    public Bounds bounds;
    public float range1;
    public float range2;
    public float range3;
    public Renderer[] renderers1;
    public Renderer[] renderers2;
    public Renderer[] renderers3;
    public void UpdateLOD() {}
    public void GenerateLOD() {}
    public void RecalculateBounds() {}
    public float GetRange(int index)
    {
        return 0;
    }
    public Renderer[] GetRenderers(int index)
    {
        return new Renderer[0];
    }
}
