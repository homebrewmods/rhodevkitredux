using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class Hull : MonoBehaviour
    {
        public BodyContainer bodyContainer
        {
            get
            {
                return new BodyContainer();
            }
        }
        [UnityEngine.HideInInspector]
        public Vector3[] verts;
        [UnityEngine.HideInInspector]
        public int[] quads;
        [UnityEngine.HideInInspector]
        public int[] tris;
        [UnityEngine.HideInInspector]
        public CurveHandle[] handles;
        [UnityEngine.HideInInspector]
        public QuadData[] quadsData;
        [UnityEngine.HideInInspector]
        public TriData[] trisData;
        public Action onChange;
        public void TriggerChange() {}
        public void Copy(Hull other) {}
        public int FindQuadDataMatch(string match)
        {
            return 0;
        }
        public int FindTriDataMatch(string match)
        {
            return 0;
        }
        public int FindQuadData(int vertIndex1, int vertIndex2, int vertIndex3, int vertIndex4)
        {
            return 0;
        }
        public int AddQuadData(int vertIndex1, int vertIndex2, int vertIndex3, int vertIndex4, string data)
        {
            return 0;
        }
        public void RemoveQuadData(int index) {}
        public void SetQuadData(int index, string data) {}
        public string GetQuadData(int index)
        {
            return "";
        }
        public int FindTriData(int vertIndex1, int vertIndex2, int vertIndex3)
        {
            return 0;
        }
        public int AddTriData(int vertIndex1, int vertIndex2, int vertIndex3, string data)
        {
            return 0;
        }
        public void RemoveTriData(int index) {}
        public void SetTriData(int index, string data) {}
        public string GetTriData(int index)
        {
            return "";
        }
        public int FindHandle(int vertIndex1, int vertIndex2)
        {
            return 0;
        }
        public int AddHandle(Vector3 direction, int vertIndex1, int vertIndex2, Space space)
        {
            return 0;
        }
        public void RemoveHandle(int index) {}
        public void MoveHandle(int index, Vector3 direction, Space space) {}
        public int FindVert(Vector3 point, int ignoreIndex, float tolerance, Space space)
        {
            return 0;
        }
        public int AddVert(Vector3 point, Space space)
        {
            return 0;
        }
        public void RemoveVert(int index) {}
        public Vector3 GetVert(int index, Space space)
        {
            return Vector3.zero;
        }
        public void RemoveUnconnectedVerts() {}
        public void MoveVert(int index, Vector3 point, Space space) {}
        public void AddQuad(int index1, int index2, int index3, int index4) {}
        public int[] FindQuads(int index)
        {
            return new int[0];
        }
        public void AddTri(int index1, int index2, int index3) {}
        public int[] FindTris(int index)
        {
            return new int[0];
        }
        public void ReconnectQuad(int indexOld, int indexNew) {}
        public void ReconnectTri(int indexOld, int indexNew) {}
        public void RemoveQuad(int index1, int index2, int index3, int index4) {}
        public void RemoveTri(int index1, int index2, int index3) {}
        public void RemoveQuads(int index) {}
        public void RemoveTris(int index) {}
        public void Optimize(float tolerance) {}
        public void Clear() {}
        public int[] GetLines()
        {
            return new int[0];
        }
        public bool RaycastVerts(Ray ray, float vertRadius, out int index1, out Vector3 point1, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            point1 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
        public bool RaycastLines(Ray ray, float lineRadius, out int index1, out int index2, out Vector3 point1, out Vector3 point2, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            index2 = 0;
            point1 = Vector3.zero;
            point2 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
        public bool RaycastQuads(Ray ray, out int index1, out int index2, out int index3, out int index4, out Vector3 point1, out Vector3 point2, out Vector3 point3, out Vector3 point4, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            index2 = 0;
            index3 = 0;
            index4 = 0;
            point1 = Vector3.zero;
            point2 = Vector3.zero;
            point3 = Vector3.zero;
            point4 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
        public bool RaycastTris(Ray ray, out int index1, out int index2, out int index3, out Vector3 point1, out Vector3 point2, out Vector3 point3, out float dist, out Vector3 point, out Vector3 normal)
        {
            index1 = 0;
            index2 = 0;
            index3 = 0;
            point1 = Vector3.zero;
            point2 = Vector3.zero;
            point3 = Vector3.zero;
            dist = 0;
            point = Vector3.zero;
            normal = Vector3.zero;
            return false;
        }
    }
}
