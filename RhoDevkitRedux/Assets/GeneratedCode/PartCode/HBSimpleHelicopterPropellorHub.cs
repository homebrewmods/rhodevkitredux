using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBSimpleHelicopterPropellorHub : Part
{
    [HBS.SerializePartVar]
    public Transform propellorPivot;
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public GameObject propellorGameObject;
    [HBS.SerializePartVar]
    public string prePropellorName;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
