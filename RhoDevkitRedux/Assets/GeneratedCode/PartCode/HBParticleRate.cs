using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBParticleRate : Part
{
    [HBS.SerializePartVar]
    public float watt;
    [HBS.SerializePartVar]
    public Transform particlePivot;
    [HBS.SerializePartVar]
    public GameObject particleGameObject;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
