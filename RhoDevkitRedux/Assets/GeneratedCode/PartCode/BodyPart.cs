using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using System.Xml.Linq;
public class BodyPart : MonoBehaviour
{
    public List<HBuildablePart> HbuildableParts;
    public void FromXElement(XElement parent) {}
    public void ToXElement(XElement parent) {}
    public void BakeXElement(XElement parent) {}
    public void FinalizeXElement(XElement parent) {}
    public void Weld() {}
    public void UnWeld() {}
}
