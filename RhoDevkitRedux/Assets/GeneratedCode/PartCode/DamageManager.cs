using System;
using UnityEngine;
using HBS;
using SLua;
public class DamageManager : object
{
    public struct BulletData
    
    {
        public Vector3d position;
        public Vector3 velocity;
        public Vector3 normal;
        public Vector4 param;
        public float travelDistance;
        public Vector4 param2;
        public float penetrationConstant;
        public float Cd;
        public Vector3 color;
        public Vector3d retPos;
        public int groupId;
    }
    public struct GridData
    
    {
        public Vector3 min;
        public Vector3 max;
        public Vector3 center;
        public Vector3 extents;
        public Matrix4x4 mat;
        public float initialHealth;
        public float health;
        public float armor;
        public int id;
        public int groupId;
    }
    public struct DamageData
    
    {
        public int owner_connID;
        public float totalDamage;
        public Vector3[] points;
        public Vector3[] forces;
        public int cc;
    }
}
