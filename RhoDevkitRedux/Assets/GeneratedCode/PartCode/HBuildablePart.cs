using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using System.Xml.Linq;
public class HBuildablePart : MonoBehaviour
{
    public BodyPart body;
    public List<HBuildNode> buildNodes;
    public List<Part> parts;
    [UnityEngine.HideInInspector]
    public bool isWelded;
    public XElement spawnRefrence;
    [UnityEngine.HideInInspector]
    public GameObject oneWayIndicator;
    public AdjustablePart[] GetAdjustableParts()
    {
        return new AdjustablePart[0];
    }
    public void ToXElement(XElement parent) {}
    public void FromXElement(XElement parent) {}
    public void ToBakeXElement(XElement parent) {}
    public void FromBakeXElement(XElement parent) {}
    public void GetForwardWeldChain(List<Transform> ret, Transform notMe) {}
    public void GetForwardWeldChainTillFirstSplitter(List<Transform> ret, Transform notMe) {}
    public void GetReverseWeldChain(List<Transform> ret) {}
    public void GetReverseTolerantWeldChain(List<Transform> ret, Transform notMe) {}
    public void GetReverseTolerantWeldChainTillFirstSplitter(List<Transform> ret, Transform notMe) {}
    public void Weld() {}
    public void UnWeld() {}
    public void SetGrayMaterial() {}
    public void SetTransparantMaterial() {}
    public void SetOriginalMaterial() {}
    public void CreateOneWayIndicator() {}
    public void DestroyOneWayIndicators() {}
}
