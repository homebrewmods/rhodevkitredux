using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.Serialize]
[SLua.CustomLuaClass]
public class ScaleRestrictor : MonoBehaviour
{
    public ScaleRestriction scaleRestriction;
    public Vector3 minScale;
    public Vector3 maxScale;
    public void Restrict() {}
}
