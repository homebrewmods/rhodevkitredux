using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public class BodyContainer : MonoBehaviour
    {
        public bool isFirstLevel
        {
            get
            {
                return false;
            }
        }
        public Frame frame
        {
            get
            {
                return new Frame();
            }
        }
        public Hull hull
        {
            get
            {
                return new Hull();
            }
        }
        public BodyContainer[] bodyContainers
        {
            get
            {
                return new BodyContainer[0];
            }
        }
        public PartContainer[] partContainers
        {
            get
            {
                return new PartContainer[0];
            }
        }
        public GameObject NewFixedNode()
        {
            return new GameObject();
        }
    }
}
