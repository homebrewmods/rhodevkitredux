using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBAirCompressor : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireInput activationInput;
    public AirWire airWire;
    public AirWire.AirWireOutput airOutput;
    public ElectricWire electricWire;
    public ElectricWire.ElectricWireInput electricInput;
    public float Watt;
    public float litersPerMinute;
    public float powerCoef;
    public float volume;
    public float curLiters;
    public float flowRate;
    public AudioSource comprAudio;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
