using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBSimpleTransmission : Part
{
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public Transform[] inputRotatables;
    [HBS.SerializePartVar]
    public Transform[] outputRotatables;
    [HBS.SerializePartVar]
    public Vector3 rotablesAxis;
    [HBS.SerializePartVar]
    public AudioClip gearSound;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void ReadFromPropertiesPass2New() {}
}
