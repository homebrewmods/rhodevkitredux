using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class PositionData : UnityEngine.Networking.MessageBase
    {
        public double x;
        public double y;
        public double z;
        public Quaternion rotation;
        public Vector3 scale;
        public Vector3 velocity;
        public Vector3 avelocity;
        public float timeStamp;
        public string id;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
