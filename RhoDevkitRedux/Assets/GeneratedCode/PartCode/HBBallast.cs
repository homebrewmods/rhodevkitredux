using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBBallast : Part
{
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void ReadFromPropertiesPass2New() {}
}
