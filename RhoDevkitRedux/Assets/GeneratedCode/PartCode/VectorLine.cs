using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
using UnityEngine.UI;
namespace Vectrosity
{
    [System.Serializable]
    public class VectorLine : object
    {
        [UnityEngine.SerializeField]
        private Vector3[] m_lineVertices;
        [UnityEngine.SerializeField]
        private Vector2[] m_lineUVs;
        [UnityEngine.SerializeField]
        private Color32[] m_lineColors;
        [UnityEngine.SerializeField]
        private List<int> m_lineTriangles;
        [UnityEngine.SerializeField]
        private int m_vertexCount;
        [UnityEngine.SerializeField]
        private GameObject m_go;
        [UnityEngine.SerializeField]
        private RectTransform m_rectTransform;
        [UnityEngine.SerializeField]
        private Color32 m_color;
        [UnityEngine.SerializeField]
        private CanvasState m_canvasState;
        [UnityEngine.SerializeField]
        private bool m_is2D;
        [UnityEngine.SerializeField]
        private List<Vector2> m_points2;
        [UnityEngine.SerializeField]
        private List<Vector3> m_points3;
        [UnityEngine.SerializeField]
        private int m_pointsCount;
        [UnityEngine.SerializeField]
        private Vector3[] m_screenPoints;
        [UnityEngine.SerializeField]
        private float[] m_lineWidths;
        [UnityEngine.SerializeField]
        private float m_lineWidth;
        [UnityEngine.SerializeField]
        private float m_maxWeldDistance;
        [UnityEngine.SerializeField]
        private float[] m_distances;
        [UnityEngine.SerializeField]
        private string m_name;
        [UnityEngine.SerializeField]
        private Material m_material;
        [UnityEngine.SerializeField]
        private Texture m_originalTexture;
        [UnityEngine.SerializeField]
        private Texture m_texture;
        [UnityEngine.SerializeField]
        private bool m_active;
        [UnityEngine.SerializeField]
        private LineType m_lineType;
        [UnityEngine.SerializeField]
        private float m_capLength;
        [UnityEngine.SerializeField]
        private bool m_smoothWidth;
        [UnityEngine.SerializeField]
        private bool m_smoothColor;
        [UnityEngine.SerializeField]
        private Joins m_joins;
        [UnityEngine.SerializeField]
        private bool m_isAutoDrawing;
        [UnityEngine.SerializeField]
        private int m_drawStart;
        [UnityEngine.SerializeField]
        private int m_drawEnd;
        [UnityEngine.SerializeField]
        private int m_endPointsUpdate;
        [UnityEngine.SerializeField]
        private bool m_useNormals;
        [UnityEngine.SerializeField]
        private bool m_useTangents;
        [UnityEngine.SerializeField]
        private bool m_normalsCalculated;
        [UnityEngine.SerializeField]
        private bool m_tangentsCalculated;
        [UnityEngine.SerializeField]
        private EndCap m_capType;
        [UnityEngine.SerializeField]
        private string m_endCap;
        [UnityEngine.SerializeField]
        private bool m_useCapColors;
        [UnityEngine.SerializeField]
        private Color32 m_frontColor;
        [UnityEngine.SerializeField]
        private Color32 m_backColor;
        [UnityEngine.SerializeField]
        private float m_lineUVBottom;
        [UnityEngine.SerializeField]
        private float m_lineUVTop;
        [UnityEngine.SerializeField]
        private float m_frontCapUVBottom;
        [UnityEngine.SerializeField]
        private float m_frontCapUVTop;
        [UnityEngine.SerializeField]
        private float m_backCapUVBottom;
        [UnityEngine.SerializeField]
        private float m_backCapUVTop;
        [UnityEngine.SerializeField]
        private bool m_continuousTexture;
        [UnityEngine.SerializeField]
        private Transform m_drawTransform;
        [UnityEngine.SerializeField]
        private bool m_viewportDraw;
        [UnityEngine.SerializeField]
        private float m_textureScale;
        [UnityEngine.SerializeField]
        private bool m_useTextureScale;
        [UnityEngine.SerializeField]
        private float m_textureOffset;
        [UnityEngine.SerializeField]
        private bool m_useMatrix;
        [UnityEngine.SerializeField]
        private Matrix4x4 m_matrix;
        [UnityEngine.SerializeField]
        private bool m_collider;
        [UnityEngine.SerializeField]
        private bool m_trigger;
        [UnityEngine.SerializeField]
        private PhysicsMaterial2D m_physicsMaterial;
        [UnityEngine.SerializeField]
        private bool m_alignOddWidthToPixels;
        public Vector3[] lineVertices
        {
            get
            {
                return new Vector3[0];
            }
        }
        public Vector2[] lineUVs
        {
            get
            {
                return new Vector2[0];
            }
        }
        public Color32[] lineColors
        {
            get
            {
                return new Color32[0];
            }
        }
        public List<int> lineTriangles
        {
            get
            {
                return new List<int>();
            }
        }
        public RectTransform rectTransform
        {
            get
            {
                return new RectTransform();
            }
        }
        public Color32 color
        {
            get
            {
                return new Color32();
            }
            set
            {
            }
        }
        public bool is2D
        {
            get
            {
                return false;
            }
        }
        public List<Vector2> points2
        {
            get
            {
                return new List<Vector2>();
            }
        }
        public List<Vector3> points3
        {
            get
            {
                return new List<Vector3>();
            }
        }
        public float lineWidth
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public float maxWeldDistance
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public string name
        {
            get
            {
                return "";
            }
            set
            {
            }
        }
        public Material material
        {
            get
            {
                return new Material("autoGenerated");
            }
            set
            {
            }
        }
        public Texture texture
        {
            get
            {
                return new Texture();
            }
            set
            {
            }
        }
        public int layer
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public bool active
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public LineType lineType
        {
            get
            {
                return LineType.Continuous;
            }
            set
            {
            }
        }
        public float capLength
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public bool smoothWidth
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public bool smoothColor
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public Joins joins
        {
            get
            {
                return Joins.Fill;
            }
            set
            {
            }
        }
        public bool isAutoDrawing
        {
            get
            {
                return false;
            }
        }
        public int drawStart
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public int drawEnd
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public int endPointsUpdate
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public string endCap
        {
            get
            {
                return "";
            }
            set
            {
            }
        }
        public bool continuousTexture
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public Transform drawTransform
        {
            get
            {
                return null;
            }
            set
            {
            }
        }
        public bool useViewportCoords
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        [UnityEngine.SerializeField]
        public float textureScale
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public float textureOffset
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public Matrix4x4 matrix
        {
            get
            {
                return new Matrix4x4();
            }
            set
            {
            }
        }
        public int drawDepth
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }
        public bool collider
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public bool trigger
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public PhysicsMaterial2D physicsMaterial
        {
            get
            {
                return new PhysicsMaterial2D();
            }
            set
            {
            }
        }
        public bool alignOddWidthToPixels
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public void MakeCurve(Vector2[] curvePoints) {}
        public void MakeCurve(Vector2[] curvePoints, int segments) {}
        public void MakeCurve(Vector2[] curvePoints, int segments, int index) {}
        public void MakeCurve(Vector3[] curvePoints) {}
        public void MakeCurve(Vector3[] curvePoints, int segments) {}
        public void MakeCurve(Vector3[] curvePoints, int segments, int index) {}
        public void MakeCurve(Vector3 anchor1, Vector3 control1, Vector3 anchor2, Vector3 control2) {}
        public void MakeCurve(Vector3 anchor1, Vector3 control1, Vector3 anchor2, Vector3 control2, int segments) {}
        public void MakeCurve(Vector3 anchor1, Vector3 control1, Vector3 anchor2, Vector3 control2, int segments, int index) {}
        public void MakeSpline(Vector2[] splinePoints) {}
        public void MakeSpline(Vector2[] splinePoints, bool loop) {}
        public void MakeSpline(Vector2[] splinePoints, int segments) {}
        public void MakeSpline(Vector2[] splinePoints, int segments, bool loop) {}
        public void MakeSpline(Vector2[] splinePoints, int segments, int index) {}
        public void MakeSpline(Vector2[] splinePoints, int segments, int index, bool loop) {}
        public void MakeSpline(Vector3[] splinePoints) {}
        public void MakeSpline(Vector3[] splinePoints, bool loop) {}
        public void MakeSpline(Vector3[] splinePoints, int segments) {}
        public void MakeSpline(Vector3[] splinePoints, int segments, bool loop) {}
        public void MakeSpline(Vector3[] splinePoints, int segments, int index) {}
        public void MakeSpline(Vector3[] splinePoints, int segments, int index, bool loop) {}
        public void MakeText(string text, Vector3 startPos, float size) {}
        public void MakeText(string text, Vector3 startPos, float size, bool uppercaseOnly) {}
        public void MakeText(string text, Vector3 startPos, float size, float charSpacing, float lineSpacing) {}
        public void MakeText(string text, Vector3 startPos, float size, float charSpacing, float lineSpacing, bool uppercaseOnly) {}
        public void MakeWireframe(Mesh mesh) {}
        public void MakeCube(Vector3 position, float xSize, float ySize, float zSize) {}
        public void MakeCube(Vector3 position, float xSize, float ySize, float zSize, int index) {}
        public void SetWidths(List<float> lineWidths) {}
        public void SetWidths(List<int> lineWidths) {}
        public float GetWidth(int index)
        {
            return 0;
        }
        public void Draw() {}
        public void Draw3D() {}
        public void Draw3DAuto() {}
        public void Draw3DAuto(float time) {}
        public void StopDrawing3DAuto() {}
        public void SetDistances() {}
        public float GetLength()
        {
            return 0;
        }
        public Vector2 GetPoint01(float distance)
        {
            return Vector2.zero;
        }
        public Vector2 GetPoint01(float distance, out int index)
        {
            index = 0;
            return Vector2.zero;
        }
        public Vector2 GetPoint(float distance)
        {
            return Vector2.zero;
        }
        public Vector2 GetPoint(float distance, out int index)
        {
            index = 0;
            return Vector2.zero;
        }
        public Vector3 GetPoint3D01(float distance)
        {
            return Vector3.zero;
        }
        public Vector3 GetPoint3D01(float distance, out int index)
        {
            index = 0;
            return Vector3.zero;
        }
        public Vector3 GetPoint3D(float distance)
        {
            return Vector3.zero;
        }
        public Vector3 GetPoint3D(float distance, out int index)
        {
            index = 0;
            return Vector3.zero;
        }
        public bool Selected(Vector2 p)
        {
            return false;
        }
        public bool Selected(Vector2 p, out int index)
        {
            index = 0;
            return false;
        }
        public bool Selected(Vector2 p, int extraDistance, out int index)
        {
            index = 0;
            return false;
        }
        public bool Selected(Vector2 p, int extraDistance, int extraLength, out int index)
        {
            index = 0;
            return false;
        }
        public bool Selected(Vector2 p, Camera cam)
        {
            return false;
        }
        public bool Selected(Vector2 p, out int index, Camera cam)
        {
            index = 0;
            return false;
        }
        public bool Selected(Vector2 p, int extraDistance, out int index, Camera cam)
        {
            index = 0;
            return false;
        }
        public bool Selected(Vector2 p, int extraDistance, int extraLength, out int index, Camera cam)
        {
            index = 0;
            return false;
        }
        public void MakeRect(Rect rect) {}
        public void MakeRect(Rect rect, int index) {}
        public void MakeRect(Vector3 bottomLeft, Vector3 topRight) {}
        public void MakeRect(Vector3 bottomLeft, Vector3 topRight, int index) {}
        public void MakeRoundedRect(Rect rect, float cornerRadius, int cornerSegments) {}
        public void MakeRoundedRect(Rect rect, float cornerRadius, int cornerSegments, int index) {}
        public void MakeRoundedRect(Vector3 bottomLeft, Vector3 topRight, float cornerRadius, int cornerSegments) {}
        public void MakeRoundedRect(Vector3 bottomLeft, Vector3 topRight, float cornerRadius, int cornerSegments, int index) {}
        public void MakeCircle(Vector3 origin, float radius) {}
        public void MakeCircle(Vector3 origin, float radius, int segments) {}
        public void MakeCircle(Vector3 origin, float radius, int segments, float pointRotation) {}
        public void MakeCircle(Vector3 origin, float radius, int segments, int index) {}
        public void MakeCircle(Vector3 origin, float radius, int segments, float pointRotation, int index) {}
        public void MakeCircle(Vector3 origin, Vector3 upVector, float radius) {}
        public void MakeCircle(Vector3 origin, Vector3 upVector, float radius, int segments) {}
        public void MakeCircle(Vector3 origin, Vector3 upVector, float radius, int segments, float pointRotation) {}
        public void MakeCircle(Vector3 origin, Vector3 upVector, float radius, int segments, int index) {}
        public void MakeCircle(Vector3 origin, Vector3 upVector, float radius, int segments, float pointRotation, int index) {}
        public void MakeEllipse(Vector3 origin, float xRadius, float yRadius) {}
        public void MakeEllipse(Vector3 origin, float xRadius, float yRadius, int segments) {}
        public void MakeEllipse(Vector3 origin, float xRadius, float yRadius, int segments, int index) {}
        public void MakeEllipse(Vector3 origin, float xRadius, float yRadius, int segments, float pointRotation) {}
        public void MakeEllipse(Vector3 origin, Vector3 upVector, float xRadius, float yRadius) {}
        public void MakeEllipse(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, int segments) {}
        public void MakeEllipse(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, int segments, int index) {}
        public void MakeEllipse(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, int segments, float pointRotation) {}
        public void MakeEllipse(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, int segments, float pointRotation, int index) {}
        public void MakeArc(Vector3 origin, float xRadius, float yRadius, float startDegrees, float endDegrees) {}
        public void MakeArc(Vector3 origin, float xRadius, float yRadius, float startDegrees, float endDegrees, int segments) {}
        public void MakeArc(Vector3 origin, float xRadius, float yRadius, float startDegrees, float endDegrees, int segments, int index) {}
        public void MakeArc(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, float startDegrees, float endDegrees) {}
        public void MakeArc(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, float startDegrees, float endDegrees, int segments) {}
        public void MakeArc(Vector3 origin, Vector3 upVector, float xRadius, float yRadius, float startDegrees, float endDegrees, int segments, int index) {}
        public void Resize(int newCount) {}
        public void AddNormals() {}
        public void AddTangents() {}
        public void SetCanvas(GameObject canvasObject) {}
        public void SetCanvas(Canvas canvas) {}
        public void SetMask(GameObject maskObject) {}
        public void SetMask(Mask mask) {}
        public int GetSegmentNumber()
        {
            return 0;
        }
        public void SetEndCapColor(Color32 color) {}
        public void SetEndCapColor(Color32 frontColor, Color32 backColor) {}
        public void SetColor(Color32 color) {}
        public void SetColor(Color32 color, int index) {}
        public void SetColor(Color32 color, int startIndex, int endIndex) {}
        public void SetColors(List<Color32> lineColors) {}
        public Color32 GetColor(int index)
        {
            return new Color32();
        }
        public void SetWidth(float width) {}
        public void SetWidth(float width, int index) {}
        public void SetWidth(float width, int startIndex, int endIndex) {}
    }
}
