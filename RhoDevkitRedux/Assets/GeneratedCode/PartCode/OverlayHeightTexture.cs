using System;
using UnityEngine;
using HBS;
using SLua;
namespace Ceto
{
    [System.Serializable]
    public class OverlayHeightTexture : object
    {
        public bool IsDrawable
        {
            get
            {
                return false;
            }
        }
        public Texture tex;
        public Vector2 scaleUV;
        public Vector2 offsetUV;
        [UnityEngine.Range(-20f, 20f)]
        public float alpha;
        public Texture mask;
        public OVERLAY_MASK_MODE maskMode;
        [UnityEngine.Range(0f, 1f)]
        public float maskAlpha;
        public bool ignoreQuerys;
    }
}
