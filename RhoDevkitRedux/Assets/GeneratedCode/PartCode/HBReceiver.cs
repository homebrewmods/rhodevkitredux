using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBReceiver : Part
{
    public override void StartPart() {}
    [System.Diagnostics.DebuggerHidden]
    public IEnumerator SlowUpdate()
    {
        yield return null;
    }
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
