using System;
using UnityEngine;
using HBS;
using SLua;
using System.IO;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBFloatToggleSwitch : Part
{
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadStateSyncData(BinaryReader reader) {}
    public override void WriteStateSyncData(BinaryWriter writer) {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
