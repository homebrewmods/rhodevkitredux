using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [HBS.Serialize]
    [SLua.CustomLuaClass]
    public class GGizmoTransform : MonoBehaviour
    {
        public float radius;
        public Color color;
    }
}
