using System;
using UnityEngine;
using HBS;
using SLua;
using System.Collections.Generic;
[UnityEngine.RequireComponent(typeof(RefrenceID))]
public class MovableNode : MonoBehaviour
{
    public MovableNodeType type;
    public List<RefrenceID> entangledObjects;
    public LineRenderer indicationLine;
    public RefrenceID lineTarget;
    public AdjustablePart part;
    public Color preCol;
    public void Highlight() {}
    public void HighlightNest() {}
    public void Unhighlight() {}
    public void UnhighlightNest() {}
    public void ShowNode() {}
    public void HideNode() {}
    public bool OnPositionChange(Vector3 worldPos)
    {
        return false;
    }
    public void AdjustDelta(Vector3 delta) {}
    public void SetLineTarget(RefrenceID lt) {}
    public void UpdateLine() {}
    public Vector3 SnapToGrid(Vector3 worldPos)
    {
        return Vector3.zero;
    }
}
