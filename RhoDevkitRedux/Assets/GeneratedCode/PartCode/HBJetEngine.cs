using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBJetEngine : Part
{
    [HBS.SerializePartVar]
    public float minPitch;
    [HBS.SerializePartVar]
    public float maxPitch;
    [HBS.SerializePartVar]
    public float minVolume;
    [HBS.SerializePartVar]
    public float maxVolume;
    [HBS.SerializePartVar]
    public float afterburnerVolume;
    [HBS.SerializePartVar]
    public float maxThrust;
    [HBS.SerializePartVar]
    public float maxSpeed;
    [HBS.SerializePartVar]
    public float electricEngineGeneratorWatt;
    [HBS.SerializePartVar]
    public float electricEngineWatt;
    [HBS.SerializePartVar]
    public float consumerRate;
    [HBS.SerializePartVar]
    public float afterburnerExtraFuelConsumerRate;
    [HBS.SerializePartVar]
    public Transform thrustPivot;
    [HBS.SerializePartVar]
    public bool useAfterburner;
    [HBS.SerializePartVar]
    public bool useVectorTrust;
    [HBS.SerializePartVar]
    public Vector2 vectorTrustMinMaxPitch;
    [HBS.SerializePartVar]
    public Vector2 vectorTrustMinMaxYaw;
    [HBS.SerializePartVar]
    public Transform[] vectorTrustRotatebles;
    [HBS.SerializePartVar]
    public float throttleUpTime;
    [HBS.SerializePartVar]
    public float throttleDownTime;
    [HBS.SerializePartVar]
    public float throttleUpTimeElectric;
    [HBS.SerializePartVar]
    public Gradient burnColor;
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public float[] rotatableRatios;
    [HBS.SerializePartVar]
    public Vector3[] rotatablesAxis;
    [HBS.SerializePartVar]
    public AudioClip jetEngineLoop;
    [HBS.SerializePartVar]
    public AudioClip afterburnerLoop;
    [HBS.SerializePartVar]
    public GameObject curBurnEffect;
    public GameObject burnEffect;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
