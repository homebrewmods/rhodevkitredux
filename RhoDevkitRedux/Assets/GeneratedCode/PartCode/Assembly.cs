using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBBuilder
{
    [SLua.CustomLuaClass]
    [HBS.Serialize]
    public class Assembly : MonoBehaviour
    {
        public BodyContainer bodyContainer
        {
            get
            {
                return new BodyContainer();
            }
        }
        public BodyContainer[] bodyContainers
        {
            get
            {
                return new BodyContainer[0];
            }
        }
        public bool changed;
        public GameObject NewBodyContainer()
        {
            return new GameObject();
        }
        public void New() {}
        public void Save() {}
        public void Open() {}
        public void OpenPath(string openPath) {}
        public void Close() {}
        public void Clear() {}
        public void Serialize(string path) {}
        public void UnSerialize(string path) {}
        public void InitParts() {}
    }
}
