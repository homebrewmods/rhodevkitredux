using System;
using UnityEngine;
using HBS;
using SLua;
namespace Ceto
{
    public class AddFoamTrail : Ceto.AddWaveOverlayBase
    {
        public Texture foamTexture;
        public bool textureFoam;
        public AddFoamTrail.ROTATION rotation;
        public AnimationCurve timeLine;
        public float duration;
        public float size;
        public float spacing;
        public float expansion;
        public float momentum;
        public float spin;
        public bool mustBeBelowWater;
        [UnityEngine.Range(0f, 2f)]
        public float alpha;
        [UnityEngine.Range(0f, 1f)]
        public float jitter;
        public override void Translate(Vector3 amount) {}
        public enum ROTATION
        {
            NONE = 0,
            RANDOM = 1,
            RELATIVE = 2
        }
    }
}
