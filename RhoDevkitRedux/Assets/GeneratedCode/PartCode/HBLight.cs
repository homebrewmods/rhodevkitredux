using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBLight : Part
{
    [HBS.SerializePartVar]
    public Light myLight;
    [HBS.SerializePartVar]
    public float watt;
    [HBS.SerializePartVar]
    public string hexColor;
    [HBS.SerializePartVar]
    public float _r;
    [HBS.SerializePartVar]
    public float _g;
    [HBS.SerializePartVar]
    public float _b;
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
