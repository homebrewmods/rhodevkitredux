using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBNetworking
{
    [HBS.SerializeComponentOnly]
    public class Position : MonoBehaviour
    {
        public Vector3 curPosition;
        public Quaternion curRotation;
        public Vector3 curVelcoty;
        public Vector3 curAngularVelocity;
        public bool update;
    }
}
