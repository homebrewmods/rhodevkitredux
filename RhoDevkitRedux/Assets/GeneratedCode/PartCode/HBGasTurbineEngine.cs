using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBGasTurbineEngine : Part
{
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public float electricEngineWatt;
    [HBS.SerializePartVar]
    public float fuelConsumption;
    [HBS.SerializePartVar]
    public TorqueCurve motorTorque;
    [HBS.SerializePartVar]
    public float engineGenerateWatt;
    [HBS.SerializePartVar]
    public TorqueCurve engineTorque;
    [HBS.SerializePartVar]
    public TorqueCurve outputTorque;
    [HBS.SerializePartVar]
    public HBAudioLoop motorLoop;
    [HBS.SerializePartVar]
    public HBAudioLoop engineLoop;
    [HBS.SerializePartVar]
    public float internalGear;
    [HBS.SerializePartVar]
    public Light lightEffect;
    [HBS.SerializePartVar]
    public Transform[] rotatables;
    [HBS.SerializePartVar]
    public AudioClip engineThrottleLoop;
    [HBS.SerializePartVar]
    public AudioClip engineIdleLoop;
    public override void StartPart() {}
    public override void OnTakeDamage() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
