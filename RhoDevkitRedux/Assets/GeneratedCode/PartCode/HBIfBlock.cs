using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBIfBlock : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireInput inputa;
    public FloatWire.FloatWireInput inputb;
    public FloatWire.FloatWireInput ABiggerThenB;
    public FloatWire.FloatWireInput BBiggerThenA;
    public FloatWire.FloatWireInput AEqualB;
    public FloatWire.FloatWireOutput outputWire;
    public override void StartPart() {}
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
