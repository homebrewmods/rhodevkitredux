using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
public class HBGPS : Part
{
    public FloatWire floatWire;
    public FloatWire.FloatWireOutput xCoord;
    public FloatWire.FloatWireOutput yCoord;
    public FloatWire.FloatWireOutput zCoord;
    public VectorWire vectorWire;
    public VectorWire.VectorWireOutput vectorOutput;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
