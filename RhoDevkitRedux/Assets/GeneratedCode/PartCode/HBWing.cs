using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class HBWing : MonoBehaviour
{
    [UnityEngine.SerializeField]
    private Bounds _wingBounds;
    [UnityEngine.SerializeField]
    private Vector3 _wingSize;
    [UnityEngine.SerializeField]
    private Vector3 _wingLocalArea;
    [UnityEngine.SerializeField]
    private Vector3 _wingCenterOffset;
    public Bounds wingBounds
    {
        get
        {
            return _wingBounds;
        }
        set
        {
            _wingBounds = value;
        }
    }
    public Vector3 wingSize
    {
        get
        {
            return Vector3.zero;
        }
    }
    public Vector3 wingLocalArea
    {
        get
        {
            return Vector3.zero;
        }
    }
    public Vector3 wingCenterOffset
    {
        get
        {
            return Vector3.zero;
        }
    }
    public bool sphereShape;
    public bool capsuleShape;
    public float perlinScale;
    public float airDensity;
    public float waterDensity;
    public bool useOverrideFudgeFactors;
    public Vector3 airFudgeFactorOverride;
    public Vector3 waterFudgeFactorOverride;
    public void OnEnable() {}
    public Vector3 GetDragPoint()
    {
        return Vector3.zero;
    }
    public float GetLiftSurfaceArea()
    {
        return 0;
    }
    public float GetDrag(Vector3 airflowVelocity)
    {
        return 0;
    }
}
