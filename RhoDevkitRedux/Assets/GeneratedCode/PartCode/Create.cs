using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBNetworking
{
    [HBS.SerializeComponentOnly]
    public class Create : MonoBehaviour
    {
        public bool createOnAwake;
        public string prefabPath;
        public CreateData create;
    }
}
