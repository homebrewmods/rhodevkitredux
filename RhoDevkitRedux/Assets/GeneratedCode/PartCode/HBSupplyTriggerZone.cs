using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.Serialize]
public class HBSupplyTriggerZone : MonoBehaviour
{
    public string[] IDList;
    public Action<HBSupply> onCollect;
    public void OnTriggerEnter(Collider other) {}
}
