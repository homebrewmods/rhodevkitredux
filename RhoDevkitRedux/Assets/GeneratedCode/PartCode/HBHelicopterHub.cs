using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBHelicopterHub : Part
{
    [HBS.SerializePartVar]
    public Transform propellorPivot;
    [HBS.SerializePartVar]
    public Transform forcePivot;
    [HBS.SerializePartVar]
    public float I;
    [HBS.SerializePartVar]
    public GameObject propellorGameObject;
    [HBS.SerializePartVar]
    public string prePropellorName;
    public HBHeliPropellor propellor;
    public float counter_t_clamp;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
