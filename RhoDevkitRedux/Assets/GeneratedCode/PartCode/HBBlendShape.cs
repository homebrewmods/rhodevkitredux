using System;
using UnityEngine;
using HBS;
using SLua;
[HBS.SerializePart]
[SLua.CustomLuaClass]
public class HBBlendShape : Part
{
    [HBS.SerializePartVar]
    public SkinnedMeshRenderer[] renderers;
    [HBS.SerializePartVar]
    public bool useLoop;
    [HBS.SerializePartVar]
    public bool usePingPong;
    [HBS.SerializePartVar]
    public bool useSine;
    [HBS.SerializePartVar]
    public bool useCenteredValue;
    [HBS.SerializePartVar]
    public float loopSpeed;
    [HBS.SerializePartVar]
    public float weightStrength;
    public override void ReadFromPropertiesPass2() {}
    public override void ReadFromPropertiesPass3() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
