using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBVectorMathBox : Part
{
    public VectorWire vectorWire;
    public VectorMethod method;
    public VectorWire.VectorWireInput inputA;
    public VectorWire.VectorWireInput inputB;
    public VectorWire.VectorWireInput inputT;
    public FloatWire floatWire;
    public FloatWire.FloatWireInput floatInputA;
    public FloatWire.FloatWireInput floatInputB;
    public FloatWire.FloatWireInput floatInputC;
    public FloatWire.FloatWireOutput floatOutputX;
    public FloatWire.FloatWireOutput floatOutputY;
    public FloatWire.FloatWireOutput floatOutputZ;
    public VectorWire.VectorWireOutput output;
    public override void ReadFromPropertiesPassImmediate() {}
    public override void ReadFromPropertiesPassImmediateNew() {}
}
