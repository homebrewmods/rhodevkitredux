using System;
using UnityEngine;
using HBS;
using SLua;
namespace HBNetworking
{
    public enum NetworkCallType
    {
        Start = 0,
        Update = 1,
        Connect = 2,
        Connecting = 3,
        Disconnect = 4,
        ConnectFail = 5,
        ForceReconnect = 6,
        OtherJoined = 7,
        OtherLeft = 8,
        OwnerSwitch = 9,
        AskStateSync = 10,
        StateSync = 11,
        AddComponent = 12,
        General = 13,
        Position = 14,
        Ping = 15,
        Remove = 16,
        Transfered = 17,
        Transfering = 18,
        TransferProgress = 19,
        Created = 20,
        ApplyServerSettings = 21,
        AdminLogin = 22,
        ServerInitializeData = 23,
        ChatMessage = 24,
        NetworkTimeSync = 25
    }
}
