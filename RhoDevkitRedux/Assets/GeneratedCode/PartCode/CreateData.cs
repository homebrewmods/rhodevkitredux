using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class CreateData : UnityEngine.Networking.MessageBase
    {
        public double x;
        public double y;
        public double z;
        public Quaternion rot;
        public string prefabPath;
        public int connID;
        public int myConnID;
        public string id;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
