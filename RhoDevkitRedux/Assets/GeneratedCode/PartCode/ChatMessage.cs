using System;
using UnityEngine;
using HBS;
using SLua;
using UnityEngine.Networking;
namespace HBNetworking
{
    public class ChatMessage : UnityEngine.Networking.MessageBase
    {
        public string name;
        public string msg;
        public override void Serialize(NetworkWriter writer) {}
        public override void Deserialize(NetworkReader reader) {}
    }
}
