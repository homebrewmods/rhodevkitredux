using System;
using UnityEngine;
using HBS;
using SLua;
[SLua.CustomLuaClass]
[HBS.SerializePart]
public class HBPressureSensor : Part
{
    public override void ReadFromPropertiesPassImmediateNew() {}
    public override void ReadFromPropertiesPassImmediate() {}
}
